# VOLUNTEER
#### [Go back](https://bitbucket.org/apdc8bit/appserver/src/master/api/api.md)



### Register, Create Worker and List Workers is the same


### Create New Event
- method: **POST**
- endpoint: **event/create**
- payload: **json**
```json
{
	"title":"String",
	"description":"String",
	"workerUsername":"String (6+) Numbers & Letters",
	"eventTime": "String date dd/mm/yyyy hh:mm  ex: 12/05/2018 15:50",
    "lat": 37.666,
    "lng": -7.666
}
```
- response: **json**
- **200**
```json
{
	"id" : 123213213
}
```
- **400**
```json
{
	"entity_is_not_volunteer"
}
```
- **400**
```json
{
	"worker_not_found"
}
```
- **400**
```json
{
	"invalid_date"
}
```
- **400**
```json
{
	"error_parsing_date"
}
```

### List My Events: 
    if basic -> lists the ones he joined, 
    if worker-> lists the ones associated, 
    if entity-> lists the ones created
- method: **GET**
- endpoint: **event/listMy?filter={filter}**
- query param = **filter** all, started, archived
- payload: **no**
- response: **json**
- **200**
```json
[
    {
        "isGoing":false,
        "title":"String",
        "description": "String",
        "worker": "String username",
        "eventTime": "Date",
        "lat": 38.12,
        "lng": -9.11,
        "id": 234234324,
        "usersJoined": [
            {
                "username":"String",
                "imageID":"String",
                "points": 123,
                "level": 420,
                "next_level_points": 400
            }, {}
        ],
        "entity": {
            "dico": "1212",
            "imageID": "String",
            "district" : "String",
            "county": "String",
            "address": "String",
            "name":"String",
            "creation_time": "Date",
            "lat": 38.66,
            "lng": 8.3,
            "totalStarted": 12,
            "totalCompleted": 3,
            "avgResponseTime": 4,
            "avgCompletionTime":3
        },
        "state": "started or archived"
    }
]
```
- **400**
```json
{
	"user_not_allowed"
}
```

### List All Events
- method: **GET**
- endpoint: **event/listAll?filter={filter}**
- query param: **filter** true or false, filter to users location
- payload: **no**
- response: **json**
- **200**
```json
[
    {
        "isGoing":false,
        "title":"String",
        "description": "String",
        "worker": "String username",
        "eventTime": "Date",
        "dico": "String",
        "address": "String username",
        "evencountytTime": "Date",
        "lat": 38.12,
        "lng": -9.11,
        "id": 234234324,
        "usersJoined": [
            {
                "username":"String",
                "imageID":"String",
                "points": 123,
                "level": 420,
                "next_level_points": 400
            }, {}
        ],
        "entity": {
            "dico": "1212",
            "imageID": "String",
            "district" : "String",
            "county": "String",
            "address": "String",
            "name":"String",
            "creation_time": "Date",
            "lat": 38.66,
            "lng": 8.3,
            "totalStarted": 12,
            "totalCompleted": 3,
            "avgResponseTime": 4,
            "avgCompletionTime":3
        },
        "state": "started or archived"
    }
]
```

### Get event by ID
- method: **GET**
- endpoint: **event/get/{id}**
- path param: **id** id do evento
- payload: **no**
- response: **json**
- **200**
```json
{
    "isGoing":false,
    "title":"String",
    "description": "String",
    "worker": "String username",
    "eventTime": "Date",
    "dico": "String",
    "address": "String username",
    "evencountytTime": "Date",
    "lat": 38.12,
    "lng": -9.11,
    "id": 234234324,
    "usersJoined": [
        {
            "username":"String",
            "imageID":"String",
            "points": 123,
            "level": 420,
            "next_level_points": 400
        }, {}
    ],
    "entity": {
        "dico": "1212",
        "imageID": "String",
        "district" : "String",
        "county": "String",
        "address": "String",
        "name":"String",
        "creation_time": "Date",
        "lat": 38.66,
        "lng": 8.3,
        "totalStarted": 12,
        "totalCompleted": 3,
        "avgResponseTime": 4,
        "avgCompletionTime":3
    },
    "state": "started or archived"
}
```

### Join Event
- method: **POST**
- endpoint: **event/join/{id}**
- path param: **id** : id of event
- payload: **no**
- response: **json**
- **200**
```json
{
	"user_joined"
}
```
- **400**
```json
{
	"user_already_joined"
}
```
- **400**
```json
{
	"event_not_found"
}
```

### Disjoino Event
- method: **DELETE**
- endpoint: **event/disjoin/{id}**
- path param: **id** : id of event
- payload: **no**
- response: **json**
- **200**
```json
{
	"user_removed"
}
```
- **400**
```json
{
	"user_not_joined"
}
```
- **400**
```json
{
	"event_not_found"
}
```

### Remove User From Event
- method: **DELETE**
- endpoint: **event/abstain/{id}**
- path param: **id** : id of event
- payload: **no**
- response: **json**
- **200**
```json
{
	"user_removed"
}
```
- **400**
```json
{
	"user_not_joined"
}
```
- **400**
```json
{
	"event_not_found"
}
```
- **403**
```json
{
	"cannot_remove_users_from_archived_event"
}
```
- **403**
```json
{
	"worker_not_associated_with_event"
}
```

### Archive Event
- method: **PUT**
- endpoint: **event/archive/{id}**
- path param: **id** : id of event
- payload: **no**
- response: **json**
- **200**
```json
{
	"event_archived"
}
```
- **400**
```json
{
	"entity_is_not_volunteer"
}
```
- **403**
```json
{
	"entity_did_not_create_event"
}
```
- **400**
```json
{
	"event_not_found"
}
```
- **403**
```json
{
	"event_has_not_happened_yet"
}
```