# WORKER
#### [Go back](https://bitbucket.org/apdc8bit/appserver/src/master/api/api.md)

### Worker list Jobs
- method: **GET**
- endpoint: **worker/getJobs?filter={filter}**
- query param: filter = todo, waiting_confirmation, completed, ""
- payload: **no**
- response: **json**
```json
[
    {
       "worker_username": "String",
       "occurrence_username": "String",
       "occurrence_id": 234324,
       "date_added": "Date",
       "date_finished": "Date",
       "state": "String: TODO, WAITING_CONFIRMATION, COMPLETED",
       "imageID": "String",
       "occurrence": "object, see api page"
    }, {}
]
```

### Worker Change Occurrence Type
- method: **PUT**
- endpoint: **worker/changeType**
- payload: **json**
```json
{
  "value":"String",
  "occurrence_id": 3454353435
}
```
- response: **json**
```json
{
  "status_updated"
}
```

### Worker Mark Job Complete
- method: **PUT**
- endpoint: **worker/markCompleted**
- payload: **json**
```json
{
  "occurrence_id": 324234323,
  "imageID": "String"
}
```
- response: **json**
```json
{
  "marked_completed"
}
```
