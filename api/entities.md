# ENTITY
#### [Go back](https://bitbucket.org/apdc8bit/appserver/src/master/api/api.md)



### Register
- method: **POST**
- endpoint: **entity/register**
- payload: **json**
```json
{
	"username":"String (4-12) Numbers & Letters",
	"email":"String Valid Email",
	"password":"String (6+) Numbers & Letters",
	"confirmation":"String equal to password",
    "name":"String",
    "zipcode":"String XXXX-XXX",
    "phoneNumber": 987654321,  
    "nif": 123456789,
    "imageID":"String",
    "address":"String",
	"isVolunteer": true
}
```
- response: **json**
- **200**
```json
{
	"message": "user_registered",
	"code": 70
}
```
- **409**
```json
{
	"message": "email_exists",
	"code": 42
}
```
- **409**
```json
{
	"message": "username_exists",
	"code": 40
}
```



### Entity Create Worker
- method: **POST**
- endpoint: **entity/create_worker**
- payload: **json**
```json
{
	"username":"String (4-12) Numbers & Letters",
	"email":"String Valid Email",
	"password":"String (6+) Numbers & Letters",
	"confirmation":"String equal to password",
	"name":"String",
	"zipcode":"String XXXX-XXX",
	"phoneNumber": 987654321,
	"nif": 123456789,
	"imageID":"String",
	"address":"String"
}
```
- response: **json**
```json
{
	"user_registered"
}
```

### Entity Edit Worker
- method: **PUT**
- endpoint: **entity/edit_worker**
- payload: **json**
```json
{
    "name":"String",
    "zipcode":"String XXXX-XXX",
    "phoneNumber": 987654321,
    "nif": 123456789,
    "imageID":"String",
    "address":"String"
}
```
- response: **json**
```json
{
	"worker_edited"
}
```

### Delete Worker Info
- method: **PUT**
- endpoint: **entity/delete_info**
- payload: **json**
```json
{
	"name": true,
	"zipcode": false,
	"phoneNumber": false,  
	"nif": true,
	"imageID": false,
	"address":false
}
```
- response: **json**
```json
{
	"info_deleted"
}
```

### Entity Delete Worker
- method: **PUT**
- endpoint: **entity/delete_worker**
- payload: **json**
```json
{
	"username":"String"
}
```
- response: **json**
```json
{
	"worker_deleted"
}
```

### Entity Associate Occurrence to Worker
- method: **POST**
- endpoint: **entity/associate_occurrence**
- payload: **json**
```json
{
	"username_worker":"String Valid Email",
	"username_occurrence":"String Valid Email",
	"id_occurrence": 546546456
}
```
- response: **json**
```json
{
	"occurrence_associated"
}
```
```json
{
	"occurrence_is_in_a_different_county"
}
```

### Entity Remove Association
- method: **PUT**
- endpoint: **entity/remove_association**
- payload: **json**
```json
{
	"username_worker":"String Valid Email",
	"username_occurrence":"String Valid Email",
	"id_occurrence": 435546546
}
```
- response: **json**
```json
{
	"occurrence_associated"
}
```

### Entity Confirm Job
- method: **PUT**
- endpoint: **entity/confirm_job**
- payload: **json**
```json
{
	"username_worker":"String Valid Email",
	"username_occurrence":"String Valid Email",
	"id_occurrence": 546546456,
	"confirm": true
}
```
- response: **json**
```json
{
	"job_confirmed"
}
```

### Entity List Workers
- method: **GET**
- endpoint: **entity/list_workers**
- payload: **no**
- response: **json**
```json
[
    {
        "worker_username":"worker",
        "date_added":"Jun 8, 2018 10:39:09 AM"
    }, {}
]
```

### **NEW** Entity List Jobs
- method: **GET**
- endpoint: **entity/list_jobs?worker={worker}?filter={filter}**
- query param: worker = worker username, or nothing
- query param: filter = todo, waiting_confirmation, completed, or nothing
- payload: **no**
- response: **json**
```json
[
    {
       "worker_username": "String",
       "occurrence_username": "String",
       "occurrence_id": 234324,
       "date_added": "Date",
       "date_finished": "Date",
       "state": "String",
       "imageID": "String",
       "occurrence": "object, see api page"
    }, {}
]
```

### **NEW** List Occurrences in Entity's area
- method: **GET**
- endpoint: **entity/listOccurrencesInArea**
- payload: **no**
- response: **json**
```json
[
    {
       "see API page": "List of Occurrences"
    }, {}
]
```

### **NEW** List Private Entity Stats
- method: **GET**
- endpoint: **entity/privateEntityStats?worker={worker}**
- query param: worker = worker username, or nothing
- payload: **no**
- response: **json**
```json
[
    {
			"avgTimeFinish": 12,
			"numbJobsStarted": 3,
			"numJobsFinish": 4,
			"numbJobsWaiting":6
    }, {}
]
```

### **NEW** List Public Entity Stats
- method: **GET**
- endpoint: **entity/publicEntityStats?entity={entity}**
- query param: worker = entity username
- payload: **no**
- response: **json**
```json
{
	"dico": "1212",
	"imageID": "String",
	"district" : "String",
	"county": "String",
	"address": "String",
	"name":"String",
	"creation_time": "Date",
	"lat": 38.66,
	"lng": 8.3,
	"totalStarted": 12,
	"totalCompleted": 3,
	"avgResponseTime": 4,
	"avgCompletionTime":3
}
```

### Entity Edit Occurrence Status
- method: **PUT**
- endpoint: **special/editOccurrenceStatus**
- payload: **json**
```json
{
  "id":235324,
  "username":"String username occ",
  "status":"String"
}
```
- response: **json**
```json
{
  "status_updated"
}
```

### Entity List Occurrences by User
- method: **GET**
- endpoint: **special/listByUser?username={username}**
- query param: **username** Username of Occurrence
- payload: **no**
- response: **json**
```json
[
    {
       "username_occurrence":"String",
       "id_occurrence": 23432,
       "users":[
          "username", "username2"
       ],
       "numb_reports": 4,
       "first_report_date": "String date",
       "last_report_date": "String date"
    }, {}
]
```
