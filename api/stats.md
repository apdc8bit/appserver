# STATS
#### [Go back](https://bitbucket.org/apdc8bit/appserver/src/master/api/api.md)

### **NEW** Show all basic Stats
- method: **GET**
- endpoint: **globalStats/getBasicStats?days={days}**
- query param: **days** number of days to return (0  < days <= 7) or nothing
- payload: **no**
- response: **json**
```json
{
  "id": 0,
  "date": "Wed Jul 04 11:21:07 UTC 2018",
  "topOccurrencesLast5Days": [
    {
      "see API page": "list of occurrences"
    }, {}
  ],
  "userMostOccurrences": {
        "username": "userBasic1",
        "imageID": "DS34vgXtXx",
        "points": 200,
        "level": 1,
        "next_level_points": 400
    },
  "top5UsersByLevel": [
    {
        "username": "userBasic2",
        "imageID": "34x5q67er89",
        "points": 530,
        "level": 4,
        "next_level_points": 1000
    },
    {
        "username": "userBasic5",
        "points": 570,
        "level": 2,
        "next_level_points": 600
    },
    {
        "username": "testing-Email",
        "points": 500,
        "level": 2,
        "next_level_points": 600
    },
    {
        "username": "userBasic8",
        "imageID": "kzFJA8D5F0",
        "points": 300,
        "level": 2,
        "next_level_points": 600
    }
  ],
  "totalNumberUsers": 55,
  "numberBasicUsers": 21,
  "numberWorkersUsers": 26,
  "usersRegisteredYesterday": 5,
  "userRegisteredLastWeek": 41,
  "getUsersRegisteredLastMonth": 55,
  "totalNumberOccurrences": 473,
  "occurrencesOnHoldTotal": 78,
  "occurrencesInProgressTotal": 0,
  "occurrencesCompletedTotal": 395,
  "occurrencesCreatedLastWeek": 68,
  "occurrencesCreatedYesterday": 5,
  "occurrencesStartedLastWeek": 3,
  "occurrencesCompletedLastWeek": 1,
  "averageOccurrenceCompletionTime": 4,
  "averageOccurrenceResponseTime": 5,
  "minFireRisk": 1,
  "maxFireRisk": 4,
  "avgFireRisk": 1,
  "loginsLastDay": 25,
  "loginsLastWeek": 314,
  "totalVotes": 54,
  "totalComments": 6,
  "votesLastWeek": 54,
  "commentsLasWeek": 6,
  "atRiskCounties":[
        "String county name", "sadsad"
  ]
}
```

### **NEW** List all Entities on Map
- method: **GET**
- endpoint: **entity/listAllEntities**
- payload: **no**
- response: **json**
```json
[
  {
    "dico": "1212",
    "imageID": "String",
    "district" : "String",
    "county": "String",
    "address": "String",
    "name":"String",
    "creation_time": "Date",
    "lat": 38.66,
    "lng": 8.3,
    "totalStarted": 12,
    "totalCompleted": 3,
    "avgResponseTime": 4,
    "avgCompletionTime":3
  }, {}
]
```
