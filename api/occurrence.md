# OCCURRENCE
#### [Go back](https://bitbucket.org/apdc8bit/appserver/src/master/api/api.md)


### Check if Occurrence exists in area
- method: **POST**
- endpoint: **occurrence/check**
- payload: **json**
```json
{
	"title":"String",
	"description":"String",
	"type":"String (get types from server)",
	"lat": 37.11111,
	"lng": -7.2222,
	"imageID":"String",
	"lats":[
		37.11111, 37.11111, 37.11111
	],
	"lngs":[
		-7.2222, -7.2222, -7.2222
	]
}
```
- response: **json**
- **200** nothing, existence is bliss
- **202**
```json
{
	"see API page": "will allow to create new occurrence, returns the closest occurrence"
}
```
- **409**
```json
{
	"see API page": "will not allow to create new occurrence, returns the closest occurrence"
}
```

### Add Occurrence
- method: **POST**
- endpoint: **occurrence/add**
- payload: **json**
```json
{
	"title":"String",
	"description":"String",
	"type":"String (get types from server)",
	"lat": 37.11111,
	"lng": -7.2222,
	"imageID":"String",
	"lats":[
		37.11111, 37.11111, 37.11111
	],
	"lngs":[
		-7.2222, -7.2222, -7.2222
	]
}
```
- response: **json**
- **200**
```json
{
	"lat": 37.11111,
    "lng": -7.2222,
    "id": 3244538,
    "username":"String creator of occurrence"
}
```
- **409**
```json
{
	"occurrence_exists"
}
```

### Edit Occurrence
- method: **PUT**
- endpoint: **occurrence/edit**
- payload: **json**
```json
{
	"id": 564375834,
	"username":"String",
	"title":"String",
	"description":"String",
	"type":"String (get types from server)",
	"lat": 37.11111,
	"lng": -7.2222,
	"imageID":"String",
	"lats":[
		37.11111, 37.11111, 37.11111
	],
	"lngs":[
		-7.2222, -7.2222, -7.2222
	]
}
```
- response: **json**
```json
{
	"lat": 37.11111,
    "lng": -7.2222
}
```

### Delete Occurrence
- method: **PUT**
- endpoint: **occurrence/delete**
- payload: **json**
```json
{
	"id": 43546546,
	"username":"String"
}
```
- response: **json**
```json
{
	"occurrence_removed"
}
```

### Vote Occurrence
- method: **POST**
- endpoint: **occurrence/vote**
- payload: **json**
```json
{
	"id": 6786545345,
	"username":"String"
}
```
- response: **json**
```json
{
	"vote_succeeded"
}
```

### Remove Vote Occurrence
- method: **PUT**
- endpoint: **occurrence/deleteVote**
- payload: **json**
```json
{
	"id": 435546546,
	"username":"String"
}
```
- response: **json**
```json
{
	"vote_removed"
}
```

### Add Comment Occurrence
- method: **POST**
- endpoint: **occurrence/comment**
- payload: **json**
```json
{
	"id": 465465676,
	"username":"String",
	"comment":"String"
}
```
- response: **json**
```json
{
	"comment_added"
}
```

### Remove Comment Occurrence
- method: **PUT**
- endpoint: **occurrence/deleteComment**
- payload: **json**
```json
{
	"comment_id":23435435435
}
```
- response: **json**
```json
{
	"comment_removed"
}
```

### Edit Comment Occurrence
- method: **PUT**
- endpoint: **occurrence/editComment**
- payload: **json**
```json
{
	"comment_id": 54657567,
	"comment":"String"
}
```
- response: **json**
```json
{
	"comment_edited"
}
```

### Report Occurrence
- method: **POST**
- endpoint: **occurrence/report**
- payload: **json**
```json
{
	"id": 54657567,
	"username":"String",
	"report":"String type: violent, offensive, fraudulent, fake, spam, other, sexual"
}
```
- response: **json**
```json
{
	"report_submitted"
}
```

### List Occurrence Comments
- method: **POST**
- endpoint: **occurrenceInfo/getComments?cursor={cursor}&size={size}**
- query param: cursor = {null or Value Previously Returned}
- query param: size = {null or number of occurrences between 1 and MAX=5}
- payload: **json**
```json
{
	"id": 3245435435,
	"username":"from the occurrence"
}
```
- response: **json**
```json
{
"cursor":"String",
"data": [
    {
        "comment":"this is a comment",
        "comment_id":6069304185323520,
        "creating_time":"Jun 8, 2018 10:39:09 AM",
        "last_edit_time":"Jun 8, 2018 10:39:09 AM",
        "username":"String",
        "imageID":"String image of user"
    }, {}
  ]
}
```

### List my  Occurrences
- method: **GET**
- endpoint: **occurrenceInfo/myOccurrences?cursor={cursor}&size={size}**
- query param: cursor = {null or Value Previously Returned}
- query param: size = {null or number of occurrences between 1 and MAX=10}
- payload: **no**
- response: **json**
```json
{
	"cursor": "String",
	"data": [
	{
		"see api page"
	},{},{}
	]
}
```

### Get occurrence info
- method: **POST**
- endpoint: **occurrenceInfo/info**
- payload: **json**
```json
{
  "username":"String",
  "id":234234
}
```
- response: **json**
```json
  "see api page"
```


### List all  Occurrences by Area **DEPRECATED: USE getMarkers**
- method: **POST**
- endpoint: **occurrenceInfo/listByArea**
- payload: **json**
```json
{
	 "top_left_lat": 37,
	 "top_left_lng": 38,
	 "bottom_right_lat": -8,
	 "bottom_right_lng": -7
}
```
- response: **json**
- **200** (returns Occurrences array)
```json
[
  {
	"see api page"
  }, {}, {}
]
```
- **202** (returns number of occurrences by a center (of hash))
```json
[
  {
     "numb_markers": 700,
     "lat_center": 38.7,
     "lng_center": -8.9
  }, {}, {}
]
```

### List all  Occurrences by Hash
- method: **GET**
- endpoint: **occurrenceInfo/getMarkers/{geohash}/{cluster}**
- path param: **geohash** generated by the client, 1 < lenght <= 4
- path param: **cluster** boolean, if true will send number of occs in area, 
						if false will send all occs if number of occs is smaller than 30 web / 15 mobile
- payload: **no**
- response: **json**
- **200** (returns Occurrences array)
```json
{
  "is_markers":true,
  "data":[
      {
        "see api page"
      }, {}, {}
  ]
}
```
- **200** (Area is too big, returns number of markers and the center of the hash)
```json
{
  "is_markers":false,
  "data":{
       "numb_markers": 700,
       "lat_center": 38.7,
       "lng_center": -8.9
  }
}
```

### List all  Occurrences by Dico
- method: **GET**
- endpoint: **occurrenceInfo/getMarkers/{dico}**
- path param: **dico** dico String
- payload: **no**
- response: **json**
- **200** (returns Occurrences array)
```json
[
	{
	"see api page"
	}, {}, {}
 ]
```
- **400**
```json
{
  "invalid_dico"
}
```