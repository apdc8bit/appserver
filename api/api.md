
# API
	Welcome. To use this API please accept this as the base URL:
	https://gaea-8bit.appspot.com/rest/{w || m}/

    Headers for Requests:
    "Authorization":"token"
    "SessionID":"session"
    
    API is outdated in some ares, but is mostly OK
	
## Select a Topic:

### [USER](https://bitbucket.org/apdc8bit/appserver/src/master/api/user.md)
### [OCCURRENCE](https://bitbucket.org/apdc8bit/appserver/src/master/api/occurrence.md)
### [WORKER](https://bitbucket.org/apdc8bit/appserver/src/master/api/worker.md)
### [ENTITY](https://bitbucket.org/apdc8bit/appserver/src/master/api/entities.md)
### [ADMIN](https://bitbucket.org/apdc8bit/appserver/src/master/api/admin.md)
### [STATS](https://bitbucket.org/apdc8bit/appserver/src/master/api/stats.md)
### [VOLUNTEER](https://bitbucket.org/apdc8bit/appserver/src/master/api/volunteer.md)



	When making requests to this API, one of the following messages can occur:
	- 200 : OK, operation successful
	- 400 : invalid values
	- 401 : token is invalid or expired
	- 403 : user doesn't have permission
	- 500 : Processing error, ups
    
##### Invalid Token: 401
```json
{
  "response":"invalid_token",
  "code": 1
}
```
##### Expired Token: 401
```json
{
  "response":"expired_token",
  "code": 2
}
```

##### Missing or Wrong Parameters: 400
```json
{
	"message": "missing_or_wrong_data",
	"code": 4
}
```

##### User Does not Have Permission: 403
```json
{
	"message": "user_no_permission",
	"code": 63
}
```

##### User Does not exist: 400
```json
{
	"message": "user_does_not_exist",
	"code": 3
}
```

##### Wrong Device: 403
```json
{
	"message": "wrong_device_id",
	"code": 3
}
```

##### Wrong User Role: 403
```json
{
	"message": "user_role_does_not_exist",
	"code": 61
}
```

##### User is not Valid: 403
```json
{
	"message": "user_not_valid",
	"code": 60
}
```

### When a Occurrence is return: 
```json
{
    "username":"String",
    "title":"String",
    "description":"String",
    "type":"String",
    "status":"String",
    "imageID":"String",
    "lat": 37.11111,
    "lng": -7.2222,
    "id": 324324323,
    "lats":[
        37.11111, 37.11111, 37.11111
    ],
    "lngs":[
        -7.2222, -7.2222, -7.2222
    ],
    "comment_count": 3,
    "vote_count": 4,
    "has_vote": true,
    "geoHash": "asd2",
    "completedImageID":"String",
    "county": "String concelho",
    "district": "String distrito",
    "dico": "String location id 1010", 
    "priority": 5,
    "address":"String will not come if geocode return NO_RESULTS"
}
```