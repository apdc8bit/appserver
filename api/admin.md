# ADMIN
#### [Go back](https://bitbucket.org/apdc8bit/appserver/src/master/api/api.md)


### Ping
- method: **GET**
- endpoint: **special/ping**
- payload: **no**
- response: **json**
- **200** OK
- **400** 
```json
{
  "invalid_token"
}
```
- **403** If user is not admin
```json
{
  "user_not_allowed"
}
```


### List Users
- method: **POST**
- endpoint: **special/listUsers?type={type}&cursor={cursor}&direction={direction}**
- query param: **type** = lastEdit, registerDate, valid, role (default userCreationTime)
- query param: **cursor** cursor
- query param: **direction** = asc (default des)
- payload: **no**
- response: **json**
```json
[
  {
	"username":"String",
	"email":"String",
	"name":"String",
	"zipcode":"String",
	"phoneNumber": 987654321,
	"nif": 123456789,
	"imageID":"String",
	"address":"String",
	"lastEdit":"String",
	"register_date":"String",
	"points": 123,
    "level": 420
    },{}
]
```

### Confirm Report
- method: **PUT**
- endpoint: **special/confirmReport?force={force}**
- query param: **force** forces the deletion if occurrence has a job associated
- payload: **json**
```json
{
  "occ_id":235324,
  "usconfirmationername": false,
}
```
- response: **json**
**200**
```json
{
  "report_confirmed"
}
```
**400**
```json
{
  "report_not_found"
}
```
**409**
```json
{
  "has_job_use_force"
}
```

### Delete User
- method: **PUT**
- endpoint: **special/deleteUser**
- payload: **json**
```json
{
    "password":"String Admin Password",
    "username":"String user to delete"
}
```
- response: **json**
```json
{
  "user_deleted"
}
```

### Verify Role Change 
- method: **POST**
- endpoint: **special/verifyRoleChange**
- payload: **json**
```json
{
    "password": "Admin password",
    "username":"String Entity Username",
    "accepted": true
}
```
- response: **json**
```json
{
  "role_changed"
}
```

### Delete Occurrence
- method: **PUT**
- endpoint: **special/delete?force={force}**
- query param: **force** forces the deletion if occurrence has a job associated
- payload: **json**
```json
{
  "id":235324,
  "username":"String username occ",
  "admin_password":"String"
}
```
- response: **json**
**200**
```json
{
  "occurrence_removed"
}
```
**400**
```json
{
  "occurrence_not_found"
}
```
**409**
```json
{
  "has_job_use_force"
}
```

### Delete Comment
- method: **PUT**
- endpoint: **special/deleteComment**
- payload: **json**
```json
{
  "comment_id":235324,
  "admin_password":"String"
}
```
- response: **json**
```json
{
  "comment_deleted"
}
```

### Number of Reports
- method: **POST**
- endpoint: **special/numbReports**
- payload: **json**
```json
{
  "password":"String"
}
```
- response: **json**
```json
{
  "numbReports": 404
}
```

### List Reports
- method: **POST**
- endpoint: **special/listReports**
- payload: **json**
```json
{
  "password":"String"
}
```
- response: **json**
```json
[
    {
       "occurrence": {
         "see API page":"whole occurrence"
       },
       "users": [
         {"username"}
       ],
       "numb_reports": 10,
       "first_report_date": "String",
       "last_report_date": "String",
       "type": "Type of Report"
    }, {}
]
```

### Register New Admin
- method: **POST**
- endpoint: **special/createAdmin**
- payload: **json**
```json
{
	"username":"String (4-12) Numbers & Letters",
	"email":"String Valid Email",
	"password":"String (6+) Numbers & Letters",
	"confirmation":"String equal to password",
	"admin_password":"Admin Password"
}
```
- response: **json**
```json
{
  "user_registered"
}
```

### List Role Change Requests
- method: **POST**
- endpoint: **special/listRoleChangeRequests**
- payload: **no**
- response: **json**
```json
[
  {
    "username":"String",
    "email":"String",
    "name":"String",
    "zipcode":"String",
    "phoneNumber": 987654321,
    "nif": 123456789,
    "imageID":"String",
    "address":"String",
    "lastEdit":"String",
    "register_date":"String",
  }, {}
]
```