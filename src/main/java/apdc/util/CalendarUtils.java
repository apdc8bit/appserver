package apdc.util;

import java.util.Date;

public class CalendarUtils {
    public static Date addDays(Date date, int days) {
        java.util.Calendar cal = java.util.Calendar.getInstance();
        cal.setTime(date);
        cal.add(java.util.Calendar.DATE, days);
        return cal.getTime();
    }
}
