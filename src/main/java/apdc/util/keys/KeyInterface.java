package apdc.util.keys;

public interface KeyInterface {
    //Divider for the different values
    String DIVIDER = "#&#&#";

    //Position of the values in the array
    int TYPE = 0;
    int VALUE = 1;
    int EXPIRATION = 2;
    int SIZE = 3;

    //Expiration times
    long EXPIRATION_TIME_WEB = 1000 * 60 * 60 * 2; // 2 hours
    long EXPIRATION_TIME_MOBILE = 1000 * 60 * 60 * 24 * 2; // 2 days

    //Types of keys
    enum Type {
        TOKEN,
        MOBILE,
        PICTURE
    }
}
