package apdc.util.email;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServlet;
import java.io.UnsupportedEncodingException;
import java.util.Properties;
import java.util.logging.Logger;

@SuppressWarnings("serial")
public class GoogleMail extends HttpServlet {
    private static final String ADDRESS = "noreply@gaea-8bit.appspotmail.com";
    private static final String PERSONAL = "Gaea";

    private static final Logger LOG = Logger.getLogger(GoogleMail.class.getName());

    public static void sendEmail(String to_email, String subject, String message) {
        Properties props = new Properties();
        Session session = Session.getDefaultInstance(props, null);

        try {
            MimeMessage msg = new MimeMessage(session);
            //Set from
            msg.setFrom(new InternetAddress(ADDRESS, PERSONAL));
            //Set to
            msg.addRecipient(Message.RecipientType.TO, new InternetAddress(to_email));
            //Set subject
            msg.setSubject(subject, "utf-8");
            //Set body = html
            msg.setContent(message, "text/html; charset=utf-8");
            Transport.send(msg);
        } catch (MessagingException | UnsupportedEncodingException e) {
            LOG.warning("Error sending email");
        }
    }
}