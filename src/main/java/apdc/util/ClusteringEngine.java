package apdc.util;


import apdc.input.verification.values.OccurrenceValues;
import apdc.output.info.OccurrenceInfo;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

public class ClusteringEngine {
    private final Logger LOG = Logger.getLogger(ClusteringEngine.class.getName());
    private final double MIN_DELTA = 1.0, MAX_DELTA = 100.0, CLUSTER_RATIO = 0.4;//.15

    private final double topLeftLat;
    private final double topLeftLng;
    private final double bottomRightLat;
    private final double bottomRightLng;

    //between 3 and 9
    private int zoomLevel;
    private double avgDelta;


    public ClusteringEngine(double topLeftLat, double topLeftLng, double bottomRightLat, double bottomRightLng) {
        this.topLeftLat = topLeftLat;
        this.topLeftLng = topLeftLng;
        this.bottomRightLat = bottomRightLat;
        this.bottomRightLng = bottomRightLng;

        calculateZoom();
    }

    private double distance(OccurrenceInfo markerL, OccurrenceInfo markerR) {
        double latDiff = markerL.lat - markerR.lat;
        double lngDiff = markerL.lng - markerR.lng;

        return Math.sqrt(Math.pow(latDiff, 2) + Math.pow(lngDiff, 2));
    }

    public List<OccurrenceInfo> start(List<Entity> occurrences, Key userKey) {

        List<OccurrenceInfo> info = OccurrenceValues.getListWithUserVote(occurrences, userKey);
        LOG.info("Starting engine with " + info.size() + " markers.");
        return execute(new ArrayList<>(info));
    }

    private List<OccurrenceInfo> execute(List<OccurrenceInfo> markers) {
        //  LOG.info("RUNNING: MARKERS: " + markers.size());

        if (markers.size() <= 1) {
            //LOG.info("RUNNING: REACHED END");
            return markers;
        }

        double threshold = avgDelta * CLUSTER_RATIO;
        int half_length = (int) Math.floor(markers.size() / 2);

        // LOG.info("RUNNING: THRESHOLD: " + threshold);

        List<OccurrenceInfo> tmp1 = new ArrayList<>(markers.subList(0, half_length));
        List<OccurrenceInfo> tmp2 = new ArrayList<>(markers.subList(half_length, markers.size()));

        List<OccurrenceInfo> leftSide = execute(tmp1);
        List<OccurrenceInfo> rightSide = execute(tmp2);

        Set<OccurrenceInfo> leftSideTMP = new HashSet<>(leftSide);
        Set<OccurrenceInfo> rightSideTMP = new HashSet<>(rightSide);

        //LOG.info("RUNNING: HALF: " + half_length + " | LEFT: " + leftSide.size() + " , RIGHT: " + rightSide.size());

        List<OccurrenceInfo> clustered = new ArrayList<>();

        for (OccurrenceInfo markerL : leftSide) {
            for (OccurrenceInfo markerR : rightSide) {
                if (distance(markerL, markerR) < threshold) {
                    double lat, lng;
                    int count;

                    List<OccurrenceInfo> c_markers;

                    //    LOG.info("RUNNING: LEFT CLUSTER: " + markerL.isCluster + " | LEFT CLUSTER: " + markerR.isCluster);

                    if (markerL.isCluster && markerR.isCluster) {
                        count = markerL.count + markerR.count;
                        lat = ((markerL.count * markerL.lat) + (markerR.count * markerR.lat)) / count;
                        lng = ((markerL.count * markerL.lng) + (markerR.count * markerR.lng)) / count;

                        List<OccurrenceInfo> tmp = new ArrayList<>();
                        tmp.addAll(markerL.markers);
                        tmp.addAll(markerR.markers);
                        c_markers = tmp;
                    } else if (markerL.isCluster) {
                        count = markerL.count + 1;
                        lat = ((markerL.count * markerL.lat) + markerR.lat) / count;
                        lng = ((markerL.count * markerL.lng) + markerR.lng) / count;
                        markerL.markers.add(markerR);

                        c_markers = markerL.markers;
                    } else if (markerR.isCluster) {
                        count = markerR.count + 1;
                        lat = ((markerR.count * markerR.lat) + markerL.lat) / count;
                        lng = ((markerR.count * markerR.lng) + markerL.lng) / count;

                        markerR.markers.add(markerL);
                        c_markers = markerR.markers;
                    } else {
                        count = 2;
                        lat = (markerR.lat + markerL.lat) / count;
                        lng = (markerR.lng + markerL.lng) / count;

                        List<OccurrenceInfo> tmp = new ArrayList<>();
                        tmp.add(markerL);
                        tmp.add(markerR);
                        c_markers = tmp;
                    }

                    leftSideTMP.remove(markerL);
                    rightSideTMP.remove(markerR);

//                    try {
//                        leftSideTMP.remove(i);
//                        rightSideTMP.remove(j);
//                    } catch (IndexOutOfBoundsException e) {
//                        LOG.warning("OUT OF BOUNDS");
//                    }

                    //    LOG.info("RUNNING: LEFT REMOVING: " + j + " | LEFT REMOVING: " + i);

                    OccurrenceInfo cluster = new OccurrenceInfo(lat, lng, count, c_markers);

                    clustered.add(cluster);
                }
            }
        }

        leftSideTMP.addAll(rightSideTMP);
        clustered.addAll(leftSideTMP);

        return clustered;
    }

    private void calculateZoom() {
        double latDelta = topLeftLat - bottomRightLat;

        double lngDelta = bottomRightLng - topLeftLng;

        assert latDelta > 0 && lngDelta > 0;

        avgDelta = (latDelta + lngDelta) / 2;

        if (avgDelta <= MIN_DELTA) {
            zoomLevel = 3;
        }

        if (avgDelta <= MAX_DELTA) {
            zoomLevel = 9;
        }
    }

}