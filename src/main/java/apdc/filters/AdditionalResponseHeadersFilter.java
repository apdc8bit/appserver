package apdc.filters;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

@Provider
public class AdditionalResponseHeadersFilter implements ContainerResponseFilter {

    public AdditionalResponseHeadersFilter() {
    }

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) {
        /* no time to test
        if (requestContext.getMethod().equals("GET")) {
            CacheControl cc = new CacheControl();
            cc.setMaxAge(100);
            responseContext.getHeaders().add("Cache-Control", cc.toString());
        }
        */

        responseContext.getHeaders().add("Access-Control-Allow-Origin", requestContext.getHeaderString("Origin"));
        responseContext.getHeaders().add("Access-Control-Allow-Credentials", "true");
        responseContext.getHeaders().add("Access-Control-Allow-Headers", "Content-Actions, X-Requested-With, Authorization, Content-Type, SessionID");
        responseContext.getHeaders().add("Access-Control-Allow-Methods", "POST, PUT, DELETE, PATCH, GET");
    }
}