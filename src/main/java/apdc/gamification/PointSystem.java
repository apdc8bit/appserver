package apdc.gamification;

import apdc.input.verification.values.NotificationValues;
import apdc.tables.user.Points;
import com.google.appengine.api.datastore.*;

import java.util.logging.Logger;

public class PointSystem {
    private static final Logger LOG = Logger.getLogger(PointSystem.class.getName());
    private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
    private static final PointsTableSingleton table = PointsTableSingleton.getInstance();

    public enum ACTIONS {
        create_occurrence,
        vote_occurrence,
        comment_occurrence,
        register,
        login,
        add_info,
        participate_event
    }

    public static boolean addPoints(ACTIONS action, String username) {
        boolean levelUP = false;
        Key userKey = KeyFactory.createKey(Points.kind, username);
        long action_points = getActionPoints(action);

        try {
            Entity userPoints = datastore.get(userKey);

            long points = (long) userPoints.getProperty(Points.points);
            long level = (long) userPoints.getProperty(Points.level);
            long next_level_points = pointsForLevelUp(level + 1);

            points += action_points;

            userPoints.setProperty(Points.points, points);

            if (points > next_level_points && next_level_points > 0) {
                levelUP = true;
                level = Math.min(++level, 6);
                userPoints.setProperty(Points.level, level);

                NotificationValues.userLevelUP(username, "Novo nivel!!!", "Acabou de avançar para o nível " + level);

                LOG.info("User: " + username + " leveled up to: " + level);
            }

            LOG.info("User: " + username + " now has: " + points + " points");

            datastore.put(userPoints);
        } catch (EntityNotFoundException e) {
            LOG.info("No user entry found when adding points, creating one");
            Entity userPoints = new Entity(Points.kind, username);
            userPoints.setProperty(Points.level, 1L);
            userPoints.setProperty(Points.points, action_points);

            datastore.put(userPoints);
        }

        return levelUP;
    }

    // Only used when a users removes a vote/comment
    public static void removePoints(ACTIONS action, String username) {
        Key userKey = KeyFactory.createKey(Points.kind, username);
        long action_points = getActionPoints(action);

        try {
            Entity userPoints = datastore.get(userKey);

            long points = (long) userPoints.getProperty(Points.points);
            long level = (long) userPoints.getProperty(Points.level);
            long current_level_points = pointsForLevelUp(level);

            points -= action_points;
            points = Math.max(0, points);

            userPoints.setProperty(Points.points, points);

            if (points < current_level_points) {
                level = Math.max(--level, 1);
                userPoints.setProperty(Points.level, level);

                LOG.info("User: " + username + " leveled down to: " + level);
            }

            LOG.info("User: " + username + " now has: " + points + " points");

            datastore.put(userPoints);
        } catch (EntityNotFoundException e) {
            LOG.warning("No user found when Removing points");
        }
    }

    public static long pointsForLevelUp(long next_level) {
        return table.getNextLevelPoints(next_level);
    }

    private static long getActionPoints(ACTIONS action) {
        return table.getPoints(action.name());
    }

    public static Values getPointsAndLevel(String username) {
        Key userKey = KeyFactory.createKey(Points.kind, username);
        Entity userPoints;
        long points, level;
        try {
            userPoints = datastore.get(userKey);
            points = (long) userPoints.getProperty(Points.points);
            level = (long) userPoints.getProperty(Points.level);
            return new Values(points, level);
        } catch (EntityNotFoundException e) {
            LOG.warning("No user found when returning points");
            return new Values(0L, 0L);
        }
    }

    public static void removeUserEntry(String username) {
        Key userPointsKey = KeyFactory.createKey(Points.kind, username);
        datastore.delete(userPointsKey);

    }

}
