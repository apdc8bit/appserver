package apdc.gamification;

import apdc.util.CalendarUtils;
import apdc.util.FileReader;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public class PointsTableSingleton {
    private static final Logger LOG = Logger.getLogger(PointsTableSingleton.class.getName());
    private static final int DAYS = 1;

    private static final String POINTS_TABLE = "points_per_action";
    private static final String LEVELS_TABLE = "points_per_level";

    private Map<String, Long> points_table;
    private Map<Long, Long> levels_table;

    private Date load_date;

    private static PointsTableSingleton instance = null;

    private PointsTableSingleton() {
    }

    public static PointsTableSingleton getInstance() {
        if (instance == null) {
            instance = new PointsTableSingleton();
        }
        return instance;
    }

    public long getPoints(String action) {
        checkTables();

        try {
            return points_table.get(action);
        } catch (NullPointerException n) {
            LOG.severe("Action Not Found");
            return 0L;
        }
    }

    public long getNextLevelPoints(long next_level) {
        checkTables();

        try {
            Long points = levels_table.get(next_level);
            if (points == null)
                return -1L;
            return points;
        } catch (NullPointerException n) {
            LOG.severe("Level not found");
            return -1L;
        }
    }

    private void checkTables() {
        if (points_table == null || levels_table == null || load_date == null || isExpired(load_date)) {
            points_table = loadTable(POINTS_TABLE);

            Map<String, Long> tmp = loadTable(LEVELS_TABLE);

            //Verify table integrity
            try {
                //Points
                for (PointSystem.ACTIONS action : PointSystem.ACTIONS.values())
                    points_table.get(action.name());

                //Levels
                long level = 1;
                levels_table = new HashMap<>();
                for (long points : tmp.values()) {
                    levels_table.put(level++, points);
                }
            } catch (NullPointerException n) {
                LOG.severe("Table is incomplete!!!");
            }
        }
    }

    private static boolean isExpired(Date loadDate) {
        return CalendarUtils.addDays(loadDate, DAYS).before(new Date());
    }

    @SuppressWarnings("unchecked")
    private Map<String, Long> loadTable(String tableFile) {
        Type type = new TypeToken<Map<String, Long>>() {
        }.getType();
        Map<String, Long> table = null;

        Gson gson = new Gson();
        JsonReader jsonReader = FileReader.read(tableFile);
        if (jsonReader != null)
            table = gson.fromJson(jsonReader, type);

        load_date = new Date();

        return table;
    }
}


