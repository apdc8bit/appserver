package apdc.output.info;

public class SettingsInfo {
    public final Boolean isColorBlind;

    public SettingsInfo(Boolean isColorBlind) {
        this.isColorBlind = isColorBlind;
    }
}
