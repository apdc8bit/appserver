package apdc.output.info;

import java.util.Date;
import java.util.List;

public class ReportInfo {
    public final List<String> type;
    public final List<String> users;
    public final long numb_reports;
    public final Date first_report_date;
    public final Date last_report_date;
    public final OccurrenceInfo occurrence;

    public ReportInfo(OccurrenceInfo occurrence, List<String> users, long numb_reports, Date first_report_date, Date last_report_date, List<String> type) {
        this.occurrence = occurrence;
        this.users = users;
        this.numb_reports = numb_reports;
        this.first_report_date = first_report_date;
        this.last_report_date = last_report_date;
        this.type = type;
    }

}
