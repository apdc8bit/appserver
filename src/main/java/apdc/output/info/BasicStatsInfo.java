package apdc.output.info;

import apdc.gamification.PointSystem;
import apdc.gamification.Values;
import apdc.input.verification.values.OccurrenceValues;
import apdc.input.verification.values.UserValues;
import apdc.resources.occurrences.Management;
import apdc.tables.user.User;
import com.google.appengine.api.datastore.*;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public class BasicStatsInfo {
    public static final String kind = "Stats", key = "BasicStats";
    private final static DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
    private final static Logger LOG = Logger.getLogger(Management.class.getName());
    private final static Gson g = new Gson();
    private static long id;

    private final Entity stats;

    public enum Stats {
        totalNumberUsers, numberBasicUsers, numberWorkersUsers, numberEntitiesUsers,
        totalNumberOccurrences, occurrencesOnHoldTotal, occurrencesInProgressTotal, occurrencesCompletedTotal,
        occurrencesCreatedLastWeek, occurrencesCreatedYesterday, occurrencesStartedLastWeek, occurrencesCompletedLastWeek,

        averageOccurrenceCompletionTime,
        averageOccurrenceResponseTime,

        topOccurrencesLast5Days,
        userMostOccurrences,
        top5UsersByLevel,
        usersRegisteredYesterday, userRegisteredLastWeek, getUsersRegisteredLastMonth,

        maxFireRisk, minFireRisk, avgFireRisk, atRiskCounties,

        loginsLastDay, loginsLastWeek,

        totalVotes, totalComments, votesLastWeek, commentsLasWeek
    }

    public BasicStatsInfo() {
        FetchOptions fo = FetchOptions.Builder.withLimit(1);
        Query ctrQuery = new Query(kind).addSort("id", Query.SortDirection.DESCENDING);
        List<Entity> results = datastore.prepare(ctrQuery).asList(fo);

        if (!results.isEmpty()) {
            id = (long) results.get(0).getProperty("id");
            id++;
            LOG.info("Creating new stats: " + id);
        } else {
            LOG.info("First stats");
            id = 0L;
        }

        stats = new Entity(kind, key + id);
    }

    public void setStat(Stats stat, long value) {
        stats.setProperty(stat.name(), value);
    }

    public void setStat(Stats stat, List<Key> value) {
        stats.setProperty(stat.name(), value);
    }

    public void setStat(Stats stat, Key value) {
        stats.setProperty(stat.name(), value);
    }

    public void setStatL(Stats stat, List<String> value) {
        stats.setProperty(stat.name(), value);
    }

    public void setStatEE(Stats stat, List<EmbeddedEntity> value) {
        stats.setProperty(stat.name(), value);
    }

    public void submit() {
        stats.setProperty("date", new Date());
        stats.setProperty("id", id);
        datastore.put(stats);
    }

    public static void addLong(JsonObject json, Stats stat, Map<String, Object> stats) {
        long value = (long) stats.get(stat.name());
        json.addProperty(stat.name(), value);
    }

    @SuppressWarnings("unchecked")
    public static void addCounties(JsonObject json, Stats stat, Map<String, Object> stats) {
        List<EmbeddedEntity> values = (List<EmbeddedEntity>) stats.get(stat.name());

        List<JsonObject> list = new LinkedList<>();
        for (EmbeddedEntity info : values) {
            JsonObject jsonCounty = new JsonObject();
            jsonCounty.addProperty("county", (String) info.getProperty("county"));
            jsonCounty.addProperty("dico", (String) info.getProperty("dico"));
            list.add(jsonCounty);
        }

        json.add(stat.name(), g.toJsonTree(list));
    }

    @SuppressWarnings("unchecked")
    public static void addOccurrence(JsonObject json, Stats stat, Map<String, Object> stats) {
        List<Key> occKeys = (List<Key>) stats.get(stat.name());
        List<Entity> occE = new LinkedList<>();

        if (occKeys != null)
            for (Key k : occKeys) {
                try {
                    Entity occ = datastore.get(k);
                    occE.add(occ);
                } catch (EntityNotFoundException e) {
                    LOG.warning("I did not find the occurrence, I DID NOT! ");
                }
            }

        json.add(stat.name(), g.toJsonTree(OccurrenceValues.getListWithUserVote(occE, null)));
    }

    public static void addUser(JsonObject json, Stats stat, Map<String, Object> stats) {
        Key userKey = (Key) stats.get(stat.name());

        try {
            Entity user = datastore.get(userKey);
            json.add(stat.name(), g.toJsonTree(UserValues.getBasicUserInfo(user)));
        } catch (EntityNotFoundException e) {
            LOG.warning("Stats: User not found");
        }
    }

    @SuppressWarnings("unchecked")
    public static void addUsers(JsonObject json, Stats stat, Map<String, Object> stats) {
        List<Key> userKeys = (List<Key>) stats.get(stat.name());
        List<JsonObject> userInfo = new LinkedList<>();

        for (Key k : userKeys) {
            try {
                Entity user = datastore.get(k);
                JsonObject userInfoS = new JsonObject();
                userInfoS.addProperty("username", k.getName());
                userInfoS.addProperty(User.email, (String) user.getProperty(User.email));

                String imageID = (String) user.getProperty(User.imageID);
                if (imageID != null && !imageID.isEmpty())
                    userInfoS.addProperty(User.imageID, imageID);

                Values values = PointSystem.getPointsAndLevel(k.getName());
                userInfoS.addProperty("points", values.getPoints());
                long level = values.getLevel();
                userInfoS.addProperty("level", level);
                userInfo.add(userInfoS);
            } catch (EntityNotFoundException e) {
                LOG.warning("User not found: " + k);
            }
        }

        json.add(stat.name(), g.toJsonTree(userInfo));
    }

}
