package apdc.output.info;

public class BasicUserInfo {
    public final String username;
    public String imageID;
    public Number points, level, next_level_points, curr_level_points;

    public BasicUserInfo(String username, String imageID, Number points, Number level, Number next_level_points, Number curr_level_points) {
        this.username = username;
        this.imageID = imageID;
        this.points = points;
        this.level = level;
        this.next_level_points = next_level_points;
        this.curr_level_points = curr_level_points;
    }
}
