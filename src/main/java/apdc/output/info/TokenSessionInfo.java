package apdc.output.info;

import apdc.datastore.Datastore;
import apdc.gamification.PointSystem;
import apdc.gamification.Values;
import apdc.permissions.UserPermissions;
import apdc.tables.user.User;
import com.google.appengine.api.datastore.Entity;

public class TokenSessionInfo {
    public final String token;
    public final String session;
    public final String email;
    public String imageID;
    public String role;
    public Boolean valid;
    public Number points, level, next_level_points, curr_level_points;
    public Boolean isVolunteer;

    public TokenSessionInfo(String token, String session, String email, String imageID, String role, Boolean valid, Number points, Number level) {
        this.token = token;
        this.session = session;
        this.email = email;
        this.imageID = imageID;
        this.role = role;
        this.valid = valid;
        this.points = points;
        this.level = level;
    }

    public TokenSessionInfo(Entity user, String token, String session) {
        this.token = token;
        this.session = session;
        this.email = (String) user.getProperty(User.email);

        if (!Datastore.getFrom(user).aString(User.role).equals(UserPermissions.ROLE.u_admin.name())) {
            this.imageID = (String) user.getProperty(User.imageID);
            this.role = (String) user.getProperty(User.role);
            this.valid = (Boolean) user.getProperty(User.valid);
        }

        if (Datastore.getFrom(user).aString(User.role).equals(UserPermissions.ROLE.u_basic.name())) {
            Values values = PointSystem.getPointsAndLevel(user.getKey().getName());
            this.points = values.getPoints();
            this.level = values.getLevel();
            this.next_level_points = PointSystem.pointsForLevelUp((long) level + 1);
            this.curr_level_points = PointSystem.pointsForLevelUp((long) level);
        }

        if (!Datastore.getFrom(user).aString(User.role).equals(UserPermissions.ROLE.u_entity.name())) {
            this.isVolunteer = Datastore.getFrom(user).aBoolean(User.volunteer);
        }

    }
}
