package apdc.output.info;

public class WorkerStats {

    public final Long avgTimeFinish;
    public final Long numbJobsStarted;
    public final Long numJobsFinish;
    public final Long numbJobsWaiting;

    public WorkerStats(Long avgTimeFinish, Long numbJobsStarted, Long numJobsFinish, Long numbJobsWaiting) {
        this.avgTimeFinish = avgTimeFinish;
        this.numbJobsStarted = numbJobsStarted;
        this.numJobsFinish = numJobsFinish;
        this.numbJobsWaiting = numbJobsWaiting;
    }
}
