package apdc.output.info;

public class CountyInfo {

    public final String county;
    public final String dico;

    public CountyInfo(String county, String dico) {
        this.county = county;
        this.dico = dico;
    }
}
