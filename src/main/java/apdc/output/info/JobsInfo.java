package apdc.output.info;

import java.util.Date;

public class JobsInfo {
    public final String worker_username;
    public final String occurrence_username;
    public final String state;
    public final String imageID;
    public final long occurrence_id;
    public final Date date_added;
    public final Date date_finished;
    public final OccurrenceInfo occurrence;

    public JobsInfo(String worker_username, String occurrence_username, long occurrence_id, Date date_added, Date date_finished, String state, String imageID, OccurrenceInfo occurrence) {
        this.worker_username = worker_username;
        this.occurrence_username = occurrence_username;
        this.occurrence_id = occurrence_id;
        this.date_added = date_added;
        this.date_finished = date_finished;
        this.state = state;
        this.occurrence = occurrence;
        this.imageID = imageID;
    }


}
