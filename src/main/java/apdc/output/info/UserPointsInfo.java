package apdc.output.info;

public class UserPointsInfo {
    public final long points;
    public final long level;
    public final long next_level_points;
    public final long curr_level_points;

    public UserPointsInfo(long points, long level, long next_level_points, long curr_level_points) {
        this.points = points;
        this.level = level;
        this.next_level_points = next_level_points;
        this.curr_level_points = curr_level_points;
    }
}
