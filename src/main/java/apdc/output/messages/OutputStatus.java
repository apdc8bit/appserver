package apdc.output.messages;

import apdc.input.verification.utils.Message;

public class OutputStatus {

    //OK
    public static final String
            REPLY_OK = new Message(Messages.OK, 2323).getJson();
    public static final String USER_REGISTERED = new Message(Messages.USER_REGISTERED, 70).getJson();
    public static final String EMAIL_CHANGED = new Message(Messages.EMAIL_CHANGED, 71).getJson();
    public static String INFO_CHANGED = new Message(Messages.INFO_CHANGED, 72).getJson();
    public static final String INFO_DELETED = new Message(Messages.INFO_DELETED, 73).getJson();
    public static final String USER_MARKED_DELETION = new Message(Messages.USER_MARKED_DELETION, 76).getJson();
    public static final String PASSWORD_CHANGED = new Message(Messages.PASSWORD_CHANGED, 78).getJson();
    public static final String SETTINGS_UPDATED = new Message(Messages.SETTINGS_UPDATED, 33).getJson();
    public static final String LOGOUT_OK = new Message(Messages.LOGOUT_OK, 23).getJson();
    public static final String PASSWORD_RECOVERED = new Message(Messages.PASSWORD_RECOVERED, 24).getJson();

    //ERROR
    public static final String
            EMAIL_RESENT = new Message(Messages.EMAIL_RESENT, 44).getJson();
    public static final String EMAIL_VALIDATED = new Message(Messages.EMAIL_VALIDATED, 0).getJson();
    public static final String INVALID_TOKEN = new Message(Messages.INVALID_TOKEN, 1).getJson();
    public static final String EXPIRED_TOKEN = new Message(Messages.EXPIRED_TOKEN, 2).getJson();
    public static final String NO_USER_FOUND = new Message(Messages.USER_DOES_NOT_EXIST, 3).getJson();
    public static final String WRONG_DATA = new Message(Messages.MISSING_OR_WRONG_DATA, 4).getJson();
    public static final String NOT_VALID = new Message(Messages.USER_NOT_VALID, 60).getJson();
    public static final String NOT_A_ROLE = new Message(Messages.USER_ROLE_DOES_NOT_EXIST, 61).getJson();
    public static final String NOT_A_DEVICE = new Message(Messages.WRONG_DEVICE_ID, 62).getJson();
    public static final String NO_PERMISSION = new Message(Messages.USER_NO_PERMISSION, 63).getJson();
    public static final String USERNAME_EXISTS = new Message(Messages.USERNAME_EXISTS, 40).getJson();
    public static final String USERNAME_NOT_EXIST = new Message(Messages.USERNAME_DOES_NOT_EXIST, 41).getJson();
    public static final String EMAIL_EXISTS = new Message(Messages.EMAIL_EXISTS, 42).getJson();
    public static final String EMAIL_NOT_EXIST = new Message(Messages.EMAIL_DOES_NOT_EXIST, 43).getJson();
    public static final String PASSWORDS_MATCH = new Message(Messages.PASSWORDS_MATCH, 74).getJson();
    public static final String PASSWORDS_DO_NOT_MATCH = new Message(Messages.PASSWORDS_DO_NOT_MATCH, 75).getJson();
    public static final String PASSWORD_MATCHES_OLD = new Message(Messages.PASSWORD_MATCHES_OLD, 77).getJson();
    public static final String INVALID_KEY = new Message(Messages.INVALID_KEY, 88).getJson();
    public static final String EXPIRED_KEY = new Message(Messages.EXPIRED_KEY, 99).getJson();
    public static final String USER_TIMEOUT = new Message(Messages.USER_IS_IN_TIMEOUT, 83).getJson();
    public static final String UNAUTHORIZED_COUNTRY = new Message(Messages.UNAUTHORIZED_COUNTRY, 84).getJson();
    public static final String ADMIN_FAILED = new Message(Messages.NOT_ALLOWED, 85).getJson();
    public static final String WRONG_DEVICE = new Message(Messages.WRONG_DEVICE, 86).getJson();
    public static final String USER_PASS_INC = new Message(Messages.USERNAME_PASSWORD_INCORRECT, 87).getJson();
    public static final String USERNAME_EMAIL_NO_MATCH = new Message(Messages.USERNAME_EMAIL_NO_MATCH, 87).getJson();
    public static final String PASSWORD_ALREADY_USED = new Message(Messages.PASSWORD_ALREADY_USED, 88).getJson();
    public static final String USER_ROLE_NOT_ALLOWED = new Message(Messages.USER_ROLE_NOT_ALLOWED, 123).getJson();
    public static final String ENTITY_NOT_VALID = new Message(Messages.ENTITY_NOT_VALID, 8080).getJson();


    public enum Messages {
        OK,
        USER_ROLE_NOT_ALLOWED,
        USERNAME_EXISTS,
        ENTITY_NOT_VALID,
        EMAIL_EXISTS,
        USERNAME_DOES_NOT_EXIST,
        EMAIL_DOES_NOT_EXIST,
        USER_REGISTERED,
        INFO_CHANGED,
        PASSWORDS_DO_NOT_MATCH,
        PASSWORDS_MATCH,
        EMAIL_RESENT,
        PASSWORD_MATCHES_OLD,
        PASSWORD_CHANGED,
        UNAUTHORIZED_COUNTRY,
        NOT_ALLOWED,
        SETTINGS_UPDATED,
        WRONG_DEVICE,
        USERNAME_PASSWORD_INCORRECT,
        PASSWORD_RECOVERED,
        USERNAME_EMAIL_NO_MATCH,
        JOB_NOT_TODO,
        ALREADY_VALIDATED,
        INVALID_TOKEN,
        REPORT_NOT_FOUND,
        REPORT_CONFIRMED,
        EXPIRED_TOKEN,
        EXPIRED_KEY,
        EXPIRED_PASSWORD_KEY,
        INVALID_PASSWORD_KEY,
        EMAIL_VALIDATED,
        IN_WATER,
        USER_IS_IN_TIMEOUT,
        NOT_FROM_PT,
        WORKER_IS_NOT_FROM_ENTITY,
        JOB_CONFIRMED,
        JOB_NOT_CONFIRMED,
        REPORT_SUBMITTED,
        KEY_SENT,
        INVALID_KEY,
        INFO_DELETED,
        LOGOUT_OK,
        OCCURRENCE_STATUS_HAS_CHANGED,
        USER_PASS_INC,
        USER_REG,
        USER_EXISTS,
        PASSWORD_ALREADY_USED,
        MISSING_OR_WRONG_DATA,
        PASS_CHANGE,
        PASS_REC,
        MARKED_COMPLETED,
        COULD_NOT_AUTHORIZE,
        USER_EMAIL,
        INFO_ADD,
        OCCURRENCE_EXISTS,
        OCCURRENCE_REGISTERED,
        OCCURRENCE_EDITED,
        OCCURRENCE_REMOVED,
        OCCURRENCE_DOES_NOT_EXIST,
        USER_NO_PERMISSION,
        STATUS_UPDATED,
        USER_DELETED,
        TYPE_UPDATED,
        USER_MARKED_DELETION,
        USER_DOES_NOT_EXIST,
        JOB_DOES_NOT_EXIST,
        OCCURRENCE_IS_NOT_ON_HOLD,
        VOTE_SUCCEEDED,
        VOTE_REMOVED,
        NO_VOTE_FOUND,
        USER_NOT_OWNER,
        OCCURRENCE_ASSOCIATED,
        OCCURRENCE_ASSOCIATION_REMOVED,
        OCCURRENCE_ALREADY_ASSOCIATED,
        WORKER_EDITED,
        WORKER_NOT_ASSOCIATED,
        PRIORITY_UPDATED,
        REQUEST_ALREADY_EXISTS,
        ROLE_CHANGED,
        ALREADY_VOTED,
        COMMENT_ADDED,
        COMMENT_EDITED,
        COMMENT_DELETED,
        COMMENT_DOES_NOT_EXIST,
        EMAIL_CHANGED,
        USER_NOT_VALID,
        USER_ROLE_DOES_NOT_EXIST,
        WRONG_DEVICE_ID
    }
}
