package apdc.resources.stats;

import apdc.output.info.BasicStatsInfo;
import apdc.resources.occurrences.Management;
import com.google.appengine.api.datastore.*;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Permite consultar estatisticas gerais da aplicação
 */
@Path("/{device}/globalStats")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class GlobalStatisticsResource {
    private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
    private final Gson g = new Gson();
    private static final Logger LOG = Logger.getLogger(Management.class.getName());

    @GET
    @Path("/getBasicStats")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getBasicStats(@QueryParam("days") int days) {
        LOG.info("Returning Basic Stats");

        int actualDays = days > 7 || days < 1 ? 1 : days;

        FetchOptions fo = FetchOptions.Builder.withLimit(actualDays);
        Query ctrQuery = new Query(BasicStatsInfo.kind).addSort("id", Query.SortDirection.DESCENDING);
        List<Entity> results = datastore.prepare(ctrQuery).asList(fo);

        List<JsonObject> jsonOgj = new LinkedList<>();

        for (Entity basicStatsE : results) {
            JsonObject json = new JsonObject();
            Map<String, Object> basicStats = basicStatsE.getProperties();

            BasicStatsInfo.addOccurrence(json, BasicStatsInfo.Stats.topOccurrencesLast5Days, basicStats);

            BasicStatsInfo.addUser(json, BasicStatsInfo.Stats.userMostOccurrences, basicStats);

            BasicStatsInfo.addUsers(json, BasicStatsInfo.Stats.top5UsersByLevel, basicStats);

            BasicStatsInfo.addLong(json, BasicStatsInfo.Stats.totalNumberUsers, basicStats);
            BasicStatsInfo.addLong(json, BasicStatsInfo.Stats.numberBasicUsers, basicStats);
            BasicStatsInfo.addLong(json, BasicStatsInfo.Stats.numberWorkersUsers, basicStats);
            BasicStatsInfo.addLong(json, BasicStatsInfo.Stats.numberEntitiesUsers, basicStats);

            BasicStatsInfo.addLong(json, BasicStatsInfo.Stats.usersRegisteredYesterday, basicStats);
            BasicStatsInfo.addLong(json, BasicStatsInfo.Stats.userRegisteredLastWeek, basicStats);
            BasicStatsInfo.addLong(json, BasicStatsInfo.Stats.getUsersRegisteredLastMonth, basicStats);

            BasicStatsInfo.addLong(json, BasicStatsInfo.Stats.totalNumberOccurrences, basicStats);
            BasicStatsInfo.addLong(json, BasicStatsInfo.Stats.occurrencesOnHoldTotal, basicStats);
            BasicStatsInfo.addLong(json, BasicStatsInfo.Stats.occurrencesInProgressTotal, basicStats);
            BasicStatsInfo.addLong(json, BasicStatsInfo.Stats.occurrencesCompletedTotal, basicStats);
            BasicStatsInfo.addLong(json, BasicStatsInfo.Stats.occurrencesCreatedLastWeek, basicStats);
            BasicStatsInfo.addLong(json, BasicStatsInfo.Stats.occurrencesCreatedYesterday, basicStats);
            BasicStatsInfo.addLong(json, BasicStatsInfo.Stats.occurrencesStartedLastWeek, basicStats);
            BasicStatsInfo.addLong(json, BasicStatsInfo.Stats.occurrencesCompletedLastWeek, basicStats);
            BasicStatsInfo.addLong(json, BasicStatsInfo.Stats.averageOccurrenceCompletionTime, basicStats);
            BasicStatsInfo.addLong(json, BasicStatsInfo.Stats.averageOccurrenceResponseTime, basicStats);
            BasicStatsInfo.addLong(json, BasicStatsInfo.Stats.occurrencesCreatedLastWeek, basicStats);

            BasicStatsInfo.addLong(json, BasicStatsInfo.Stats.minFireRisk, basicStats);
            BasicStatsInfo.addLong(json, BasicStatsInfo.Stats.maxFireRisk, basicStats);
            BasicStatsInfo.addLong(json, BasicStatsInfo.Stats.avgFireRisk, basicStats);
            BasicStatsInfo.addCounties(json, BasicStatsInfo.Stats.atRiskCounties, basicStats);

            BasicStatsInfo.addLong(json, BasicStatsInfo.Stats.loginsLastDay, basicStats);
            BasicStatsInfo.addLong(json, BasicStatsInfo.Stats.loginsLastWeek, basicStats);

            BasicStatsInfo.addLong(json, BasicStatsInfo.Stats.totalVotes, basicStats);
            BasicStatsInfo.addLong(json, BasicStatsInfo.Stats.totalComments, basicStats);
            BasicStatsInfo.addLong(json, BasicStatsInfo.Stats.votesLastWeek, basicStats);
            BasicStatsInfo.addLong(json, BasicStatsInfo.Stats.commentsLasWeek, basicStats);

            json.addProperty("id", (long) basicStats.get("id"));
            json.addProperty("date", basicStats.get("date").toString());

            jsonOgj.add(json);
        }

        return Response.ok(g.toJson(jsonOgj)).build();
    }

}
