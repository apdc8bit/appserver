package apdc.resources.occurrences;

import apdc.gamification.PointSystem;
import apdc.input.data.occurrences.ReportOccurrenceData;
import apdc.input.data.occurrences.comments.CommentIDData;
import apdc.input.data.occurrences.comments.CommentOccurrenceData;
import apdc.input.data.occurrences.comments.EditCommentData;
import apdc.input.data.occurrences.votes.VoteOccurrenceData;
import apdc.input.verification.utils.InputDataVerification;
import apdc.input.verification.utils.Reply;
import apdc.input.verification.utils.RequestDataVerification;
import apdc.input.verification.values.NotificationValues;
import apdc.input.verification.values.OccurrenceValues;
import apdc.output.messages.OutputStatus;
import apdc.permissions.Actions;
import apdc.resources.api.OccurrenceResources;
import apdc.tables.occurrence.*;
import apdc.tables.user.User;
import com.google.appengine.api.datastore.*;
import com.google.gson.JsonObject;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import static apdc.resources.api.Base.*;

@Path("/{device}/occurrence")
public class Interaction implements OccurrenceResources.InteractionI {

    public Interaction() {
    }

    @Override
    public Response voteOccurrence(VoteOccurrenceData data, String device, String tokenID, String sessionID) {
        Reply reply = RequestDataVerification.validateDataTokenPermission(data, tokenID, sessionID, device, Actions.vote_occurrence);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        LOG.info("Attempting to add a like to occurrence");

        TransactionOptions options = TransactionOptions.Builder.withXG(true);
        Transaction txn = datastore.beginTransaction(options);

        Key userKey = reply.getUser().getKey();
        Key userOcc = KeyFactory.createKey(User.kind, data.username);

        try {
            //Get Occurrence
            long id = data.id;
            Key occurrenceKey = KeyFactory.createKey(userOcc, Occurrences.kind, id);
            String lKey = KeyFactory.keyToString(occurrenceKey).concat(KeyFactory.keyToString(userKey));
            Entity occ = datastore.get(occurrenceKey);

            //Create vote key
            Key voteKey = KeyFactory.createKey(Votes.kind, lKey);

            try {
                //Check if user has already voted
                datastore.get(voteKey);
                LOG.warning("user already voted");
                txn.rollback();
                return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson(OutputStatus.Messages.ALREADY_VOTED)).build();
            } catch (EntityNotFoundException e) {
                //Add vote
                Entity votes = new Entity(Votes.kind, lKey);
                votes.setProperty(Votes.occurrenceKey, occurrenceKey);
                votes.setProperty(Votes.userKey, userKey);
                votes.setProperty(Votes.added_date, new Date());
                datastore.put(txn, votes);
            }

            //Create vote count key
            Key vcCountKey = KeyFactory.createKey(VotesCommentsCount.kind, id);
            try {
                //Check if exists and updates value
                Entity vcEntity = datastore.get(vcCountKey);
                long nLikes = InputDataVerification.getLongValue((Long) vcEntity.getProperty(VotesCommentsCount.likeCount));
                vcEntity.setProperty(VotesCommentsCount.likeCount, ++nLikes);
                vcEntity.setProperty(VotesCommentsCount.lastInteractionTime, new Date());
                datastore.put(txn, vcEntity);
            } catch (EntityNotFoundException e) {
                //Creates new entity
                Entity vcEntity = new Entity(VotesCommentsCount.kind, id);
                vcEntity.setProperty(VotesCommentsCount.likeCount, 1L);
                vcEntity.setProperty(VotesCommentsCount.commentCount, 0L);
                vcEntity.setProperty(VotesCommentsCount.occurrenceKey, occurrenceKey);
                vcEntity.setProperty(VotesCommentsCount.lastInteractionTime, new Date());
                vcEntity.setProperty(VotesCommentsCount.occurrenceCreationTime, occ.getProperty(Occurrences.creation_time));
                datastore.put(txn, vcEntity);
            }

            txn.commit();

            //Add points for voting on occurrence
            boolean levelUP = PointSystem.addPoints(PointSystem.ACTIONS.vote_occurrence, userKey.getName());
            JsonObject json = new JsonObject();
            json.addProperty("levelUP", levelUP);

            return Response.ok(g.toJson(json)).build();
        } catch (EntityNotFoundException e) {
            LOG.warning("occurrence doesn't exist.");

            txn.rollback();
            return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson(OutputStatus.Messages.OCCURRENCE_DOES_NOT_EXIST)).build();
        } finally {
            if (txn != null && txn.isActive()) {
                txn.rollback();
                LOG.severe("SERVER ERROR");
            }
        }
    }

    @Override
    public Response deleteLikeOccurrence(VoteOccurrenceData data, String device, String tokenID, String sessionID) {
        Reply reply = RequestDataVerification.validateDataTokenPermission(data, tokenID, sessionID, device, Actions.delete_vote);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        LOG.info("Attempting to add a like to occurrence");

        TransactionOptions options = TransactionOptions.Builder.withXG(true);
        Transaction txn = datastore.beginTransaction(options);

        Key userKey = reply.getUser().getKey();
        Key userOcc = KeyFactory.createKey(User.kind, data.username);

        try {
            //Check if occurrence exists
            long id = data.id;
            Key occurrenceKey = KeyFactory.createKey(userOcc, Occurrences.kind, id);
            datastore.get(occurrenceKey);


            String voteKeyString = KeyFactory.keyToString(occurrenceKey).concat(KeyFactory.keyToString(userKey));
            Key voteKey = KeyFactory.createKey(Votes.kind, voteKeyString);

            try {
                //Check if vote exists
                Entity votes = datastore.get(voteKey);
                Key owner = (Key) votes.getProperty(Votes.userKey);

                //Check if owner matches user login
                if (owner.compareTo(userKey) != 0) {
                    LOG.warning("User is not the owner of the vote");
                    txn.rollback();
                    return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson(OutputStatus.Messages.USER_NOT_OWNER)).build();
                }

                datastore.delete(txn, voteKey);
            } catch (EntityNotFoundException e) {
                LOG.warning("No vote found");
                txn.rollback();
                return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson(OutputStatus.Messages.NO_VOTE_FOUND)).build();
            }

            Key vcCountKey = KeyFactory.createKey(VotesCommentsCount.kind, id);

            try {
                //Check if vote count exists and updates value
                Entity vcEntity = datastore.get(vcCountKey);
                long nLikes = InputDataVerification.getLongValue((Long) vcEntity.getProperty(VotesCommentsCount.likeCount));
                vcEntity.setProperty(VotesCommentsCount.likeCount, --nLikes);
                vcEntity.setProperty(VotesCommentsCount.lastInteractionTime, new Date());
                datastore.put(txn, vcEntity);
            } catch (EntityNotFoundException e) {
                LOG.warning("No vote found");
                txn.rollback();
                return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson(OutputStatus.Messages.NO_VOTE_FOUND)).build();
            }

            //Remove points for removing vote on occurrence
            PointSystem.removePoints(PointSystem.ACTIONS.vote_occurrence, userKey.getName());

            txn.commit();
            return Response.ok(g.toJson(OutputStatus.Messages.VOTE_REMOVED)).build();
        } catch (EntityNotFoundException e) {
            LOG.warning("Occurrence doesn't exist.");

            txn.rollback();
            return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson(OutputStatus.Messages.OCCURRENCE_DOES_NOT_EXIST)).build();
        } finally {
            if (txn != null && txn.isActive()) {
                txn.rollback();
                LOG.severe("SERVER ERROR");
            }
        }
    }

    @Override
    public Response commentOccurrence(CommentOccurrenceData data, String device, String tokenID, String sessionID) {
        Reply reply = RequestDataVerification.validateDataTokenPermission(data, tokenID, sessionID, device, Actions.comment_occurrence);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        LOG.info("Attempting to add a comment to occurrence");

        TransactionOptions options = TransactionOptions.Builder.withXG(true);
        Transaction txn = datastore.beginTransaction(options);

        Key userKey = reply.getUser().getKey();
        Key userOcc = KeyFactory.createKey(User.kind, data.username);

        try {
            //Check if occurrence exists
            long id = data.id;
            Key occurrenceKey = KeyFactory.createKey(userOcc, Occurrences.kind, id);
            Entity occ = datastore.get(occurrenceKey);

            //Creates a new comment entity
            Entity comments = new Entity(Comments.kind); //cria uma chave automaticamente (valor numerico)
            comments.setProperty(Comments.occurrenceKey, occurrenceKey);
            comments.setProperty(Comments.userKey, userKey);
            comments.setProperty(Comments.comment, data.comment);
            Date date = new Date();
            comments.setProperty(Comments.creation_time, date);
            comments.setProperty(Comments.last_edit_time, date);

            datastore.put(txn, comments);

            Key vcCountKey = KeyFactory.createKey(VotesCommentsCount.kind, id);
            try {
                //Check if comment count exists and updates value
                Entity vcEntity = datastore.get(vcCountKey);
                long nComments = InputDataVerification.getLongValue((Long) vcEntity.getProperty(VotesCommentsCount.commentCount));
                vcEntity.setProperty(VotesCommentsCount.commentCount, ++nComments);
                vcEntity.setProperty(VotesCommentsCount.lastInteractionTime, new Date());
                datastore.put(txn, vcEntity);
            } catch (EntityNotFoundException e) {
                //Creates new count entity
                Entity vcEntity = new Entity(VotesCommentsCount.kind, id);
                vcEntity.setProperty(VotesCommentsCount.commentCount, 1L);
                vcEntity.setProperty(VotesCommentsCount.likeCount, 0L);
                vcEntity.setProperty(VotesCommentsCount.occurrenceKey, occurrenceKey);
                vcEntity.setProperty(VotesCommentsCount.lastInteractionTime, new Date());
                vcEntity.setProperty(VotesCommentsCount.occurrenceCreationTime, occ.getProperty(Occurrences.creation_time));
                datastore.put(txn, vcEntity);
            }

            txn.commit();

            NotificationValues.occurrenceCommentNotification(occ, userOcc.getName(), userKey.getName(), data.comment);

            //Add points for voting on occurrence
            boolean levelUP = PointSystem.addPoints(PointSystem.ACTIONS.vote_occurrence, userKey.getName());
            JsonObject json = new JsonObject();
            json.addProperty("levelUP", levelUP);

            return Response.ok(g.toJson(OutputStatus.Messages.COMMENT_ADDED)).build();
        } catch (EntityNotFoundException e) {
            LOG.warning("Occurrence doesn't exist.");

            txn.rollback();
            return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson(OutputStatus.Messages.OCCURRENCE_DOES_NOT_EXIST)).build();
        } finally {
            if (txn != null && txn.isActive()) {
                txn.rollback();
                LOG.severe("SERVER ERROR");
            }
        }
    }

    @Override
    public Response deleteCommentOccurrence(CommentIDData data, String device, String tokenID, String sessionID) {
        Reply reply = RequestDataVerification.validateDataTokenPermission(data, tokenID, sessionID, device, Actions.edit_comment);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        LOG.info("Attempting to add a like to occurrence");

        TransactionOptions options = TransactionOptions.Builder.withXG(true);
        Transaction txn = datastore.beginTransaction(options);

        Key userKey = reply.getUser().getKey();
        Key commentKey = KeyFactory.createKey(Comments.kind, data.comment_id);

        try {
            //Check if comment exists
            Entity comments = datastore.get(commentKey);
            Key owner = (Key) comments.getProperty(Comments.userKey);

            //Check if owner is user login
            if (owner.compareTo(userKey) == 0) {
                //Updates comment count and deletes comment
                Key occKey = (Key) comments.getProperty(Comments.occurrenceKey);
                Key vcCountKey = KeyFactory.createKey(VotesCommentsCount.kind, occKey.getId());

                Entity vcEntity = datastore.get(vcCountKey);

                long nComments = InputDataVerification.getLongValue((Long) vcEntity.getProperty(VotesCommentsCount.commentCount));
                vcEntity.setProperty(VotesCommentsCount.commentCount, --nComments);
                vcEntity.setProperty(VotesCommentsCount.lastInteractionTime, new Date());

                //Delete comment
                datastore.delete(commentKey);
                datastore.put(txn, vcEntity);
            } else {
                LOG.warning("User is not the owner of the comment");

                txn.rollback();
                return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson(OutputStatus.Messages.USER_NOT_OWNER)).build();
            }

            PointSystem.removePoints(PointSystem.ACTIONS.comment_occurrence, userKey.getName());

            txn.commit();
            return Response.ok(g.toJson(OutputStatus.Messages.COMMENT_DELETED)).build();
        } catch (EntityNotFoundException e) {
            LOG.warning("Comment doesn't exist.");
            return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson(OutputStatus.Messages.COMMENT_DOES_NOT_EXIST)).build();
        } finally {
            if (txn != null && txn.isActive()) {
                txn.rollback();
                LOG.severe("SERVER ERROR");
            }
        }
    }


    @Override
    public Response editCommentOccurrence(EditCommentData data, String device, String tokenID, String sessionID) {
        Reply reply = RequestDataVerification.validateDataTokenPermission(data, tokenID, sessionID, device, Actions.delete_comment);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        LOG.info("Attempting to add a like to occurrence");

        TransactionOptions options = TransactionOptions.Builder.withXG(true);
        Transaction txn = datastore.beginTransaction(options);

        Key userKey = reply.getUser().getKey();
        Key commentKey = KeyFactory.createKey(Comments.kind, data.comment_id);

        try {
            //Check if comment exists
            Entity comments = datastore.get(commentKey);

            Key owner = (Key) comments.getProperty(Comments.userKey);

            //Check if owner matches user login
            if (owner.compareTo(userKey) == 0) {
                //Check if new comment is different than new one
                if (!comments.getProperty(Comments.comment).equals(data.comment)) {
                    comments.setProperty(Comments.comment, data.comment);
                    comments.setProperty(Comments.last_edit_time, new Date());
                    datastore.put(txn, comments);
                }
            } else {
                LOG.warning("user is not the owner of the comment");

                txn.rollback();
                return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson(OutputStatus.Messages.USER_NOT_OWNER)).build();
            }

            txn.commit();
            return Response.ok(g.toJson(OutputStatus.Messages.COMMENT_EDITED)).build();
        } catch (EntityNotFoundException e) {
            LOG.warning("Occurrence doesn't exist.");

            txn.rollback();
            return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson(OutputStatus.Messages.OCCURRENCE_DOES_NOT_EXIST)).build();
        } finally {
            if (txn != null && txn.isActive()) {
                txn.rollback();
                LOG.severe("SERVER ERROR");
            }
        }
    }

    @SuppressWarnings("unchecked")
    public Response reportOccurrence(ReportOccurrenceData data, String device, String tokenID, String sessionID) {
        Reply reply = RequestDataVerification.validateTokenPermission(tokenID, sessionID, device, Actions.report_occurrence);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        TransactionOptions options = TransactionOptions.Builder.withXG(true);
        Transaction txn = datastore.beginTransaction(options);

        Key occOwner = KeyFactory.createKey(User.kind, data.username);
        Key occKey = KeyFactory.createKey(occOwner, Occurrences.kind, data.id);

        try {
            //Check if occurrence exists
            datastore.get(occKey);

            Key reportKey = KeyFactory.createKey(Reports.kind, occKey.getId());

            try {
                //Check if a report for occurrence exists and updates value
                Entity report = datastore.get(reportKey);

                List<Key> usersList = (List<Key>) report.getProperty(Reports.usersList);

                //Check if user already reported
                if (usersList.contains(reply.getUser().getKey())) {
                    LOG.warning("User already reported");
                    txn.rollback();
                    //Does not send a error
                    return Response.ok(g.toJson(OutputStatus.Messages.REPORT_SUBMITTED)).build();
                }

                usersList.add(reply.getUser().getKey());
                report.setProperty(Reports.usersList, usersList);
                long count = InputDataVerification.getLongValue((Long) report.getProperty(Reports.count));
                report.setProperty(Reports.count, ++count);
                report.setProperty(Reports.last_report_date, new Date());
                HashSet<String> reportTypes = (HashSet<String>) report.getProperty(Reports.type);
                reportTypes.add(OccurrenceValues.getReportValue(data.report));
                report.setProperty(Reports.type, reportTypes);

                datastore.put(txn, report);
            } catch (EntityNotFoundException e) {
                //Create new report and set value
                Entity report = new Entity(Reports.kind, occKey.getId());
                report.setProperty(Reports.count, 1L);
                report.setProperty(Reports.occurrenceKey, occKey);
                report.setProperty(Reports.first_report_date, new Date());
                report.setProperty(Reports.last_report_date, new Date());
                report.setProperty(Reports.usersList, Arrays.asList(reply.getUser().getKey()));

                HashSet<String> reportTypes = new HashSet<>();
                reportTypes.add(OccurrenceValues.getReportValue(data.report));
                report.setProperty(Reports.type, reportTypes);

                datastore.put(txn, report);
            }

            txn.commit();
            return Response.ok(g.toJson(OutputStatus.Messages.REPORT_SUBMITTED)).build();
        } catch (EntityNotFoundException e) {
            LOG.warning("Occurrence doesn't exist.");

            txn.rollback();
            return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson(OutputStatus.Messages.OCCURRENCE_DOES_NOT_EXIST)).build();
        } finally {
            if (txn != null && txn.isActive()) {
                txn.rollback();
                LOG.severe("SERVER ERROR");
            }
        }

    }

}
