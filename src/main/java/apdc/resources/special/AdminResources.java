package apdc.resources.special;


import apdc.gamification.PointSystem;
import apdc.gamification.Values;
import apdc.input.data.admin.*;
import apdc.input.data.basic.LoginData;
import apdc.input.verification.utils.EmailValidationKey;
import apdc.input.verification.utils.Reply;
import apdc.input.verification.utils.RequestDataVerification;
import apdc.input.verification.values.UserValues;
import apdc.output.info.AllUserInfo;
import apdc.output.messages.OutputStatus;
import apdc.permissions.Actions;
import apdc.permissions.PermissionTableSingleton;
import apdc.permissions.UserPermissions;
import apdc.tables.special.NewEntityRequests;
import apdc.tables.user.User;
import apdc.util.email.EmailResource;
import com.google.appengine.api.datastore.*;
import com.google.appengine.repackaged.org.apache.commons.codec.digest.DigestUtils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

@Path("/{device}/special")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class AdminResources {
    private static final int MAX_NUMB_USERS = 10;
    private static final Logger LOG = Logger.getLogger(AdminResources.class.getName());
    private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
    private final Gson g = new Gson();

    public AdminResources() {
    }

    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response userLogin(LoginData data, @Context HttpServletRequest request, @Context HttpHeaders headers, @PathParam("device") String device) {
        Reply reply = RequestDataVerification.validateData(data);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        LOG.info("Attempting admin login: " + data.username);

        reply = UserValues.loginAdmin(data, request, headers, device);

        return Response.status(reply.getStatus()).entity(reply.getResponse()).build();
    }

    @GET
    @Path("/ping")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response userPing(@PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID) {
        Reply reply = RequestDataVerification.validateToken(tokenID, sessionID, device);

        if (reply.ok()) {
            String role = (String) reply.getUser().getProperty(User.role);

            if (role.equals(UserPermissions.ROLE.u_admin.name())) {
                LOG.info("User is Admin");
                return Response.ok().build();
            } else {
                LOG.warning("User is not Admin: " + reply.getUser().getKey().getName());
                return Response.status(Response.Status.FORBIDDEN).entity(g.toJson("user_not_allowed")).build();
            }
        }

        return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson("invalid_token")).build();
    }


    @PUT
    @Path("/deleteUser")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response deleteUser(UserIDData data, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID) {
        Reply reply = RequestDataVerification.validateDataTokenPermission(data, tokenID, sessionID, device, Actions.delete_all_users);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        String hashedPWD = (String) reply.getUser().getProperty(User.password);
        if (!hashedPWD.equals(DigestUtils.sha512Hex(data.password))) {
            LOG.warning("Admin password does not match");
            return Response.status(Response.Status.FORBIDDEN).entity(g.toJson(OutputStatus.Messages.COULD_NOT_AUTHORIZE)).build();
        }

        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withXG(true));

        Key userToDelete = KeyFactory.createKey(User.kind, data.username);
        try {
            datastore.get(txn, userToDelete);
            datastore.delete(txn, userToDelete);

            LOG.info("User deleted");
            txn.commit();
            return Response.ok(g.toJson(OutputStatus.Messages.USER_DELETED)).build();
        } catch (EntityNotFoundException e) {
            LOG.warning("User does not exist");
            txn.rollback();
            return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson(OutputStatus.Messages.USER_DOES_NOT_EXIST)).build();
        } finally {
            if (txn != null && txn.isActive()) {
                txn.rollback();
                LOG.severe("SERVER ERROR");
            }
        }
    }

    @POST
    @Path("/listUsers")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response listUsers(@PathParam("device") String device, @QueryParam("type") String type, @QueryParam("cursor") String cursor, @QueryParam("direction") String direction, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID) {
        Reply reply = RequestDataVerification.validateTokenPermission(tokenID, sessionID, device, Actions.list_all_users);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        /*
        String hashedPWD = (String) reply.getUser().getProperty(User.password);
        if (!hashedPWD.equals(DigestUtils.sha512Hex(data.password))) {
            LOG.warning("Admin password does not match");
            return Response.status(Response.Status.FORBIDDEN).entity(g.toJson(OutputStatus.Messages.COULD_NOT_AUTHORIZE)).build();
        }
        */

        Query.SortDirection sort = Query.SortDirection.DESCENDING;
        String value = User.creation_time;
        direction = direction == null ? "" : direction;

        if (direction.equals("asc"))//desc
            sort = Query.SortDirection.ASCENDING;

        if (type != null) {
            switch (type) {
                case "lastEdit":
                    value = User.last_edit_time;
                    break;
                case "registerDate":
                    value = User.creation_time;
                    break;
                case "valid":
                    value = User.valid;
                    break;
                case "asc":
                    value = User.role;
                    break;
            }
        }

        int numUsers = 0;
        FetchOptions fo = FetchOptions.Builder.withLimit(MAX_NUMB_USERS);

        //If cursor is not null use it
        if (cursor != null && !cursor.isEmpty())
            fo.startCursor(Cursor.fromWebSafeString(cursor));
        else {
            Query ctrQuery = new Query(User.kind);
            numUsers = datastore.prepare(ctrQuery.setKeysOnly()).asList(FetchOptions.Builder.withDefaults()).size();
        }

        Query ctrQuery = new Query(User.kind).addSort(value, sort);
        QueryResultList<Entity> results = datastore.prepare(ctrQuery).asQueryResultList(fo);

        List<AllUserInfo> out = new LinkedList<>();

        for (Entity user : results) {
            String imageID, email, username, role, zipcode, name, address;
            Date lastEdit, register_date;
            Number phoneNumber, nif, points, level, next_level_points, curr_level_points;
            boolean valid;
            Boolean isVolunteer;

            name = (String) user.getProperty(User.name);
            phoneNumber = (Number) user.getProperty(User.phone_number);
            address = (String) user.getProperty(User.address);
            nif = (Number) user.getProperty(User.nif);
            zipcode = (String) user.getProperty(User.zip_code);
            username = user.getKey().getName();
            email = (String) user.getProperty(User.email);
            imageID = (String) user.getProperty(User.imageID);
            lastEdit = (Date) user.getProperty(User.last_edit_time);
            register_date = (Date) user.getProperty(User.creation_time);
            role = (String) user.getProperty(User.role);
            valid = (boolean) user.getProperty(User.valid);
            Values values = PointSystem.getPointsAndLevel(username);
            points = values.getPoints();
            level = values.getLevel();
            isVolunteer = (Boolean) user.getProperty(User.volunteer);
            isVolunteer = isVolunteer == null ? false : isVolunteer;
            next_level_points = PointSystem.pointsForLevelUp(((long) level) + 1);
            curr_level_points = PointSystem.pointsForLevelUp(((long) level));

            out.add(new AllUserInfo(username, email, role, imageID, zipcode, name, phoneNumber, address, nif, lastEdit, register_date, valid, points, level, isVolunteer, next_level_points, curr_level_points));
        }

        //Combine number of users and users info
        JsonObject json = new JsonObject();
        json.add("data", g.toJsonTree(out));
        json.addProperty("numUsers", numUsers);
        json.addProperty("cursor", results.getCursor().toWebSafeString());

        return Response.ok(g.toJson(json)).build();
    }


    @POST
    @Path("/listRoleChangeRequests")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response listRoleChangeRequests(@PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID) {
        Reply reply = RequestDataVerification.validateTokenPermission(tokenID, sessionID, device, Actions.verify_role_change);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        /*
        String hashedPWD = (String) reply.getUser().getProperty(User.password);
        if (!hashedPWD.equals(DigestUtils.sha512Hex(data.password))) {
            LOG.warning("Admin password does not match");
            return Response.status(Response.Status.FORBIDDEN).entity(g.toJson(OutputStatus.Messages.COULD_NOT_AUTHORIZE)).build();
        }*/

        LOG.info("List Role Change Requests");

        List<AllUserInfo> out = new LinkedList<>();
        Query ctrQuery = new Query(NewEntityRequests.kind);
        List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());

        String imageID, email, username, role, zipcode, name, address;
        Date lastEdit, register_date;
        Number phoneNumber, nif;
        boolean valid;
        Boolean isVolunteer;

        for (Entity res : results) {
            Key entityKey = KeyFactory.createKey(User.kind, res.getKey().getName());
            try {
                Entity user = datastore.get(entityKey);
                name = (String) user.getProperty(User.name);
                phoneNumber = (Number) user.getProperty(User.phone_number);
                address = (String) user.getProperty(User.address);
                nif = (Number) user.getProperty(User.nif);
                zipcode = (String) user.getProperty(User.zip_code);
                username = user.getKey().getName();
                email = (String) user.getProperty(User.email);
                imageID = (String) user.getProperty(User.imageID);
                lastEdit = (Date) user.getProperty(User.last_edit_time);
                register_date = (Date) user.getProperty(User.creation_time);
                role = (String) user.getProperty(User.role);
                valid = (boolean) user.getProperty(User.valid);
                isVolunteer = (Boolean) user.getProperty(User.volunteer);
                isVolunteer = isVolunteer == null ? false : isVolunteer;

                out.add(new AllUserInfo(username, email, role, imageID, zipcode, name, phoneNumber, address, nif, lastEdit, register_date, valid, null, null, isVolunteer, null, null));
            } catch (EntityNotFoundException e) {
                LOG.warning("Entity not found!");
            }
        }

        return Response.ok(g.toJson(out)).build();
    }


    @POST
    @Path("/verifyRoleChange")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response verifyRoleChange(RoleChangeData data, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID) {
        Reply reply = RequestDataVerification.validateDataTokenPermission(data, tokenID, sessionID, device, Actions.verify_role_change);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        String hashedPWD = (String) reply.getUser().getProperty(User.password);
        if (!hashedPWD.equals(DigestUtils.sha512Hex(data.password))) {
            LOG.warning("Admin password does not match");
            return Response.status(Response.Status.FORBIDDEN).entity(g.toJson(OutputStatus.Messages.COULD_NOT_AUTHORIZE)).build();
        }

        LOG.info("Editing occurrence status");

        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withXG(true));

        try {
            Key key = KeyFactory.createKey(NewEntityRequests.kind, data.username);
            Entity request = datastore.get(key);
            String role = (String) request.getProperty(NewEntityRequests.role);

            if (role.equals(UserPermissions.ROLE.u_entity.name())) {
                Key eKey = KeyFactory.createKey(User.kind, data.username);
                Entity eEntity = datastore.get(eKey);

                String email = (String) eEntity.getProperty(User.email);

                if (data.accepted) {
                    eEntity.setProperty(User.role, UserPermissions.ROLE.u_entity.name());
                    eEntity.setProperty(User.valid, true);
                    datastore.put(txn, eEntity);
                    datastore.delete(txn, key);
                    EmailResource.sendEntityRegistrationResult(data.username, email, true);
                    LOG.info("Role change accepted");
                } else {
                    datastore.delete(txn, key);
                    EmailResource.sendEntityRegistrationResult(data.username, email, false);
                    LOG.info("Role change denied");
                }
            } else {
                txn.rollback();
                return Response.status(418).entity(g.toJson("We are currently only accepting role changes to Entity, please try again at a latter date. Thank you.")).build();
            }

            txn.commit();
            return Response.ok(g.toJson(OutputStatus.Messages.ROLE_CHANGED)).build();
        } catch (EntityNotFoundException e) {
            LOG.warning("Request was already made");
            txn.rollback();
            return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson(OutputStatus.Messages.REQUEST_ALREADY_EXISTS)).build();

        } finally {
            if (txn != null && txn.isActive()) {
                txn.rollback();
                LOG.severe("SERVER ERROR");
            }
        }
    }

    @POST
    @Path("/createAdmin")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response createAdmin(RegisterAdminData data, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID) {
        Reply reply = RequestDataVerification.validateDataTokenPermission(data, tokenID, sessionID, device, Actions.create_new_admin);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();


        String hashedPWD = (String) reply.getUser().getProperty(User.password);
        if (!hashedPWD.equals(DigestUtils.sha512Hex(data.admin_password))) {
            LOG.warning("Admin password does not match");
            return Response.status(Response.Status.FORBIDDEN).entity(g.toJson(OutputStatus.Messages.COULD_NOT_AUTHORIZE)).build();
        }

        //Check Email and Username
        Reply replyCheck = UserValues.usernameAvailable(data.username);
        if (!replyCheck.ok())
            return Response.status(replyCheck.getStatus()).entity(replyCheck.getResponse()).build();

        replyCheck = UserValues.emailAvailable(data.email);
        if (!replyCheck.ok())
            return Response.status(replyCheck.getStatus()).entity(replyCheck.getResponse()).build();


        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withXG(true));

        try {
            Entity user = UserValues.addUserAdmin(data);
            datastore.put(txn, user);

            String key = EmailValidationKey.getKey(user.getKey().getName());
            EmailResource.sendEmailValidation(user.getKey().getName(), data.email, key);

            txn.commit();

            return Response.ok(g.toJson(OutputStatus.Messages.USER_REG)).build();
        } finally {
            if (txn.isActive()) {
                LOG.severe("SERVER ERROR");
                txn.rollback();
            }
        }
    }

    @POST
    @Path("/createBackOffice")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response createBackOffice(RegisterBackOfficeData data, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID) {
        Reply reply = RequestDataVerification.validateDataTokenPermission(data, tokenID, sessionID, device, Actions.create_new_admin);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        String hashedPWD = (String) reply.getUser().getProperty(User.password);
        if (!hashedPWD.equals(DigestUtils.sha512Hex(data.admin_password))) {
            LOG.warning("Admin password does not match");
            return Response.status(Response.Status.FORBIDDEN).entity(g.toJson(OutputStatus.Messages.COULD_NOT_AUTHORIZE)).build();
        }

        //Check Email and Username
        Reply replyCheck = UserValues.usernameAvailable(data.username);
        if (!replyCheck.ok())
            return Response.status(replyCheck.getStatus()).entity(replyCheck.getResponse()).build();

        replyCheck = UserValues.emailAvailable(data.email);
        if (!replyCheck.ok())
            return Response.status(replyCheck.getStatus()).entity(replyCheck.getResponse()).build();

        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withXG(true));

        try {
            Entity user;
            if (data.role.equals(UserPermissions.ROLE.u_entity.name()))
                user = UserValues.addUserEntity(data);
            else if (data.role.equals(UserPermissions.ROLE.u_worker.name()))
                user = UserValues.addUserWorker(data);
            else {
                LOG.warning("Invalid Role: " + data.role);
                return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson("invalid_role")).build();
            }

            LOG.info("New back office user created: " + data.role + " : username: " + data.username);

            datastore.put(txn, user);

            String key = EmailValidationKey.getKey(user.getKey().getName());
            EmailResource.sendEmailValidation(user.getKey().getName(), data.email, key);

            txn.commit();

            return Response.ok(g.toJson(OutputStatus.Messages.USER_REG)).build();
        } finally {
            if (txn.isActive()) {
                LOG.severe("SERVER ERROR");
                txn.rollback();
            }
        }
    }

    @POST
    @Path("/createBasic")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response createBasic(RegisterBasicData data, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID) {
        Reply reply = RequestDataVerification.validateDataTokenPermission(data, tokenID, sessionID, device, Actions.create_new_admin);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        String hashedPWD = (String) reply.getUser().getProperty(User.password);
        if (!hashedPWD.equals(DigestUtils.sha512Hex(data.admin_password))) {
            LOG.warning("Admin password does not match");
            return Response.status(Response.Status.FORBIDDEN).entity(g.toJson(OutputStatus.Messages.COULD_NOT_AUTHORIZE)).build();
        }

        //Check Email and Username
        Reply replyCheck = UserValues.usernameAvailable(data.username);
        if (!replyCheck.ok())
            return Response.status(replyCheck.getStatus()).entity(replyCheck.getResponse()).build();

        replyCheck = UserValues.emailAvailable(data.email);
        if (!replyCheck.ok())
            return Response.status(replyCheck.getStatus()).entity(replyCheck.getResponse()).build();

        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withXG(true));

        try {
            Entity user = UserValues.addUserBasic(data);
            user = UserValues.editUser(user, data);
            datastore.put(txn, user);

            LOG.info("New basic user username: " + data.username);

            txn.commit();

            return Response.ok(g.toJson(OutputStatus.Messages.USER_REG)).build();
        } finally {
            if (txn.isActive()) {
                LOG.severe("SERVER ERROR");
                txn.rollback();
            }
        }
    }


    @PUT
    @Path("/getPermissionTable")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getPermissionTable(@PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID) {
        Reply reply = RequestDataVerification.validateTokenPermission(tokenID, sessionID, device, Actions.create_new_admin);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        String jsonTable = PermissionTableSingleton.getInstance().getTableJson();

        return Response.ok(jsonTable).build();
    }

    @PUT
    @Path("/setPermissionTable")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response setPermissionTable(TableData data, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID) {
        Reply reply = RequestDataVerification.validateDataTokenPermission(data, tokenID, sessionID, device, Actions.create_new_admin);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        String hashedPWD = (String) reply.getUser().getProperty(User.password);
        if (!hashedPWD.equals(DigestUtils.sha512Hex(data.password))) {
            LOG.warning("Admin password does not match");
            return Response.status(Response.Status.FORBIDDEN).entity(g.toJson(OutputStatus.Messages.COULD_NOT_AUTHORIZE)).build();
        }

        boolean valid = PermissionTableSingleton.getInstance().setTableJson(data.table);

        if (!valid) {
            LOG.warning("Invalid Table");
            return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson("invalid_table")).build();
        }

        return Response.ok(g.toJson("table_updated")).build();
    }

}
