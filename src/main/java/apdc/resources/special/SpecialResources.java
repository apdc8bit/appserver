package apdc.resources.special;

import apdc.input.data.admin.ChangeStatusOccurrenceData;
import apdc.input.data.admin.DeleteCommentAdminData;
import apdc.input.data.admin.OccurrenceIDAdminData;
import apdc.input.data.admin.ReportData;
import apdc.input.verification.utils.InputDataVerification;
import apdc.input.verification.utils.Reply;
import apdc.input.verification.utils.RequestDataVerification;
import apdc.input.verification.values.OccurrenceValues;
import apdc.input.verification.values.WorkerValues;
import apdc.output.info.OccurrenceInfo;
import apdc.output.info.ReportInfo;
import apdc.output.messages.OutputStatus;
import apdc.permissions.Actions;
import apdc.resources.occurrences.Management;
import apdc.tables.occurrence.Comments;
import apdc.tables.occurrence.Occurrences;
import apdc.tables.occurrence.Reports;
import apdc.tables.occurrence.VotesCommentsCount;
import apdc.tables.user.User;
import apdc.tables.worker.WorkerJobs;
import com.google.appengine.api.datastore.*;
import com.google.appengine.repackaged.org.apache.commons.codec.digest.DigestUtils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

@Path("/{device}/special")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class SpecialResources {
    private static final Logger LOG = Logger.getLogger(Management.class.getName());
    private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
    private final Gson g = new Gson();
    private static final int RESPONSE_SIZE = 10;

    public SpecialResources() {
    }

    @PUT
    @Path("/editOccurrenceStatus")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response editOccurrenceStatus(ChangeStatusOccurrenceData data, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID) {
        Reply reply = RequestDataVerification.validateDataTokenPermission(data, tokenID, sessionID, device, Actions.edit_all_occurrences);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        LOG.info("Editing occurrence status");

        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withXG(true));

        try {
            long id = data.id;
            Key ownerKey = KeyFactory.createKey(User.kind, data.username);

            Key occurrenceKey = KeyFactory.createKey(ownerKey, Occurrences.kind, id);
            Entity occurrence = datastore.get(occurrenceKey);

            occurrence.setProperty(Occurrences.status, data.status);
            occurrence.setProperty(Occurrences.last_edit_time, new Date());

            datastore.put(txn, occurrence);
            txn.commit();

            LOG.info("Status updated");
            return Response.ok(g.toJson(OutputStatus.Messages.STATUS_UPDATED)).build();
        } catch (EntityNotFoundException e) {
            LOG.warning("Occurrence doesn't exist.");
            return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson(OutputStatus.Messages.OCCURRENCE_DOES_NOT_EXIST)).build();
        } finally {
            if (txn != null && txn.isActive()) {
                txn.rollback();
                LOG.severe("SERVER ERROR");
            }
        }

    }


    @GET
    @Path("/listByUser")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @SuppressWarnings("unchecked")
    public Response listOccurrenceByUser(@QueryParam("username") String username, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID) {
        Reply reply = RequestDataVerification.validateTokenPermission(tokenID, sessionID, device, Actions.list_all_occurrences);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        username = username == null ? "" : username.toLowerCase();

        LOG.info("Attempting to list occurrences by user: " + username);

        Key userKeyOwner = KeyFactory.createKey(User.kind, username);

        Query ctrQuery = new Query(Occurrences.kind).setAncestor(userKeyOwner);

        List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());
        List<OccurrenceInfo> occurrences = OccurrenceValues.getListWithUserVote(results, reply.getUser().getKey());

        return Response.ok(g.toJson(occurrences)).build();
    }


    @PUT
    @Path("/delete")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response deleteOccurrence(OccurrenceIDAdminData data, @QueryParam("force") boolean force, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID) {
        Reply reply = RequestDataVerification.validateDataTokenPermission(data, tokenID, sessionID, device, Actions.delete_all_occurrences);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        String hashedPWD = (String) reply.getUser().getProperty(User.password);
        if (!hashedPWD.equals(DigestUtils.sha512Hex(data.admin_password))) {
            LOG.warning("Admin password does not match");
            return Response.status(Response.Status.FORBIDDEN).entity(g.toJson(OutputStatus.Messages.COULD_NOT_AUTHORIZE)).build();
        }

        LOG.info("Attempting to delete occurrence: " + data.id);

        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withXG(true));

        try {
            long id = data.id;
            Key ownerKey = KeyFactory.createKey(User.kind, data.username);
            Key occurrenceKey = KeyFactory.createKey(ownerKey, Occurrences.kind, id);

            Query ctrQuery = new Query(WorkerJobs.kind)
                    .setFilter(new Query.FilterPredicate(WorkerJobs.occurrence_key, Query.FilterOperator.EQUAL, occurrenceKey));
            int count = datastore.prepare(ctrQuery).countEntities(FetchOptions.Builder.withDefaults());
            if (count > 0 && !force) {
                LOG.warning("Occurrence has job associated");
                return Response.status(Response.Status.CONFLICT).entity(g.toJson("has_job_use_force")).build();
            }

            datastore.get(txn, occurrenceKey);

            try {
                Entity job = datastore.get(KeyFactory.createKey(WorkerJobs.kind, occurrenceKey.getId()));
                String state = (String) job.getProperty(WorkerJobs.state);
                if (state.equals(WorkerValues.States.TODO.name()))
                    datastore.delete(txn, job.getKey());
                else
                    LOG.info("Occurrence not in TODO");
            } catch (EntityNotFoundException d) {
                LOG.warning("Not not found");
            }

            OccurrenceValues.deleteOccurrence(occurrenceKey);
            datastore.delete(txn, occurrenceKey);

            txn.commit();

            LOG.info("Occurrence deleted successfully");
            return Response.ok(g.toJson(OutputStatus.Messages.OCCURRENCE_REMOVED)).build();
        } catch (EntityNotFoundException e) {
            LOG.warning("Occurrence doesn't exist.");
            return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson(OutputStatus.Messages.OCCURRENCE_DOES_NOT_EXIST)).build();
        } finally {
            if (txn != null && txn.isActive()) {
                txn.rollback();
                LOG.severe("SERVER ERROR");
            }
        }

    }

    @PUT
    @Path("/deleteComment")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response deleteComment(DeleteCommentAdminData data, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID) {
        Reply reply = RequestDataVerification.validateDataTokenPermission(data, tokenID, sessionID, device, Actions.delete_all_comments);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        String hashedPWD = (String) reply.getUser().getProperty(User.password);
        if (!hashedPWD.equals(DigestUtils.sha512Hex(data.admin_password))) {
            LOG.warning("Admin password does not match");
            return Response.status(Response.Status.FORBIDDEN).entity(g.toJson(OutputStatus.Messages.COULD_NOT_AUTHORIZE)).build();
        }

        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withXG(true));

        Key commentKey = KeyFactory.createKey(Comments.kind, data.comment_id);

        try {
            //Check if comment exists
            Entity comments = datastore.get(commentKey);

            //Updates comment count and deletes comment
            Key occKey = (Key) comments.getProperty(Comments.occurrenceKey);
            Key vcCountKey = KeyFactory.createKey(VotesCommentsCount.kind, occKey.getId());

            Entity vcEntity = datastore.get(vcCountKey);

            long nComments = InputDataVerification.getLongValue((Long) vcEntity.getProperty(VotesCommentsCount.commentCount));
            vcEntity.setProperty(VotesCommentsCount.commentCount, --nComments);

            //Delete comment
            datastore.delete(commentKey);
            datastore.put(txn, vcEntity);

            txn.commit();
            return Response.ok(g.toJson(OutputStatus.Messages.COMMENT_DELETED)).build();
        } catch (EntityNotFoundException e) {
            LOG.warning("Comment does not exist.");
            return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson(OutputStatus.Messages.COMMENT_DOES_NOT_EXIST)).build();
        } finally {
            if (txn != null && txn.isActive()) {
                txn.rollback();
                LOG.severe("SERVER ERROR");
            }
        }

    }

    @POST
    @Path("/numbReports")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @SuppressWarnings("unchecked")
    public Response numbReports(@PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID) {
        Reply reply = RequestDataVerification.validateTokenPermission(tokenID, sessionID, device, Actions.list_reports);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        /*
        String hashedPWD = (String) reply.getUser().getProperty(User.password);
        if (!hashedPWD.equals(DigestUtils.sha512Hex(data.password))) {
            LOG.warning("Admin password does not match");
            return Response.status(Response.Status.FORBIDDEN).entity(g.toJson(OutputStatus.Messages.COULD_NOT_AUTHORIZE)).build();
        }*/

        Query ctrQuery = new Query(Reports.kind);
        int numbReports = datastore.prepare(ctrQuery).countEntities(FetchOptions.Builder.withDefaults());

        JsonObject json = new JsonObject();
        json.addProperty("numbReports", numbReports);

        return Response.ok(g.toJson(json)).build();
    }

    @POST
    @Path("/listReports")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @SuppressWarnings("unchecked")
    public Response listReports(@PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID) {
        Reply reply = RequestDataVerification.validateTokenPermission(tokenID, sessionID, device, Actions.list_reports);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        /*
        String hashedPWD = (String) reply.getUser().getProperty(User.password);
        if (!hashedPWD.equals(DigestUtils.sha512Hex(data.password))) {
            LOG.warning("Admin password does not match");
            return Response.status(Response.Status.FORBIDDEN).entity(g.toJson(OutputStatus.Messages.COULD_NOT_AUTHORIZE)).build();
        }*/


        Query ctrQuery = new Query(Reports.kind).addSort(Reports.first_report_date, Query.SortDirection.ASCENDING);
        List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());

        List<ReportInfo> out = new LinkedList<>();

        for (Entity e : results) {
            Key occKey = (Key) e.getProperty(Reports.occurrenceKey);
            try {
                Entity occ = datastore.get(occKey);
                List<Key> userKeys = (List<Key>) e.getProperty(Reports.usersList);
                List<String> users = new LinkedList<>();
                for (Key key : userKeys)
                    users.add(key.getName());
                long numb_reports = InputDataVerification.getLongValue((Long) e.getProperty(Reports.count));
                Date first_report_date = (Date) e.getProperty(Reports.first_report_date);
                Date last_report_date = (Date) e.getProperty(Reports.last_report_date);
                List<String> types = (List<String>) e.getProperty(Reports.type);

                OccurrenceInfo occurrence = OccurrenceValues.getOccurrenceInfo(occ, null, null);

                out.add(new ReportInfo(occurrence, users, numb_reports, first_report_date, last_report_date, types));
            } catch (EntityNotFoundException e1) {
                LOG.warning("Occurrence not found");
            }
        }

        return Response.ok(g.toJson(out)).build();
    }

    @PUT
    @Path("/confirmReport")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @SuppressWarnings("unchecked")
    public Response confirmReport(ReportData data, @QueryParam("force") boolean force, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID) {
        Reply reply = RequestDataVerification.validateDataTokenPermission(data, tokenID, sessionID, device, Actions.confirm_report);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        LOG.warning("force: " + force);
        LOG.warning("conf: " + data.confirmation);
        LOG.warning("occ: " + data.occ_id);

        String hashedPWD = (String) reply.getUser().getProperty(User.password);
        if (!hashedPWD.equals(DigestUtils.sha512Hex(data.password))) {
            LOG.warning("Admin password does not match");
            return Response.status(Response.Status.FORBIDDEN).entity(g.toJson(OutputStatus.Messages.COULD_NOT_AUTHORIZE)).build();
        }

        TransactionOptions options = TransactionOptions.Builder.withXG(true);
        Transaction txn = datastore.beginTransaction(options);

        Key reportKey = KeyFactory.createKey(Reports.kind, data.occ_id);

        try {
            Entity report = datastore.get(reportKey);

            if (data.confirmation) {
                //delete occurrence
                Key occKey = (Key) report.getProperty(Reports.occurrenceKey);
                LOG.warning("KEY: " + occKey);
                Query ctrQuery = new Query(WorkerJobs.kind)
                        .setFilter(new Query.FilterPredicate(WorkerJobs.occurrence_key, Query.FilterOperator.EQUAL, occKey));
                int count = datastore.prepare(ctrQuery).countEntities(FetchOptions.Builder.withDefaults());

                if (count > 0 && !force) {
                    LOG.warning("Occurrence has job associated");
                    txn.rollback();
                    return Response.status(Response.Status.CONFLICT).entity(g.toJson("has_job_use_force")).build();
                }

                LOG.info("Removing Occurrence");

                OccurrenceValues.deleteOccurrence(occKey);

                try {
                    Entity job = datastore.get(KeyFactory.createKey(WorkerJobs.kind, occKey.getId()));
                    String state = (String) job.getProperty(WorkerJobs.state);
                    if (state.equals(WorkerValues.States.TODO.name()))
                        datastore.delete(txn, job.getKey());
                } catch (EntityNotFoundException d) {
                    LOG.warning("Not not found");
                }

                datastore.delete(txn, occKey);
                datastore.delete(txn, reportKey);
            } else {
                //False report
                LOG.warning("False report");
                datastore.delete(txn, reportKey);
            }

            txn.commit();
            return Response.ok(OutputStatus.Messages.REPORT_CONFIRMED).build();
        } catch (EntityNotFoundException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(OutputStatus.Messages.REPORT_NOT_FOUND).build();
        } finally {
            if (txn != null && txn.isActive()) {
                txn.rollback();
                LOG.severe("SERVER ERROR");
            }
        }
    }

    @PUT
    @Path("/listAllOccurrences")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response listAllOccurrences(@PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID, @QueryParam("cursor") String cursor, @QueryParam("size") int size) {
        Reply reply = RequestDataVerification.validateTokenPermission(tokenID, sessionID, device, Actions.list_all_occurrences);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        /*
        String hashedPWD = (String) reply.getUser().getProperty(User.password);
        if (!hashedPWD.equals(DigestUtils.sha512Hex(data.password))) {
            LOG.warning("Admin password does not match");
            return Response.status(Response.Status.FORBIDDEN).entity(g.toJson(OutputStatus.Messages.COULD_NOT_AUTHORIZE)).build();
        }*/

        //Limit number of entities
        int numbOccs = RESPONSE_SIZE;
        if (size > 0 && size < RESPONSE_SIZE)
            numbOccs = size;

        FetchOptions fo = FetchOptions.Builder.withLimit(numbOccs);

        //If cursor is not null use it
        if (cursor != null && !cursor.isEmpty())
            fo.startCursor(Cursor.fromWebSafeString(cursor));

        //Query by User Key on Occurrences, sort by new
        Query query = new Query(Occurrences.kind)
                .addSort(Occurrences.creation_time, Query.SortDirection.DESCENDING);
        QueryResultList<Entity> result = datastore.prepare(query).asQueryResultList(fo);
        List<OccurrenceInfo> occurrences = OccurrenceValues.getListWithUserVote(result, null);

        return Response.ok(g.toJson(occurrences)).build();
    }

}
