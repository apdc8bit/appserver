package apdc.resources.entities;

import apdc.input.data.entity.EventData;
import apdc.input.verification.utils.Reply;
import apdc.input.verification.utils.RequestDataVerification;
import apdc.input.verification.values.NotificationValues;
import apdc.input.verification.values.VolunteerValues;
import apdc.permissions.Actions;
import apdc.permissions.UserPermissions;
import apdc.tables.entity.Events;
import apdc.tables.entity.EventsAss;
import apdc.tables.user.User;
import com.google.appengine.api.datastore.*;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

@Path("/{device}/event")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class EntityVolunteerResource {
    private static final Logger LOG = Logger.getLogger(EntityResource.class.getName());
    private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
    private final Gson g = new Gson();

    public EntityVolunteerResource() {
    }

    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response createEvent(EventData data, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID) {
        Reply reply = RequestDataVerification.validateDataTokenPermission(data, tokenID, sessionID, device, Actions.create_new_event);
        if (!reply.ok()) {
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();
        }

        Reply reply2 = VolunteerValues.isVolunteer(reply.getUser());
        if (!reply2.ok()) {
            return Response.status(reply2.getStatus()).entity(reply2.getResponse()).build();
        }

        Transaction txn = null;

        try {
            Key workerKey = KeyFactory.createKey(User.kind, data.workerUsername);
            datastore.get(workerKey);
            Date convertedDate;
            try {
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm");
                convertedDate = dateFormat.parse(data.eventTime);
                if (convertedDate.before(new Date())) {
                    LOG.warning("Invalid Date");
                    return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson("invalid_date")).build();
                }
            } catch (ParseException e) {
                LOG.warning("Error Parsing Date");
                return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson("error_parsing_date")).build();
            }

            Entity event = VolunteerValues.addEvent(data, workerKey, reply.getUser().getKey(), convertedDate);

            txn = datastore.beginTransaction(TransactionOptions.Builder.withXG(true));
            datastore.put(txn, event);
            txn.commit();
            LOG.info("Event Added");

            NotificationValues.userNearEvent(event, data.workerUsername);

            JsonObject json = new JsonObject();
            json.addProperty("id", event.getKey().getId());

            return Response.ok(g.toJson(json)).build();
        } catch (EntityNotFoundException e) {
            LOG.warning("Worker not found");
            return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson("worker_not_found")).build();
        } finally {
            if (txn != null && txn.isActive()) {
                LOG.severe("SERVER ERROR");
                txn.rollback();
            }
        }
    }

    @GET
    @Path("/listMy")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response listMyEvents(@QueryParam("filter") String filter, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID) {
        Reply reply = RequestDataVerification.validateToken(tokenID, sessionID, device);
        if (!reply.ok()) {
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();
        }

        VolunteerValues.FilterEvents filterEvents = VolunteerValues.getUserFilter(reply.getUser());

        if (filterEvents == null) {
            return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson("user_not_allowed")).build();
        }

        if (filterEvents.role.equals(UserPermissions.ROLE.u_basic)) {
            Query.Filter user_filter = new Query.FilterPredicate(EventsAss.userKey, Query.FilterOperator.EQUAL, filterEvents.userKey);
            Query ctrQuery = new Query(EventsAss.kind)
                    .setFilter(user_filter);
            List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());
            List<Key> eventKeys = new LinkedList<>();
            for (Entity e : results) {
                eventKeys.add((Key) e.getProperty(EventsAss.eventKey));
            }
            List<Entity> events = new LinkedList<>();
            events.addAll(datastore.get(eventKeys).values());
            return Response.ok(g.toJson(VolunteerValues.listEvents(events, reply.getUser().getKey()))).build();
        }

        Query.Filter user_filter;
        switch (filterEvents.role) {
            case u_entity:
                user_filter = new Query.FilterPredicate(Events.entityKey, Query.FilterOperator.EQUAL, filterEvents.userKey);
                break;
            case u_worker:
                user_filter = new Query.FilterPredicate(Events.workerKey, Query.FilterOperator.EQUAL, filterEvents.userKey);
                break;
            default:
                user_filter = new Query.FilterPredicate(Events.entityKey, Query.FilterOperator.EQUAL, "");
                break;
        }
        Query.Filter state_filter = VolunteerValues.getQueryFilter(filter);
        Query.CompositeFilter and_filter = Query.CompositeFilterOperator.and(user_filter, state_filter);

        Query ctrQuery = new Query(Events.kind)
                .setFilter(and_filter)
                .addSort(Events.state, Query.SortDirection.ASCENDING)
                .addSort(Events.eventTime, Query.SortDirection.ASCENDING);
        List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());

        LOG.info("Returning events: " + results.size());

        return Response.ok(g.toJson(VolunteerValues.listEvents(results, reply.getUser().getKey()))).build();
    }

    @GET
    @Path("/listAll")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response listAllEvents(@QueryParam("filter") boolean filter, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID) {
        Reply reply = RequestDataVerification.validateToken(tokenID, sessionID, device);

        Query ctrQuery;
        Query.Filter filterArchived = new Query.FilterPredicate(Events.state, Query.FilterOperator.EQUAL, VolunteerValues.States.started.name());

        if (filter && reply.ok()) {
            String dico = (String) reply.getUser().getProperty(User.dico);
            if (dico != null) {
                Query.Filter filterDico = new Query.FilterPredicate(Events.dico, Query.FilterOperator.EQUAL, dico);
                Query.CompositeFilter and_filter = Query.CompositeFilterOperator.and(filterDico, filterArchived);
                ctrQuery = new Query(Events.kind)
                        .setFilter(and_filter);
                //.addSort(Events.eventTime, Query.SortDirection.ASCENDING);
            } else
                ctrQuery = new Query(Events.kind).addSort(Events.eventTime, Query.SortDirection.ASCENDING).setFilter(filterArchived);
        } else {
            ctrQuery = new Query(Events.kind).addSort(Events.eventTime, Query.SortDirection.ASCENDING).setFilter(filterArchived);
        }

        List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());

        LOG.info("Returning events: " + results.size());

        return Response.ok(g.toJson(VolunteerValues.listEvents(results, reply.ok() ? reply.getUser().getKey() : null))).build();
    }

    @GET
    @Path("/get/{id}")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response listAllEvents(@PathParam("id") long id, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID) {
        Reply reply = RequestDataVerification.validateToken(tokenID, sessionID, device);

        Key userKey = reply.ok() ? reply.getUser().getKey() : null;

        try {
            Key eventKey = KeyFactory.createKey(Events.kind, id);
            Entity event = datastore.get(eventKey);

            LOG.info("Returning event: " + id);
            return Response.ok(g.toJson(VolunteerValues.getEventInfo(event, userKey))).build();
        } catch (EntityNotFoundException e) {
            LOG.warning("Event not found");
            return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson("event_not_found")).build();
        }
    }

    @POST
    @Path("/join/{id}")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response joinEvent(@PathParam("id") long id, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID) {
        Reply reply = RequestDataVerification.validateTokenPermission(tokenID, sessionID, device, Actions.join_event);
        if (!reply.ok()) {
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();
        }

        Entity event = VolunteerValues.getEvent(id);

        if (event == null) {
            LOG.info("Event not found");
            return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson("event_not_found")).build();
        }

        Transaction txn = null;

        try {
            VolunteerValues.EventAssValues eventAssValues = VolunteerValues.getEventAssValues(id, reply.getUser().getKey().getName());

            if (eventAssValues.entity != null) {
                LOG.info("User already joined");
                return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson("user_already_joined")).build();
            }

            txn = datastore.beginTransaction(TransactionOptions.Builder.withXG(true));
            Entity eventAss = new Entity(EventsAss.kind, eventAssValues.keyS);
            eventAss.setProperty(EventsAss.eventKey, event.getKey());
            eventAss.setProperty(EventsAss.userKey, reply.getUser().getKey());
            datastore.put(txn, eventAss);
            txn.commit();

            LOG.info("User " + reply.getUser().getKey().getName() + " joined " + id + " event");
            return Response.ok(g.toJson("user_joined")).build();
        } finally {
            if (txn != null && txn.isActive()) {
                txn.rollback();
                LOG.severe("SERVER ERROR");
            }
        }
    }

    @DELETE
    @Path("/disjoin/{id}")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response disjoinEvent(@PathParam("id") long id, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID) {
        Reply reply = RequestDataVerification.validateTokenPermission(tokenID, sessionID, device, Actions.join_event);
        if (!reply.ok()) {
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();
        }

        Entity event = VolunteerValues.getEvent(id);

        if (event == null) {
            LOG.info("Event not found");
            return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson("event_not_found")).build();
        }

        Transaction txn = null;

        try {
            VolunteerValues.EventAssValues eventAssValues = VolunteerValues.getEventAssValues(id, reply.getUser().getKey().getName());

            if (eventAssValues.entity == null) {
                LOG.info("User hasn't joined yet");
                return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson("user_not_joined")).build();
            }

            txn = datastore.beginTransaction(TransactionOptions.Builder.withXG(true));
            datastore.delete(txn, eventAssValues.key);
            txn.commit();

            LOG.info("User " + reply.getUser().getKey().getName() + " removed from " + id + " event");
            return Response.ok(g.toJson("user_removed")).build();
        } finally {
            if (txn != null && txn.isActive()) {
                txn.rollback();
                LOG.severe("SERVER ERROR");
            }
        }

    }

    @DELETE
    @Path("/removeUser/{id}/{username}")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response removeUser(@PathParam("id") long id, @PathParam("username") String username, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID) {
        Reply reply = RequestDataVerification.validateTokenPermission(tokenID, sessionID, device, Actions.manage_event);
        if (!reply.ok()) {
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();
        }

        Entity event = VolunteerValues.getEvent(id);
        if (event == null) {
            LOG.info("Event not found");
            return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson("event_not_found")).build();
        }

        String state = (String) event.getProperty(Events.state);
        if (state.equals(VolunteerValues.States.archived.name())) {
            LOG.warning("Event is Archived");
            return Response.status(Response.Status.FORBIDDEN).entity(g.toJson("cannot_remove_users_from_archived_event")).build();
        }

        if (!VolunteerValues.isWorkerAssociated(reply.getUser(), event))
            return Response.status(Response.Status.FORBIDDEN).entity(g.toJson("worker_not_associated_with_event")).build();

        Transaction txn = null;
        try {
            Entity user;

            try {
                Key userKey = KeyFactory.createKey(User.kind, username);
                user = datastore.get(userKey);
            } catch (EntityNotFoundException e) {
                LOG.warning("User not Found");
                return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson("user_not_found")).build();
            }

            VolunteerValues.EventAssValues eventAssValues = VolunteerValues.getEventAssValues(id, username);

            if (eventAssValues.entity == null) {
                LOG.info("User hasn't joined");
                return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson("user_not_joined")).build();
            }

            txn = datastore.beginTransaction(TransactionOptions.Builder.withXG(true));
            datastore.delete(txn, eventAssValues.key);
            txn.commit();

            LOG.info("User " + user.getKey().getName() + " removed from " + id + " event");
            return Response.ok(g.toJson("user_removed")).build();
        } finally {
            if (txn != null && txn.isActive()) {
                txn.rollback();
                LOG.severe("SERVER ERROR");
            }
        }
    }

    @PUT
    @Path("/archive/{id}")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response removeUser(@PathParam("id") long id, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID) {
        Reply reply = RequestDataVerification.validateTokenPermission(tokenID, sessionID, device, Actions.create_new_event);
        if (!reply.ok()) {
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();
        }

        Reply reply2 = VolunteerValues.isVolunteer(reply.getUser());
        if (!reply2.ok()) {
            return Response.status(reply2.getStatus()).entity(reply2.getResponse()).build();
        }

        Entity event = VolunteerValues.getEvent(id);

        if (event == null) {
            LOG.info("Event not found");
            return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson("event_not_found")).build();
        }

        String username = ((Key) event.getProperty(Events.entityKey)).getName();
        if (!username.equals(reply.getUser().getKey().getName())) {
            LOG.warning("Entity did not create event");
            return Response.status(Response.Status.FORBIDDEN).entity(g.toJson("entity_did_not_create_event")).build();
        }

        Date date = (Date) event.getProperty(Events.eventTime);

        if (date.after(new Date())) {
            LOG.warning("Event has not happened yet");
            return Response.status(Response.Status.FORBIDDEN).entity(g.toJson("event_has_not_happened_yet")).build();
        }

        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withXG(true));

        try {
            event = VolunteerValues.archiveEvent(event);

            datastore.put(txn, event);
            txn.commit();

            LOG.info("Event archived");
            return Response.ok(g.toJson("event_archived")).build();

        } finally {
            if (txn != null && txn.isActive()) {
                txn.rollback();
                LOG.severe("SERVER ERROR");
            }
        }

    }

}