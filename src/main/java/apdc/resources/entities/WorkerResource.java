package apdc.resources.entities;

import apdc.input.data.occurrences.MarkJobCompletedData;
import apdc.input.data.occurrences.OccurrenceChangeTypeData;
import apdc.input.verification.utils.Reply;
import apdc.input.verification.utils.RequestDataVerification;
import apdc.input.verification.values.NotificationValues;
import apdc.input.verification.values.OccurrenceValues;
import apdc.input.verification.values.WorkerValues;
import apdc.output.messages.OutputStatus;
import apdc.permissions.Actions;
import apdc.resources.occurrences.Management;
import apdc.tables.occurrence.Occurrences;
import apdc.tables.worker.WorkerJobs;
import com.google.appengine.api.datastore.*;
import com.google.gson.Gson;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

@Path("/{device}/worker")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class WorkerResource {

    private static final Logger LOG = Logger.getLogger(Management.class.getName());
    private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
    private final Gson g = new Gson();

    public WorkerResource() {
    }

    @GET
    @Path("/getJobs")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getOccurrences(@PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID, @QueryParam("filter") String filter) {
        Reply reply = RequestDataVerification.validateTokenPermission(tokenID, sessionID, device, Actions.list_self_worker_jobs);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        List<Entity> results;
        Query.Filter propertyFilter;
        WorkerValues.States state = WorkerValues.getState(filter);

        if (state != null)
            propertyFilter = new Query.FilterPredicate(WorkerJobs.state, Query.FilterOperator.EQUAL, state.name());
        else
            propertyFilter = new Query.FilterPredicate(WorkerJobs.state, Query.FilterOperator.NOT_EQUAL, "");

        Query ctrQuery = new Query(WorkerJobs.kind)
                .setAncestor(reply.getUser().getKey())
                .addSort(WorkerJobs.state, Query.SortDirection.ASCENDING)
                .addSort(WorkerJobs.added_time, Query.SortDirection.DESCENDING)
                .setFilter(propertyFilter);
        results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());

        return Response.ok().entity(g.toJson(WorkerValues.listJobs(results))).build();
    }

    @PUT
    @Path("/changeType")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response changeState(OccurrenceChangeTypeData data, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID) {
        Reply reply = RequestDataVerification.validateTokenPermission(tokenID, sessionID, device, Actions.change_other_occurrence_type);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withXG(true));

        Key workerKey = reply.getUser().getKey();
        Key assKey = KeyFactory.createKey(workerKey, WorkerJobs.kind, data.occurrence_id);

        try {
            Entity ass = datastore.get(assKey);
            Key occKey = (Key) ass.getProperty(WorkerJobs.occurrence_key);
            Entity occ = datastore.get(occKey);

            String type = OccurrenceValues.getType(data.value);

            if (type.equalsIgnoreCase(data.value)) {
                LOG.warning("Incorrect type using: " + type);
            }

            occ.setProperty(Occurrences.type, type);
            occ.setProperty(Occurrences.last_edit_time, new Date());
            datastore.put(txn, occ);

            txn.commit();
            return Response.ok(OutputStatus.Messages.TYPE_UPDATED).build();
        } catch (EntityNotFoundException e) {
            LOG.warning("Occurrence not found or Occurrence not associated with worker");
            return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson(OutputStatus.Messages.OCCURRENCE_DOES_NOT_EXIST)).build();
        } finally {
            if (txn != null && txn.isActive()) {
                txn.rollback();
                LOG.severe("SERVER ERROR");
            }
        }
    }

    @PUT
    @Path("/markCompleted")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response markCompleted(MarkJobCompletedData data, @PathParam("device") String device, @HeaderParam("Authorization") String tokenID, @HeaderParam("SessionID") String sessionID) {
        Reply reply = RequestDataVerification.validateDataTokenPermission(data, tokenID, sessionID, device, Actions.mark_occurrence_complete);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();


        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withXG(true));

        Key workerKey = reply.getUser().getKey();
        Key assKey = KeyFactory.createKey(workerKey, WorkerJobs.kind, data.occurrence_id);

        try {
            Entity ass = datastore.get(assKey);

            String currState = (String) ass.getProperty(WorkerJobs.state);

            if (!currState.equalsIgnoreCase(WorkerValues.States.TODO.name())) {
                LOG.warning("Jobs is not in state TODO");
                txn.rollback();
                return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson(OutputStatus.Messages.JOB_NOT_TODO)).build();
            }

            ass.setProperty(WorkerJobs.state, WorkerValues.States.WAITING_CONFIRMATION.name());
            ass.setProperty(WorkerJobs.imageID, data.imageID);
            ass.setProperty(WorkerJobs.finish_time, new Date());

            datastore.put(txn, ass);

            try {
                Key occKey = (Key) ass.getProperty(WorkerJobs.occurrence_key);
                Entity occ = datastore.get(occKey);
                NotificationValues.entityWorkerJobFinished(occ, workerKey.getName());
            } catch (EntityNotFoundException e) {
                LOG.warning("Occurrence not found");
            }


            txn.commit();
            return Response.ok(OutputStatus.Messages.MARKED_COMPLETED).build();
        } catch (EntityNotFoundException e) {
            LOG.warning("Occurrence not found");
            txn.rollback();
            return Response.status(Response.Status.BAD_REQUEST).entity(g.toJson(OutputStatus.Messages.OCCURRENCE_DOES_NOT_EXIST)).build();
        } finally {
            if (txn != null && txn.isActive()) {
                txn.rollback();
                LOG.severe("SERVER ERROR");
            }
        }
    }


}
