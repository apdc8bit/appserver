package apdc.resources.users;

import apdc.exceptions.ExpiredEmailVerificationException;
import apdc.exceptions.InvalidEmailKeyException;
import apdc.gamification.PointSystem;
import apdc.input.data.SettingsData;
import apdc.input.data.basic.AdditionalRegistrationData;
import apdc.input.data.basic.DeleteInfoData;
import apdc.input.data.basic.EmailData;
import apdc.input.data.basic.InitialRegistrationData;
import apdc.input.data.entity.DeleteUserData;
import apdc.input.verification.utils.EmailValidationKey;
import apdc.input.verification.utils.Reply;
import apdc.input.verification.utils.RequestDataVerification;
import apdc.input.verification.values.UserValues;
import apdc.output.messages.OutputStatus;
import apdc.permissions.Actions;
import apdc.resources.api.UserResources;
import apdc.tables.user.Settings;
import apdc.tables.user.User;
import apdc.util.email.EmailResource;
import com.google.appengine.api.datastore.*;
import com.google.gson.JsonObject;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.util.Date;

import static apdc.resources.api.Base.g;
import static apdc.resources.api.UserResources.LOG;
import static apdc.resources.api.UserResources.datastore;

@Path("{device}/user/")
public class Account implements UserResources.AccountI {

    public Account() {
    }

    @Override
    public Response confirmUsername(String username) {
        LOG.info("Attempting to confirm username: " + username);

        Reply reply = UserValues.usernameAvailable(username);

        return Response.status(reply.getStatus()).entity(reply.getResponse()).build();
    }

    @Override
    public Response confirmEmail(String email) {
        LOG.info("Attempting to confirm email: " + email);

        Reply reply = UserValues.emailAvailable(email);

        return Response.status(reply.getStatus()).entity(reply.getResponse()).build();
    }

    @Override
    public Response userRegister(InitialRegistrationData data, String device) {
        Reply reply = RequestDataVerification.validateData(data);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        LOG.info("Attempting to register: " + data.username);

        //Check Email and Username
        Reply replyCheck = UserValues.usernameAvailable(data.username);
        if (!replyCheck.ok())
            return Response.status(replyCheck.getStatus()).entity(replyCheck.getResponse()).build();

        replyCheck = UserValues.emailAvailable(data.email);
        if (!replyCheck.ok())
            return Response.status(replyCheck.getStatus()).entity(replyCheck.getResponse()).build();

        TransactionOptions options = TransactionOptions.Builder.withXG(true);
        Transaction txn = datastore.beginTransaction(options);

        try {
            Entity user = UserValues.addUserBasic(data);
            datastore.put(txn, user);

            String key = EmailValidationKey.getKey(user.getKey().getName());
            EmailResource.sendEmailValidation(user.getKey().getName(), data.email, key);

            txn.commit();

            PointSystem.addPoints(PointSystem.ACTIONS.register, data.username);

            LOG.info("User registered: " + data.username);

            return Response.ok(OutputStatus.USER_REGISTERED).build();
        } finally {
            if (txn.isActive()) {
                LOG.severe("SERVER ERROR");
                txn.rollback();
            }
        }
    }

    @Override
    public Response changeEmail(EmailData data, String device, String tokenID, String sessionID) {
        Reply reply = RequestDataVerification.validateDataTokenPermission(data, tokenID, sessionID, device, Actions.change_self_email);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        LOG.info("Changing user's email");

        //Check if email exists
        Reply replyCheck = UserValues.emailAvailable(data.email);
        if (!replyCheck.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        TransactionOptions options = TransactionOptions.Builder.withXG(true);
        Transaction txn = datastore.beginTransaction(options);

        try {
            Entity user = reply.getUser();
            String hashedPWD = (String) user.getProperty(User.password);

            //Check Passwords
            reply = UserValues.comparePasswords(hashedPWD, data.password);
            if (!reply.ok()) {
                txn.rollback();
                return Response.status(reply.getStatus()).entity(reply.getResponse()).build();
            }

            user.setProperty(User.email, data.email);
            user.setProperty(User.last_edit_time, new Date());
            boolean valid = (boolean) user.getProperty(User.valid);

            if (valid) {
                //User is valid, send confirmation
                EmailResource.sendNewEmailConfirmation(reply.getUsername(), data.email);
            } else {
                //User is not valid, send email validation
                String key = EmailValidationKey.getKey(reply.getUsername());
                EmailResource.sendEmailValidation(reply.getUsername(), data.email, key);
            }

            LOG.info("Email changed from user: " + reply.getUsername());

            datastore.put(txn, user);
            txn.commit();
            return Response.ok(OutputStatus.EMAIL_CHANGED).build();
        } finally {
            if (txn.isActive()) {
                LOG.severe("SERVER ERROR");
                txn.rollback();
            }
        }
    }

    @Override
    public Response userAddEditInfo(AdditionalRegistrationData data, String device, String tokenID, String sessionID) {
        Reply reply = RequestDataVerification.validateDataTokenPermission(data, tokenID, sessionID, device, Actions.edit_self_profile);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        LOG.info("Adding or editing user's information.");

        TransactionOptions options = TransactionOptions.Builder.withXG(true);
        Transaction txn = datastore.beginTransaction(options);

        try {
            Entity userEntity = reply.getUser();
            int n_props = userEntity.getProperties().size();

            //Edit/Add info
            userEntity = UserValues.editUser(userEntity, data);

            datastore.put(txn, userEntity);

            LOG.info("Information added to user: " + reply.getUsername());

            txn.commit();

            boolean levelUP = false;
            //Check if user added new info
            if (userEntity.getProperties().size() > n_props) {
                levelUP = PointSystem.addPoints(PointSystem.ACTIONS.add_info, userEntity.getKey().getName());
            }

            JsonObject json = new JsonObject();
            json.addProperty("levelUP", levelUP);

            return Response.ok(g.toJson(json)).build();
        } finally {
            if (txn != null && txn.isActive()) {
                LOG.severe("SERVER ERROR");
                txn.rollback();
            }
        }
    }

    @Override
    public Response setSettings(SettingsData data, String device, String tokenID, String sessionID) {
        Reply reply = RequestDataVerification.validateDataTokenPermission(data, tokenID, sessionID, device, Actions.change_settings);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withXG(true));
        String key = KeyFactory.keyToString(reply.getUser().getKey());
        Entity settings;

        try {
            try {
                Key settingsKey = KeyFactory.createKey(Settings.kind, key);
                settings = datastore.get(txn, settingsKey);
            } catch (EntityNotFoundException e) {
                settings = new Entity(Settings.kind, key);
            }

            if (data.isColorBlind != null)
                settings.setProperty(Settings.colorBind, data.isColorBlind);

            datastore.put(txn, settings);
            txn.commit();
            return Response.ok(OutputStatus.SETTINGS_UPDATED).build();
        } finally {
            if (txn != null && txn.isActive()) {
                LOG.severe("SERVER ERROR");
                txn.rollback();
            }
        }
    }

    @Override
    public Response deleteInfo(DeleteInfoData data, String device, String tokenID, String sessionID) {
        Reply reply = RequestDataVerification.validateDataTokenPermission(data, tokenID, sessionID, device, Actions.edit_self_profile);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        LOG.info("Deleting user's information.");

        TransactionOptions options = TransactionOptions.Builder.withXG(true);
        Transaction txn = datastore.beginTransaction(options);

        try {
            Entity userEntity = reply.getUser();

            UserValues.deleteInfoUser(userEntity, data);

            datastore.put(txn, userEntity);

            LOG.info("Info deleted from user: " + userEntity.getKey().getName());

            txn.commit();
            return Response.ok(OutputStatus.INFO_DELETED).build();
        } finally {
            if (txn != null && txn.isActive()) {
                LOG.severe("SERVER ERROR");
                txn.rollback();
            }
        }

    }

    @Override
    public Response userDelete(DeleteUserData data, String tokenID, String device, String sessionID) {
        Reply reply = RequestDataVerification.validateDataTokenPermission(data, tokenID, sessionID, device, Actions.delete_self_account);
        if (!reply.ok())
            return Response.status(reply.getStatus()).entity(reply.getResponse()).build();

        LOG.info("Deleting user.");

        TransactionOptions options = TransactionOptions.Builder.withXG(true);
        Transaction txn = datastore.beginTransaction(options);

        try {
            Entity user = reply.getUser();
            String email = (String) user.getProperty(User.password);
            String hashedPWD = (String) user.getProperty(User.password);

            //Check Passwords
            Reply reply2 = UserValues.comparePasswords(hashedPWD, data.password);
            if (!reply2.ok()) {
                txn.rollback();
                return Response.status(reply2.getStatus()).entity(reply2.getResponse()).build();
            }

            String key = EmailValidationKey.getKey(reply.getUsername());
            EmailResource.sendAccountDeletion(reply.getUsername(), email, key);
            LOG.info("User marked for deletion");

            txn.commit();
            return Response.ok(OutputStatus.USER_MARKED_DELETION).build();
        } finally {
            if (txn != null && txn.isActive()) {
                txn.rollback();
                LOG.severe("SERVER ERROR");
            }
        }
    }

    @Override
    public Response userValidateEmail(String key) {
        LOG.info("Validating email key: " + key);

        TransactionOptions options = TransactionOptions.Builder.withXG(true);
        Transaction txn = datastore.beginTransaction(options);
        try {
            try {
                Entity user = EmailValidationKey.getUserValid(key);
                user.setProperty(User.valid, true);
                user.setProperty(User.last_edit_time, new Date());
                datastore.put(txn, user);

                LOG.info("User validated: " + user.getKey().getName());

                txn.commit();
                return Response.ok(OutputStatus.EMAIL_VALIDATED).build();
            } catch (ExpiredEmailVerificationException e) {
                txn.rollback();
                return Response.status(Response.Status.FORBIDDEN).entity(OutputStatus.EXPIRED_KEY).build();
            } catch (InvalidEmailKeyException e) {
                txn.rollback();
                return Response.status(Response.Status.FORBIDDEN).entity(OutputStatus.INVALID_KEY).build();
            }
        } finally {
            if (txn != null && txn.isActive()) {
                txn.rollback();
                LOG.severe("SERVER ERROR");
            }
        }
    }

    @Override
    public Response resendEmailKey(String key) {
        LOG.info("Validating email key: " + key);

        TransactionOptions options = TransactionOptions.Builder.withXG(true);
        Transaction txn = datastore.beginTransaction(options);

        try {
            Entity user;
            try {
                user = EmailValidationKey.getUserExpired(key);
            } catch (InvalidEmailKeyException e) {
                return Response.status(Response.Status.FORBIDDEN).entity(OutputStatus.INVALID_KEY).build();
            }

            String email = (String) user.getProperty(User.email);
            String new_key = EmailValidationKey.getKey(user.getKey().getName());
            EmailResource.sendEmailValidation(user.getKey().getName(), email, new_key);
            LOG.info("Email resent: " + user.getKey().getName());

            return Response.ok(OutputStatus.EMAIL_RESENT).build();
        } finally {
            if (txn != null && txn.isActive()) {
                txn.rollback();
                LOG.severe("SERVER ERROR");
            }
        }

    }

    @Override
    public Response userConfirmDelete(String device, String key) {
        LOG.info("Validating deletion key: " + key);

        Entity user;
        try {
            user = EmailValidationKey.getUserValid(key);
        } catch (ExpiredEmailVerificationException e) {
            return Response.status(Response.Status.FORBIDDEN).entity(OutputStatus.EXPIRED_KEY).build();
        } catch (InvalidEmailKeyException e) {
            return Response.status(Response.Status.FORBIDDEN).entity(OutputStatus.INVALID_KEY).build();
        }

        TransactionOptions options = TransactionOptions.Builder.withXG(true);
        Transaction txn = datastore.beginTransaction(options);

        try {
            user.setProperty(User.last_edit_time, new Date());
            user.setProperty(User.for_removal, true);
            datastore.put(txn, user);

            txn.commit();
            return Response.ok(OutputStatus.USER_MARKED_DELETION).build();
        } finally {
            if (txn != null && txn.isActive()) {
                txn.rollback();
                LOG.severe("SERVER ERROR");
            }
        }
    }
}
