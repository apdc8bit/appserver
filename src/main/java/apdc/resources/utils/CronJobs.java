package apdc.resources.utils;

import apdc.gamification.PointSystem;
import apdc.input.verification.values.OccurrenceValues;
import apdc.input.verification.values.StatsValues;
import apdc.tables.occurrence.Occurrences;
import apdc.tables.special.EmailValidation;
import apdc.tables.special.Images;
import apdc.tables.user.User;
import apdc.util.CalendarUtils;
import apdc.util.email.EmailResource;
import com.google.appengine.api.datastore.*;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

@Path("/cron")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class CronJobs {
    private static final int daysBeforeUserRemoval = 14;
    private static final int daysBeforeOccurrenceArchived = 7;
    private static final int daysBeforeImageRemoved = 1;
    private static final int daysBeforeEmailKeyRemoval = 1;

    private static final Logger LOG = Logger.getLogger(CronJobs.class.getName());
    private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

    public CronJobs() {
    }

    @POST
    @Path("/executeTokenRemoval")
    public Response executeTokenRemoval() {
        long tokens_removed = 0L, emails_removed = 0L;

        /* TODO removed because off o time limitations (change from stateless sessions to session with state)
        Query.Filter propertyFilter = new Query.FilterPredicate(Tokens.expirationDate, Query.FilterOperator.GREATER_THAN_OR_EQUAL, System.currentTimeMillis());
        Query ctrQuery = new Query(Tokens.kind).setFilter(propertyFilter);
        List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());
        for (Entity e : results) {
            datastore.delete(e.getKey());
            tokens_removed++;
        }
        */

        //Emails
        Date date_before = CalendarUtils.addDays(new Date(), -daysBeforeEmailKeyRemoval);
        Query.Filter propertyFilter2 = new Query.FilterPredicate(EmailValidation.expiration_date, Query.FilterOperator.GREATER_THAN_OR_EQUAL, date_before);
        Query ctrQuery2 = new Query(EmailValidation.kind)
                .setFilter(propertyFilter2);

        List<Entity> results2 = datastore.prepare(ctrQuery2).asList(FetchOptions.Builder.withDefaults());

        for (Entity e : results2) {
            datastore.delete(e.getKey());
            emails_removed++;
        }

        LOG.info("Tokens removed: " + tokens_removed + " | Emails: " + emails_removed);
        return Response.ok().build();
    }

    @POST
    @Path("/executeUserRemoval")
    public Response executeUserRemoval() {
        Query.Filter propertyFilter = new Query.FilterPredicate(User.for_removal, Query.FilterOperator.EQUAL, true);
        Date date_before = CalendarUtils.addDays(new Date(), -daysBeforeUserRemoval);
        Query.Filter propertyFilter2 = new Query.FilterPredicate(User.last_edit_time, Query.FilterOperator.LESS_THAN, date_before);

        Query.CompositeFilter and_filter = Query.CompositeFilterOperator.and(propertyFilter, propertyFilter2);

        Query ctrQuery = new Query(User.kind)
                .setFilter(and_filter);

        List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());
        long users_removed = 0L;

        for (Entity e : results) {
            String filename = (String) e.getProperty(User.imageID);
            //GcsServlet.deleteFile(filename, GcsServlet.FILE.user_image);
            datastore.delete(e.getKey());
            PointSystem.removeUserEntry(e.getKey().getName());
            users_removed++;
        }

        LOG.info("Users removed: " + users_removed);
        return Response.ok().build();
    }

    @POST
    @Path("/executeImageCleanup")
    public Response executeImageCleanup() {
        Date date_before = CalendarUtils.addDays(new Date(), -daysBeforeImageRemoved);

        Query.Filter propertyFilter = new Query.FilterPredicate(Images.addedTime, Query.FilterOperator.LESS_THAN, date_before);
        Query ctrQuery = new Query(Images.kind).setFilter(propertyFilter);

        List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withLimit(20));

        long imagesRemoved = 0L;
        for (Entity e : results) {
            String imageID = (String) e.getProperty(Images.id);
            if (imageID != null) {
                Query.Filter propertyFilter2 = new Query.FilterPredicate(Occurrences.imageID, Query.FilterOperator.EQUAL, imageID);
                Query ctrQuery2 = new Query(Occurrences.kind).setFilter(propertyFilter2);
                List<Entity> results2 = datastore.prepare(ctrQuery2).asList(FetchOptions.Builder.withDefaults());

                if (results2.isEmpty()) {
                    imagesRemoved++;
                    EmailResource.dev("ImageID: " + imageID);
                    //GcsServlet.deleteFile(imageID, GcsServlet.FILE.occurrence_image); TODO jusrt in case
                }

                datastore.delete(e.getKey());
            }
        }

        LOG.info("Images removed: " + imagesRemoved);
        return Response.ok().build();
    }

    @POST
    @Path("/executeOccurrenceArchive")
    public Response executeOccurrenceArchive() {
        Date date_before = CalendarUtils.addDays(new Date(), -daysBeforeOccurrenceArchived);

        Query.Filter propertyFilter = new Query.FilterPredicate(Occurrences.completion_time, Query.FilterOperator.LESS_THAN, date_before);
        Query.Filter propertyFilter2 = new Query.FilterPredicate(Occurrences.status, Query.FilterOperator.EQUAL, OccurrenceValues.STATUS.completed.name());
        Query.Filter propertyFilter3 = new Query.FilterPredicate(Occurrences.archived, Query.FilterOperator.EQUAL, false);

        Query.CompositeFilter and_filter = Query.CompositeFilterOperator.and(propertyFilter, propertyFilter2, propertyFilter3);

        Query ctrQuery = new Query(Occurrences.kind)
                .setFilter(and_filter);

        List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());

        long occurrences_archived = 0L;

        for (Entity e : results) {
            e.setProperty(Occurrences.archived, true);
            datastore.put(e);
            occurrences_archived++;
        }

        LOG.info("Occurrences archived: " + occurrences_archived);
        return Response.ok().build();
    }

    @GET
    @Path("/databaseCleanup")
    public Response databaseCleanup() {
        LOG.fine("Queueing Cleanup Service...");
        Queue queue = QueueFactory.getDefaultQueue();
        queue.add(TaskOptions.Builder.withUrl("/rest/cron/executeTokenRemoval"));
        queue.add(TaskOptions.Builder.withUrl("/rest/cron/executeUserRemoval"));
        queue.add(TaskOptions.Builder.withUrl("/rest/cron/executeOccurrenceArchive"));
        //queue.add(TaskOptions.Builder.withUrl("/rest/cron/executeImageCleanup"));
        //queue.add(TaskOptions.Builder.withUrl("/rest/cron/checkOccurrences"));
        return Response.ok().build();
    }

    @GET
    @Path("/updateRiskDatabase")
    public Response updateRiskDatabase() {
        LOG.fine("Queueing Database Update Service...");
        Queue queue = QueueFactory.getDefaultQueue();
        queue.add(TaskOptions.Builder.withUrl("/rest/cron/executeDatabaseUpdate"));
        return Response.ok().build();
    }

    @POST
    @Path("/executeDatabaseUpdate")
    public Response executeDatabaseUpdate() {
        StatsValues.loadBasicStats();
        return Response.ok().build();
    }

    @POST
    @Path("/checkOccurrences/{username}/{id}")
    public Response checkOccurrences(@PathParam("username") String username, @PathParam("id") long id) {
        /*
        Query.Filter propertyFilter = new Query.FilterPredicate(Occurrences.checked, Query.FilterOperator.EQUAL, false);
        Query ctrQuery = new Query(Occurrences.kind).setFilter(propertyFilter);

        List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withLimit(50));
        List<Entity> in = new LinkedList<>();
        */
        Key userKey = KeyFactory.createKey(User.kind, username);
        Key occKey = KeyFactory.createKey(userKey, Occurrences.kind, id);

        try {
            Entity occ = datastore.get(occKey);
            double lat = (double) occ.getProperty(Occurrences.lat);
            double lng = (double) occ.getProperty(Occurrences.lng);

            //Check if in water
            if (OccurrenceValues.isInWater(lat, lng)) {
                LOG.warning("Occ in Water: " + id);
                datastore.delete(occ.getKey());
            } else {
                //Check if in Portugal and get Address
                OccurrenceValues.GeoCode geo = OccurrenceValues.getAddress(lat, lng);
                if (!geo.inPT) {
                    LOG.warning("Occ not in PT: " + id);
                    datastore.delete(occ.getKey());
                } else {
                    occ.setProperty(Occurrences.checked, true);
                    occ.setIndexedProperty(Occurrences.address, geo.address);
                    datastore.put(occ);
                }
            }
        } catch (EntityNotFoundException e) {
            LOG.warning("Occ not found");
        }

        /*
        int waterDeleted = 0, placeDeleted = 0;

        for (Entity e : results) {
            double lat = (double) e.getProperty(Occurrences.lat);
            double lng = (double) e.getProperty(Occurrences.lng);

            //Check if in water
            if (OccurrenceValues.isInWater(lat, lng)) {
                datastore.delete(e.getKey());
                waterDeleted++;
            } else {
                //Check if in Portugal and get Address
                OccurrenceValues.GeoCode geo = OccurrenceValues.getAddress(lat, lng);
                if (!geo.inPT) {
                    datastore.delete(e.getKey());
                    placeDeleted++;
                } else {
                    e.setProperty(Occurrences.checked, true);
                    e.setIndexedProperty(Occurrences.address, geo.address);

                    in.add(e);
                }
            }
        }
        datastore.put(in);
        LOG.info("OCCS DELETED: -WATER: " + waterDeleted + " | PLACE: " + placeDeleted);
        */

        return Response.ok().build();
    }

}
