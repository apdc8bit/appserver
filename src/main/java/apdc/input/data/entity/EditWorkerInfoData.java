package apdc.input.data.entity;

import apdc.input.data.InputDataInterface;
import apdc.input.data.basic.AdditionalRegistrationData;
import apdc.input.verification.utils.InputDataVerification;

public class EditWorkerInfoData extends AdditionalRegistrationData implements InputDataInterface {
    public String username;

    @Override
    public boolean validateData() {
        username = username == null ? "" : username.toLowerCase();
        boolean valid = super.validateData();

        return valid && InputDataVerification.validateText(username, InputDataVerification.TYPE.USERNAME);
    }

}
