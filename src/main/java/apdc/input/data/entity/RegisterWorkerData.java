package apdc.input.data.entity;

import apdc.input.data.InputDataInterface;
import apdc.input.data.basic.InitialRegistrationData;
import apdc.input.verification.utils.InputDataVerification;

public class RegisterWorkerData extends InitialRegistrationData implements InputDataInterface {
    public String name;
    public String zipcode;
    public int phoneNumber;
    public int nif;
    public String imageID;
    public String address;
    public boolean isVolunteer;

    public RegisterWorkerData() {
    }

    @Override
    public boolean validateData() {
        boolean valid = super.validateData();

        name = name == null ? "" : name;
        zipcode = zipcode == null ? "" : zipcode;
        imageID = imageID == null ? "" : imageID;
        address = address == null ? "" : InputDataVerification.cleanText(address);

        if (!imageID.isEmpty())
            valid = valid && InputDataVerification.validateText(imageID, InputDataVerification.TYPE.ID);

        return InputDataVerification.validateText(name, InputDataVerification.TYPE.NAME) &&
                InputDataVerification.validateNumber(phoneNumber, InputDataVerification.TYPE.PHONE_NUMBER) &&
                InputDataVerification.validateText(zipcode, InputDataVerification.TYPE.ZIPCODE) &&
                InputDataVerification.validateNumber(nif, InputDataVerification.TYPE.NIF) &&
                InputDataVerification.validateText(address, InputDataVerification.TYPE.ADDRESS) && valid;
    }
}
