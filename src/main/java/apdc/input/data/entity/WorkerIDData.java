package apdc.input.data.entity;

import apdc.input.data.InputDataInterface;
import apdc.input.verification.utils.InputDataVerification;

public class WorkerIDData implements InputDataInterface {
    public String username;

    public WorkerIDData() {
    }

    @Override
    public boolean validateData() {
        username = username == null ? "" : username.toLowerCase();
        return InputDataVerification.validateText(username, InputDataVerification.TYPE.USERNAME);
    }
}
