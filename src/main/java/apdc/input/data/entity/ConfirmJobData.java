package apdc.input.data.entity;

import apdc.input.data.InputDataInterface;
import apdc.input.data.occurrences.AssociateWorkerOccurrenceData;

public class ConfirmJobData extends AssociateWorkerOccurrenceData implements InputDataInterface {

    public boolean confirm;

}
