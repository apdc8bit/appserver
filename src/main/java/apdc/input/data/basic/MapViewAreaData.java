package apdc.input.data.basic;

import apdc.input.data.InputDataInterface;
import apdc.input.verification.utils.InputDataVerification;

public class MapViewAreaData implements InputDataInterface {
    public double top_left_lat;
    public double top_left_lng;
    public double bottom_right_lat;
    public double bottom_right_lng;

    public MapViewAreaData() {
    }

    @Override
    public boolean validateData() {
        return InputDataVerification.validateNumber(top_left_lat, InputDataVerification.TYPE.LATITUDE) &&
                InputDataVerification.validateNumber(bottom_right_lat, InputDataVerification.TYPE.LATITUDE) &&
                InputDataVerification.validateNumber(top_left_lng, InputDataVerification.TYPE.LONGITUDE) &&
                InputDataVerification.validateNumber(bottom_right_lng, InputDataVerification.TYPE.LONGITUDE) &&
                top_left_lat > bottom_right_lat && top_left_lng < bottom_right_lng;
    }

}
