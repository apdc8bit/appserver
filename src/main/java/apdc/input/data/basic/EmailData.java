package apdc.input.data.basic;

import apdc.input.data.InputDataInterface;
import apdc.input.verification.utils.InputDataVerification;

public class EmailData implements InputDataInterface {
    public String email;
    public String password;

    public EmailData() {
    }

    @Override
    public boolean validateData() {
        email = email == null ? "" : email;
        password = password == null ? "" : password;
        return InputDataVerification.validateText(email, InputDataVerification.TYPE.EMAIL) &&
                InputDataVerification.validateText(password, InputDataVerification.TYPE.PASSWORD);
    }
}
