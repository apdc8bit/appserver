package apdc.input.data.basic;

import apdc.input.data.InputDataInterface;
import apdc.input.verification.utils.InputDataVerification;

public class PasswordChangeData implements InputDataInterface {
    public String password_old;
    public String password_new;
    public String password_new_conf;

    public PasswordChangeData() {
    }

    @Override
    public boolean validateData() {
        password_old = password_old == null ? "" : password_old;
        password_new = password_new == null ? "" : password_new;
        password_new_conf = password_new_conf == null ? "" : password_new_conf;

        return password_new.equals(password_new_conf) &&
                InputDataVerification.validateText(password_old, InputDataVerification.TYPE.PASSWORD) &&
                InputDataVerification.validateText(password_new, InputDataVerification.TYPE.PASSWORD) &&
                InputDataVerification.validateText(password_new_conf, InputDataVerification.TYPE.PASSWORD) &&
                //Check if new pass is diff than old
                !password_old.equals(password_new);
    }

}
