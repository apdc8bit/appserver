package apdc.input.data.basic;

import apdc.input.data.InputDataInterface;
import apdc.input.verification.utils.InputDataVerification;

public class ConfirmPasswordData implements InputDataInterface {
    public String password;

    @Override
    public boolean validateData() {
        return InputDataVerification.validateText(password, InputDataVerification.TYPE.PASSWORD);
    }
}
