package apdc.input.data.basic;

import apdc.input.data.InputDataInterface;
import apdc.input.verification.utils.InputDataVerification;

public class InitialRegistrationData implements InputDataInterface {
    public String email;
    public String username;
    public String password;
    public String confirmation;

    public InitialRegistrationData() {
    }

    @Override
    public boolean validateData() {
        email = email == null ? "" : email;
        password = password == null ? "" : password;
        username = username == null ? "" : username.toLowerCase();
        confirmation = confirmation == null ? "" : confirmation;

        return InputDataVerification.validateText(password, InputDataVerification.TYPE.PASSWORD) &&
                InputDataVerification.validateText(email, InputDataVerification.TYPE.EMAIL) &&
                InputDataVerification.validateText(username, InputDataVerification.TYPE.USERNAME) &&
                password.equals(confirmation);
    }
}