package apdc.input.data.basic;

import apdc.input.data.InputDataInterface;
import apdc.input.verification.utils.InputDataVerification;

public class LoginData implements InputDataInterface {
    public String username;
    public String password;
    public String deviceID;

    public LoginData() {
    }

    @Override
    public boolean validateData() {
        username = username == null ? "" : username.toLowerCase();
        password = password == null ? "" : password;
        deviceID = deviceID == null ? "" : deviceID;

        return InputDataVerification.validateText(username, InputDataVerification.TYPE.USERNAME) &&
                InputDataVerification.validateText(password, InputDataVerification.TYPE.PASSWORD);
    }

}
