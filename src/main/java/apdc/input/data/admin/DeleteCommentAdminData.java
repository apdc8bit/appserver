package apdc.input.data.admin;

import apdc.input.data.InputDataInterface;
import apdc.input.data.occurrences.comments.CommentIDData;
import apdc.input.verification.utils.InputDataVerification;

public class DeleteCommentAdminData extends CommentIDData implements InputDataInterface {
    public String admin_password;

    @Override
    public boolean validateData() {
        if (!super.validateData())
            return false;

        return InputDataVerification.validateText(admin_password, InputDataVerification.TYPE.PASSWORD);
    }
}
