package apdc.input.data.admin;

import apdc.input.data.InputDataInterface;
import apdc.input.verification.utils.InputDataVerification;

public class AdminPassData implements InputDataInterface {

    public String password;

    public AdminPassData() {
    }

    @Override
    public boolean validateData() {
        password = password == null ? "" : password;

        return InputDataVerification.validateText(password, InputDataVerification.TYPE.PASSWORD);
    }
}
