package apdc.input.data.admin;

import apdc.input.data.InputDataInterface;
import apdc.input.data.entity.RegisterWorkerData;
import apdc.input.verification.utils.InputDataVerification;

public class RegisterBackOfficeData extends RegisterWorkerData implements InputDataInterface {
    public String admin_password;
    public String role;

    public RegisterBackOfficeData() {

    }

    @Override
    public boolean validateData() {
        password = password == null ? "" : password;
        role = role == null ? "" : role;

        return InputDataVerification.validateText(password, InputDataVerification.TYPE.PASSWORD) && super.validateData();
    }

}
