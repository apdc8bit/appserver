package apdc.input.data.admin;

import apdc.input.data.InputDataInterface;
import apdc.input.verification.utils.InputDataVerification;

public class UserIDData extends AdminPassData implements InputDataInterface {
    public String username;

    public UserIDData() {
    }

    @Override
    public boolean validateData() {
        if (!super.validateData())
            return false;

        username = username == null ? "" : username.toLowerCase();
        return InputDataVerification.validateText(username, InputDataVerification.TYPE.USERNAME);
    }
}
