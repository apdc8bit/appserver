package apdc.input.data.admin;

import apdc.input.data.InputDataInterface;
import apdc.input.data.basic.InitialRegistrationData;
import apdc.input.verification.utils.InputDataVerification;

public class RegisterAdminData extends InitialRegistrationData implements InputDataInterface {

    public String admin_password;

    @Override
    public boolean validateData() {
        boolean valid = super.validateData();

        return valid && InputDataVerification.validateText(admin_password, InputDataVerification.TYPE.PASSWORD);
    }

}
