package apdc.input.data.occurrences;

import apdc.input.data.InputDataInterface;
import apdc.input.verification.utils.InputDataVerification;
import apdc.input.verification.values.OccurrenceValues;

public class OccurrenceChangeTypeData implements InputDataInterface {
    public String value;
    public long occurrence_id;

    public OccurrenceChangeTypeData() {
    }

    @Override
    public boolean validateData() {
        value = value == null ? "" : OccurrenceValues.getStatus(value);

        if (occurrence_id == 0L)
            return false;

        return InputDataVerification.validateNumber(occurrence_id, InputDataVerification.TYPE.ID);
    }
}
