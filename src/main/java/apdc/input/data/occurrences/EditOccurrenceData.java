package apdc.input.data.occurrences;

import apdc.input.data.InputDataInterface;
import apdc.input.verification.utils.InputDataVerification;

import java.util.ArrayList;

public class EditOccurrenceData extends CreateOccurrenceData implements InputDataInterface {
    public long id;
    public String username;

    public EditOccurrenceData() {
    }

    @Override
    public boolean validateData() {
        imageID = imageID == null ? "" : InputDataVerification.cleanText(imageID);
        title = title == null ? "" : InputDataVerification.cleanText(title);
        description = description == null ? "" : InputDataVerification.cleanText(description);
        username = username == null ? "" : username.toLowerCase();
        type = type == null ? "" : type;
        lats = lats == null ? new ArrayList<Double>() : lats;
        lngs = lngs == null ? new ArrayList<Double>() : lngs;

        boolean valid = true;

        if (username.isEmpty() || !InputDataVerification.validateText(username, InputDataVerification.TYPE.USERNAME))
            return false;
        if (id == 0 || !InputDataVerification.validateNumber(id, InputDataVerification.TYPE.ID))
            return false;

        if (lats.isEmpty() && lngs.isEmpty()) {
            if (lat != 0 && lng != 0)
                valid = InputDataVerification.validateNumber(lat, InputDataVerification.TYPE.LATITUDE) &&
                        InputDataVerification.validateNumber(lng, InputDataVerification.TYPE.LONGITUDE);
        } else if (valid = InputDataVerification.validateCoordinates(lats, lngs)) {
            double[] points = InputDataVerification.generateCenter(lats, lngs);
            lat = points[0];
            lng = points[0];
        }

        valid = valid && (imageID.isEmpty() || InputDataVerification.validateText(imageID, InputDataVerification.TYPE.ID));
        valid = valid && (title.isEmpty() || InputDataVerification.validateText(title, InputDataVerification.TYPE.TITLE));
        valid = valid && (description.isEmpty() || InputDataVerification.validateText(description, InputDataVerification.TYPE.DESCRIPTION));

        return valid;
    }

}
