package apdc.input.data.occurrences;

import apdc.input.data.InputDataInterface;
import apdc.input.verification.utils.InputDataVerification;

public class MarkJobCompletedData implements InputDataInterface {
    public long occurrence_id;
    public String imageID;

    @Override
    public boolean validateData() {
        imageID = imageID == null ? "" : imageID;

        if (imageID.isEmpty() || occurrence_id == 0L)
            return false;

        return InputDataVerification.validateText(imageID, InputDataVerification.TYPE.IMAGE) &&
                InputDataVerification.validateNumber(occurrence_id, InputDataVerification.TYPE.ID);
    }
}
