package apdc.input.data.occurrences.votes;

import apdc.input.data.InputDataInterface;
import apdc.input.verification.utils.InputDataVerification;

public class VoteOccurrenceData implements InputDataInterface {

    public long id;
    public String username;

    public VoteOccurrenceData() {

    }

    @Override
    public boolean validateData() {
        username = username == null ? "" : username.toLowerCase();

        if (id == 0L || username.isEmpty())
            return false;

        return InputDataVerification.validateText(username, InputDataVerification.TYPE.USERNAME) &&
                InputDataVerification.validateNumber(id, InputDataVerification.TYPE.ID);
    }

}
