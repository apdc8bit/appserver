package apdc.input.verification.values;

import apdc.input.verification.utils.Reply;
import apdc.output.info.JobsInfo;
import apdc.output.info.OccurrenceInfo;
import apdc.output.info.WorkersInfo;
import apdc.output.messages.OutputStatus;
import apdc.tables.user.User;
import apdc.tables.worker.WorkerByEntity;
import apdc.tables.worker.WorkerJobs;
import com.google.appengine.api.datastore.*;
import com.google.gson.Gson;

import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

public class WorkerValues {
    public enum States {
        TODO, WAITING_CONFIRMATION, COMPLETED
    }

    private static final Logger LOG = Logger.getLogger(WorkerValues.class.getName());
    private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
    private static final Gson g = new Gson();

    public static Reply isWorkerFromEntity(String worker, Key entity) {
        try {
            Key workerKey = KeyFactory.createKey(User.kind, worker);

            //Check if worker was created by this entity
            Query.Filter propertyFilter = new Query.FilterPredicate(WorkerByEntity.worker_key, Query.FilterOperator.EQUAL, workerKey);
            Query ctrQuery = new Query(WorkerByEntity.kind).setAncestor(entity).setFilter(propertyFilter);
            List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());
            LOG.info("Found: " + results.size());
            LOG.info("Worker: " + worker);
            LOG.info("Entity: " + entity.getName());
            if (results.isEmpty()) {
                LOG.warning("Worker is not from Entity");
                return new Reply("", null, g.toJson(OutputStatus.Messages.WORKER_IS_NOT_FROM_ENTITY), Response.Status.FORBIDDEN);
            }

            Entity userEntity = datastore.get(workerKey);
            return new Reply(userEntity.getKey().getName(), userEntity, "", Response.Status.OK);
        } catch (EntityNotFoundException e) {
            LOG.warning("Worker doesn't exist.");
            return new Reply("", null, g.toJson(OutputStatus.Messages.USER_DOES_NOT_EXIST), Response.Status.BAD_REQUEST);
        }
    }

    public static List<WorkersInfo> listWorkers(List<Entity> workers) {
        List<WorkersInfo> out = new LinkedList<>();

        for (Entity e : workers) {
            String username = ((Key) e.getProperty(WorkerByEntity.worker_key)).getName();
            Date added_date = (Date) e.getProperty(WorkerByEntity.added_time);
            out.add(new WorkersInfo(username, added_date));
        }

        return out;
    }

    public static List<JobsInfo> listJobs(List<Entity> jobs) {
        List<JobsInfo> out = new LinkedList<>();

        for (Entity e : jobs) {
            Key occKey = (Key) e.getProperty(WorkerJobs.occurrence_key);
            String worker_username = e.getParent().getName();
            String occurrence_username = occKey.getParent().getName();
            long occurrence_id = ((Key) e.getProperty(WorkerJobs.occurrence_key)).getId();
            Date date_started = ((Date) e.getProperty(WorkerJobs.added_time));
            Date date_finished = ((Date) e.getProperty(WorkerJobs.finish_time));
            String state = (String) e.getProperty(WorkerJobs.state);
            String imageID = (String) e.getProperty(WorkerJobs.imageID);

            OccurrenceInfo occ;
            try {
                occ = OccurrenceValues.getOccurrenceInfo(datastore.get(occKey), null, null);
            } catch (EntityNotFoundException ef) {
                occ = null;
            }

            out.add(new JobsInfo(worker_username, occurrence_username, occurrence_id, date_started, date_finished, state, imageID, occ));
        }

        return out;
    }

    public static States getState(String state) {
        try {
            return States.valueOf(state.toUpperCase());
        } catch (IllegalArgumentException | NullPointerException e) {
            return null;
        }
    }

}
