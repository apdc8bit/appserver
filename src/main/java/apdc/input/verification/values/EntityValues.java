package apdc.input.verification.values;

import apdc.dico.DicoLatLng;
import apdc.output.info.EntityInfo;
import apdc.output.info.WorkerStats;
import apdc.resources.entities.EntityResource;
import apdc.tables.entity.EntityStats;
import apdc.tables.occurrence.Occurrences;
import apdc.tables.user.User;
import apdc.tables.worker.WorkerByEntity;
import apdc.tables.worker.WorkerJobs;
import com.google.appengine.api.datastore.*;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class EntityValues {
    private static final Logger LOG = Logger.getLogger(EntityResource.class.getName());
    private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
    private final Gson g = new Gson();


    public static List<EntityInfo> getEntitiesInfo(List<Entity> entities) {
        List<EntityInfo> info = new LinkedList<>();

        for (Entity e : entities)
            info.add(getEntityStats(e));

        return info;
    }

    public static List<WorkerStats> getPrivateStats(Entity entity) {
        Query ctrQuery = new Query(WorkerByEntity.kind).setAncestor(entity.getKey());
        List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());

        List<WorkerStats> stats = new LinkedList<>();

        for (Entity e : results)
            stats.add(getWorkerStats(e));

        return stats;
    }

    public static WorkerStats getWorkerStats(Entity worker) {
        Query ctrQuery = new Query(WorkerJobs.kind).setAncestor(worker.getKey());
        List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());

        Long avgTimeFinish = -1L, numbJobsStarted = 0L, numJobsFinish = 0L, numbJobsWaiting = 0L;

        for (Entity e : results) {
            Date start = (Date) e.getProperty(WorkerJobs.added_time);
            Date end = (Date) e.getProperty(WorkerJobs.finish_time);
            Date conf = (Date) e.getProperty(WorkerJobs.confirmed_time);

            //Probls works...
            numbJobsStarted++;
            if (conf != null)
                numJobsFinish++;
            else {
                if (end != null)
                    numbJobsWaiting++;
            }
            if (end != null) {
                if (avgTimeFinish == -1.0)
                    avgTimeFinish = TimeUnit.MILLISECONDS.toHours((end.getTime() - start.getTime()));
                else {
                    avgTimeFinish = TimeUnit.MILLISECONDS.toHours(((avgTimeFinish * (numJobsFinish - 1)) + (end.getTime() - start.getTime())) / numJobsFinish);
                }
            }
        }

        return new WorkerStats(avgTimeFinish, numbJobsStarted, numJobsFinish, numbJobsWaiting);
    }

    public static EntityInfo getEntityStats(Entity entity) {
        String imageID, district, county, address, name, dico;
        Date creation_time;
        Double lat, lng;
        Long totalStarted, totalCompleted, avgResponseTime, avgCompletionTime;
        imageID = (String) entity.getProperty(User.imageID);
        district = (String) entity.getProperty(User.district);
        county = (String) entity.getProperty(User.county);
        address = (String) entity.getProperty(User.address);
        name = (String) entity.getProperty(User.name);
        creation_time = (Date) entity.getProperty(User.creation_time);
        lat = (Double) entity.getProperty(User.lat);
        lng = (Double) entity.getProperty(User.lng);
        dico = (String) entity.getProperty(User.dico);

        try {
            String key = KeyFactory.keyToString(entity.getKey());
            Key statsKey = KeyFactory.createKey(EntityStats.kind, key);
            Entity eStats = datastore.get(statsKey);
            totalStarted = (Long) eStats.getProperty(EntityStats.numbStarted);
            totalCompleted = (Long) eStats.getProperty(EntityStats.numbFinished);
            avgResponseTime = (Long) eStats.getProperty(EntityStats.avgResponseTime);
            avgCompletionTime = (Long) eStats.getProperty(EntityStats.avgCompletionTime);
        } catch (EntityNotFoundException f) {
            totalStarted = -1L;
            totalCompleted = -1L;
            avgResponseTime = -1L;
            avgCompletionTime = -1L;
        }

        return new EntityInfo(imageID, district, county, address, name, creation_time, lat, lng, totalStarted, totalCompleted, avgResponseTime, avgCompletionTime, dico);
    }

    public static void startedOccurrence(Key entityKey, Entity ass, Entity occ) {
        String key = KeyFactory.keyToString(entityKey);
        Key statsKey = KeyFactory.createKey(EntityStats.kind, key);

        Entity stats;
        Long numbStarted = 1L;
        try {
            stats = datastore.get(statsKey);
            numbStarted = (Long) stats.getProperty(EntityStats.numbStarted);
            numbStarted = numbStarted == null ? 1L : numbStarted + 1;
            stats.setProperty(EntityStats.numbStarted, numbStarted);
        } catch (EntityNotFoundException e) {
            stats = new Entity(EntityStats.kind, key);
            stats.setProperty(EntityStats.numbStarted, numbStarted);
            stats.setProperty(EntityStats.numbFinished, 0L);
        }

        Date created = (Date) occ.getProperty(Occurrences.creation_time);
        Date associated = (Date) ass.getProperty(WorkerJobs.added_time);

        long timeResponse = TimeUnit.MILLISECONDS.toHours(associated.getTime() - created.getTime());

        Long avgResponseTime = (Long) stats.getProperty(EntityStats.avgResponseTime);//hours

        if (avgResponseTime == null) {
            stats.setProperty(EntityStats.avgResponseTime, timeResponse);
        } else {
            avgResponseTime = ((avgResponseTime * (numbStarted - 1)) + timeResponse) / (numbStarted);
            stats.setProperty(EntityStats.avgResponseTime, avgResponseTime);
        }

        datastore.put(stats);
    }

    public static void finishedOccurrence(Key entityKey, Entity ass, Entity occ) {
        String key = KeyFactory.keyToString(entityKey);
        Key statsKey = KeyFactory.createKey(EntityStats.kind, key);

        Entity stats;
        Long numbFinished = 1L;
        try {
            stats = datastore.get(statsKey);
            numbFinished = (Long) stats.getProperty(EntityStats.numbFinished);
            numbFinished = numbFinished == null ? 1L : numbFinished + 1;
            stats.setProperty(EntityStats.numbFinished, numbFinished);
        } catch (EntityNotFoundException e) {
            stats = new Entity(EntityStats.kind, key);
            stats.setProperty(EntityStats.numbStarted, 1L);
            stats.setProperty(EntityStats.numbFinished, 1L);
        }

        Date started = (Date) occ.getProperty(Occurrences.creation_time);
        Date finished = (Date) occ.getProperty(Occurrences.completion_time);

        long timeComplete = TimeUnit.MILLISECONDS.toHours(finished.getTime() - started.getTime());

        Long avgCompleteTime = (Long) stats.getProperty(EntityStats.avgResponseTime);

        if (avgCompleteTime == null) {
            stats.setProperty(EntityStats.avgCompletionTime, timeComplete);
        } else {
            avgCompleteTime = ((avgCompleteTime * (numbFinished - 1)) + timeComplete) / (numbFinished);
            stats.setProperty(EntityStats.avgCompletionTime, avgCompleteTime);
        }

        datastore.put(stats);
    }

    public static DicoLatLng getDicoFromAddress(String address, String postalCode) {
        String request_address = (address + ", " + postalCode + ", PT").replace(" ", "+");
        String uri = "https://maps.googleapis.com/maps/api/geocode/json?address=" + request_address + "&key=AIzaSyDUiaTDhTj0SoOkNgHWqsLH5Z080HAascM";
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(uri);

        String reply = "";
        try {
            HttpResponse response = client.execute(request);
            HttpEntity entity = response.getEntity();
            boolean ok = response.getStatusLine().toString().split(" ")[1].equals("200");
            if (ok && entity != null) {
                try (InputStream stream = entity.getContent()) {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        reply = reply.concat(line);
                    }
                }
            } else {
                // If the API has reached the limit allow everything
                return null;
            }
        } catch (IOException ef) {
            return null;
        }

        JsonParser parser = new JsonParser();
        JsonObject o = parser.parse(reply).getAsJsonObject();

        String status = o.get("status").getAsString();

        if (!status.equals("OK")) {
            return new DicoLatLng(-1, -1);
        }

        JsonObject results = o
                .getAsJsonArray("results")
                .get(0).getAsJsonObject()
                .getAsJsonObject("geometry")
                .getAsJsonObject("location");

        double lat = results.get("lat").getAsDouble();
        double lng = results.get("lng").getAsDouble();

        return new DicoLatLng(lat, lng);
    }


}
