package apdc.input.verification.values;

import apdc.dico.DicoData;
import apdc.gamification.PointSystem;
import apdc.input.data.entity.EventData;
import apdc.input.verification.utils.Reply;
import apdc.output.info.BasicUserInfo;
import apdc.output.info.EntityInfo;
import apdc.output.info.EventInfo;
import apdc.permissions.UserPermissions;
import apdc.tables.entity.Events;
import apdc.tables.entity.EventsAss;
import apdc.tables.user.User;
import com.google.appengine.api.datastore.*;
import com.google.gson.Gson;

import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

public class VolunteerValues {
    private static final Logger LOG = Logger.getLogger(VolunteerValues.class.getName());
    private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
    private static final Gson g = new Gson();

    public enum Filters {
        all, started, archived
    }

    public enum States {
        started, archived
    }

    private static Filters getFilter(String filter) {
        filter = filter == null ? "" : filter.toLowerCase();
        try {
            return Filters.valueOf(filter);
        } catch (IllegalArgumentException | NullPointerException e) {
            return Filters.all;
        }
    }

    public static Entity addEvent(EventData data, Key workerKey, Key entityKey, Date date) {
        //check if a similar event exist? nobody got time fo dat
        Entity event = new Entity(Events.kind);
        event.setProperty(Events.title, data.title);
        event.setProperty(Events.description, data.description);
        event.setProperty(Events.eventTime, date);
        event.setProperty(Events.lat, data.lat);
        event.setProperty(Events.lng, data.lng);
        event.setProperty(Events.workerKey, workerKey);
        event.setProperty(Events.entityKey, entityKey);
        event.setProperty(Events.state, VolunteerValues.States.started.name());

        DicoData dicoData = OccurrenceValues.getDico(data.lat, data.lng);
        if (dicoData != null) {
            event.setProperty(Events.dico, dicoData.dico);
            event.setProperty(Events.county, dicoData.county);
            event.setProperty(Events.district, dicoData.district);
        }

        OccurrenceValues.GeoCode geocode = OccurrenceValues.getAddress(data.lat, data.lng);
        if (geocode.address != null && !geocode.address.isEmpty())
            event.setProperty(Events.address, geocode.address);

        return event;
    }

    public static Query.Filter getQueryFilter(String filter) {
        switch (getFilter(filter)) {
            case all:
                return new Query.FilterPredicate(Events.state, Query.FilterOperator.NOT_EQUAL, "");
            case started:
                return new Query.FilterPredicate(Events.state, Query.FilterOperator.EQUAL, "started");
            case archived:
                return new Query.FilterPredicate(Events.state, Query.FilterOperator.EQUAL, "archived");
            default:
                return new Query.FilterPredicate(Events.state, Query.FilterOperator.NOT_EQUAL, "");
        }
    }

    public static Entity archiveEvent(Entity event) {
        Query usersQuery = new Query(EventsAss.kind).setFilter(new Query.FilterPredicate(EventsAss.eventKey, Query.FilterOperator.EQUAL, event.getKey()));
        List<Entity> results = datastore.prepare(usersQuery).asList(FetchOptions.Builder.withDefaults());

        //Add points to users
        for (Entity eventAss : results) {
            Key userKey = (Key) eventAss.getProperty(EventsAss.userKey);
            PointSystem.addPoints(PointSystem.ACTIONS.participate_event, userKey.getName());
        }

        event.setProperty(Events.state, States.archived.name());
        return event;
    }

    public static Reply isVolunteer(Entity entity) {
        Boolean volunteer = (Boolean) entity.getProperty(User.volunteer);
        if (volunteer == null || !volunteer) {
            LOG.warning("Entity is not volunteer");
            return new Reply("", null, g.toJson("entity_is_not_volunteer"), Response.Status.BAD_REQUEST);
        }
        return new Reply("", null, "", Response.Status.OK);
    }

    public static EventInfo getEventInfo(Entity event, Key user) {
        String title, description, worker, state, address, dico, district, county;
        EntityInfo entity = null;
        Date eventTime;
        double lat, lng;
        long id;
        boolean isGoing;
        List<BasicUserInfo> usersJoined;

        usersJoined = new LinkedList<>();
        isGoing = false;

        title = (String) event.getProperty(Events.title);
        description = (String) event.getProperty(Events.description);
        eventTime = (Date) event.getProperty(Events.eventTime);
        lat = (double) event.getProperty(Events.lat);
        lng = (double) event.getProperty(Events.lng);
        id = event.getKey().getId();
        address = (String) event.getProperty(Events.address);
        dico = (String) event.getProperty(Events.dico);
        district = (String) event.getProperty(Events.district);
        county = (String) event.getProperty(Events.county);

        Key entityKey = (Key) event.getProperty(Events.entityKey);
        try {
            entity = EntityValues.getEntityStats(datastore.get(entityKey));
        } catch (EntityNotFoundException e) {
            LOG.severe("Entity not Found");
        }

        Key workerKey = (Key) event.getProperty(Events.workerKey);
        worker = workerKey.getName();

        Query usersQuery = new Query(EventsAss.kind)
                .setFilter(new Query.FilterPredicate(EventsAss.eventKey, Query.FilterOperator.EQUAL, event.getKey()));

        List<Entity> results = datastore.prepare(usersQuery).asList(FetchOptions.Builder.withDefaults());

        for (Entity eventAss : results) {
            Key userKey = (Key) eventAss.getProperty(EventsAss.userKey);

            if (user != null && userKey.getName().equals(user.getName())) {
                isGoing = true;
            }

            try {
                BasicUserInfo userInfo = UserValues.getBasicUserInfo(datastore.get(userKey));
                usersJoined.add(userInfo);
            } catch (EntityNotFoundException e) {
                LOG.warning("User not Found");
            }
        }

        state = (String) event.getProperty(Events.state);

        return new EventInfo(title, description, worker, eventTime, lat, lng, id, usersJoined, entity, state, address, dico, district, county, isGoing);
    }

    public static List<EventInfo> listEvents(List<Entity> events, Key user) {
        List<EventInfo> info = new LinkedList<>();

        for (Entity event : events) {
            info.add(getEventInfo(event, user));
        }

        return info;
    }

    public static Entity getEvent(long id) {
        Key eventKey = KeyFactory.createKey(Events.kind, id);
        try {
            return datastore.get(eventKey);
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

    public static class EventAssValues {
        public final String keyS;
        public final Key key;
        public final Entity entity;

        public EventAssValues(String keyS, Key key, Entity entity) {
            this.key = key;
            this.entity = entity;
            this.keyS = keyS;
        }
    }

    public static EventAssValues getEventAssValues(long event_id, String username) {
        String key = username + event_id;
        Key eventAssKey = KeyFactory.createKey(EventsAss.kind, key);
        try {
            Entity eventAss = datastore.get(eventAssKey);
            return new EventAssValues(key, eventAssKey, eventAss);
        } catch (EntityNotFoundException e) {
            return new EventAssValues(key, eventAssKey, null);
        }
    }

    public static class FilterEvents {
        public final Key userKey;
        public final UserPermissions.ROLE role;

        public FilterEvents(Key userKey, UserPermissions.ROLE role) {
            this.userKey = userKey;
            this.role = role;
        }
    }

    public static FilterEvents getUserFilter(Entity user) {

        String role = (String) user.getProperty(User.role);

        if (role.equals(UserPermissions.ROLE.u_basic.name()))
            return new FilterEvents(user.getKey(), UserPermissions.ROLE.u_basic);

        else if (role.equals(UserPermissions.ROLE.u_entity.name())) {
            Reply reply2 = VolunteerValues.isVolunteer(user);
            if (reply2.ok())
                return new FilterEvents(user.getKey(), UserPermissions.ROLE.u_entity);

        } else if (role.equals(UserPermissions.ROLE.u_worker.name())) {
            return new FilterEvents(user.getKey(), UserPermissions.ROLE.u_worker);
        }

        return null;
    }

    public static boolean isWorkerAssociated(Entity worker, Entity event) {
        String workerKey = ((Key) event.getProperty(Events.workerKey)).getName();
        if (!workerKey.equals(worker.getKey().getName())) {
            LOG.warning("Worker not associated with event");
            return false;
        } else
            return true;
    }
}
