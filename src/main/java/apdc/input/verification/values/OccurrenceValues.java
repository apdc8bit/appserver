package apdc.input.verification.values;

import apdc.dico.DicoData;
import apdc.dico.DicoTableSingleton;
import apdc.firerisk.FireRiskData;
import apdc.firerisk.FireRiskTableSingleton;
import apdc.input.verification.utils.InputDataVerification;
import apdc.output.info.CommentInfo;
import apdc.output.info.OccurrenceInfo;
import apdc.resources.occurrences.Management;
import apdc.tables.occurrence.Comments;
import apdc.tables.occurrence.Occurrences;
import apdc.tables.occurrence.Votes;
import apdc.tables.occurrence.VotesCommentsCount;
import apdc.tables.user.User;
import apdc.util.CalendarUtils;
import com.github.davidmoten.geo.GeoHash;
import com.google.api.client.util.IOUtils;
import com.google.appengine.api.datastore.*;
import com.google.common.io.ByteStreams;
import com.google.gson.*;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import javax.xml.bind.DatatypeConverter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public class OccurrenceValues {
    private static final Logger LOG = Logger.getLogger(Management.class.getName());
    private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
    private static final Gson g = new Gson();

    public enum STATUS {
        on_hold,
        in_progress,
        completed
    }

    public enum TYPE {
        forest_cleanup,
        tree_cutting,
        garbage_cleanup,
        other
    }

    public enum PRIORITY {
        one, two, tree, four, five
    }

    public enum ORDER {
        votes, date, none
    }

    public static ORDER getOrder(String order) {
        order = order == null ? "" : order.toLowerCase();
        try {
            return ORDER.valueOf(order);
        } catch (IllegalArgumentException | NullPointerException e) {
            return ORDER.none;
        }
    }

    public static class Result {
        public final int r;
        public final Entity e;

        Result(int r, Entity e) {
            this.r = r;
            this.e = e;
        }
    }

    public static Result checkOccurrence(String occGeoHash) {
        //String occGeoHash = GeoHash.encodeHash(data.lat, data.lng, 8);

        List<String> geoHashes = GeoHash.neighbours(occGeoHash);
        geoHashes.add(occGeoHash);

        List<Entity> results = new LinkedList<>();
        for (String geoHash : geoHashes) {
            char[] c = geoHash.toCharArray();
            c[c.length - 1]++;
            String geohash_bigger = new String(c);
            Query.Filter archived_filter = new Query.FilterPredicate(Occurrences.archived, Query.FilterOperator.EQUAL, false);
            Query.Filter hash_bigger_filter = new Query.FilterPredicate(Occurrences.geoHash, Query.FilterOperator.GREATER_THAN_OR_EQUAL, geoHash);
            Query.Filter hash_smaller_filter = new Query.FilterPredicate(Occurrences.geoHash, Query.FilterOperator.LESS_THAN, geohash_bigger);
            Query.CompositeFilter and_filter = Query.CompositeFilterOperator.and(hash_bigger_filter, hash_smaller_filter, archived_filter);
            Query ctrQuery = new Query(Occurrences.kind).setFilter(and_filter);
            results.addAll(datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults()));
        }

        Date oneDayBefore = CalendarUtils.addDays(new Date(), -1);

        for (Entity occ : results) {
            Date occDate = (Date) occ.getProperty(Occurrences.creation_time);

            if (oneDayBefore.before(occDate)) {
                LOG.warning("Found recent occurrence in same location");
                return new Result(2, occ);//not allowed
            }
        }

        if (!results.isEmpty()) {
            LOG.warning("Found occurrence in same location");
            return new Result(1, results.get(0));//question???
        }

        LOG.info("Did not find any occurrences");
        return new Result(0, null);//ok
    }

    public static String getStatus(String status) {
        status = status == null ? "" : status.toLowerCase();
        try {
            return STATUS.valueOf(status).name();
        } catch (IllegalArgumentException | NullPointerException e) {
            return STATUS.on_hold.name();
        }
    }

    public static String getType(String type) {
        type = type == null ? "" : type.toLowerCase();
        try {
            return TYPE.valueOf(type).name();
        } catch (IllegalArgumentException | NullPointerException e) {
            return TYPE.other.name();
        }
    }

    public static String getPriority(String priority) {
        priority = priority == null ? "" : priority.toLowerCase();
        try {
            return PRIORITY.valueOf(priority).name();
        } catch (IllegalArgumentException | NullPointerException e) {
            return PRIORITY.one.name();
        }
    }

    public static void deleteOccurrence(Key occKey) {
        try {
            Entity occ = datastore.get(occKey);

            //Delete from Votes Table
            Query.Filter propertyFilterV = new Query.FilterPredicate(Votes.occurrenceKey, Query.FilterOperator.EQUAL, occKey);
            Query ctrQueryV = new Query(Votes.kind).setFilter(propertyFilterV);
            List<Entity> resultsV = datastore.prepare(ctrQueryV).asList(FetchOptions.Builder.withDefaults());
            for (Entity e : resultsV) datastore.delete(e.getKey());

            //Delete from Comments Table
            Query.Filter propertyFilterC = new Query.FilterPredicate(Comments.occurrenceKey, Query.FilterOperator.EQUAL, occKey);
            Query ctrQueryC = new Query(Comments.kind).setFilter(propertyFilterC);
            List<Entity> resultsC = datastore.prepare(ctrQueryC).asList(FetchOptions.Builder.withDefaults());
            for (Entity e : resultsC) datastore.delete(e.getKey());

            //Delete from Votes and Comments Count Table
            Key vcCountKey = KeyFactory.createKey(VotesCommentsCount.kind, occKey.getId());
            datastore.delete(vcCountKey);

            //Delete image from bucket
            String filename = (String) occ.getProperty(Occurrences.imageID);
            // if (filename != null && !filename.isEmpty())
            // GcsServlet.deleteFile(filename, GcsServlet.FILE.occurrence_image);


            // datastore.delete(occKey);
        } catch (EntityNotFoundException e) {
            LOG.warning("Occurrence not found");
        }
    }

//    public static void createOccurrence(Key occKey) {
//
//    }
//
//    public static void editOccurrence(Key occKey) {
//
//    }

    public enum REPORTS {
        violent, offensive, fraudulent, fake, spam, other, sexual,
    }

    public static String getReportValue(String report) {
        report = report == null ? "" : report.toLowerCase();
        try {
            return REPORTS.valueOf(report).name();
        } catch (IllegalArgumentException | NullPointerException e) {
            return REPORTS.other.name();
        }
    }

    public static OccurrenceInfo getOccurrenceInfo(Entity occurrence, Key userKey, Map<Key, Entity> votes) {
        return getOccurrenceInfo(occurrence.getProperties(), userKey, occurrence.getKey(), occurrence.getParent(), votes);
    }

    @SuppressWarnings("unchecked")
    private static OccurrenceInfo getOccurrenceInfo(Map<String, Object> occurrence, Key userKey, Key occKey, Key occUser, Map<Key, Entity> votes) {
        double lat = (double) occurrence.get(Occurrences.lat);
        double lng = (double) occurrence.get(Occurrences.lng);

        List<Double> lats = (List<Double>) occurrence.get(Occurrences.lats);
        List<Double> lngs = (List<Double>) occurrence.get(Occurrences.lngs);

        long id = occKey.getId();
        String username = occUser.getName();

        String title = (String) occurrence.get(Occurrences.title);
        String description = (String) occurrence.get(Occurrences.description);
        String type = (String) occurrence.get(Occurrences.type);
        String status = (String) occurrence.get(Occurrences.status);
        String imageID = (String) occurrence.get(Occurrences.imageID);
        String geoHash = (String) occurrence.get(Occurrences.geoHash);
        Date creationDate = (Date) occurrence.get(Occurrences.creation_time);
        String completedImageID = (String) occurrence.get(Occurrences.imageIDCompleted);

        String dico = (String) occurrence.get(Occurrences.dico);
        String county = (String) occurrence.get(Occurrences.county);
        String district = (String) occurrence.get(Occurrences.district);
        String address = (String) occurrence.get(Occurrences.address);

        FireRiskTableSingleton frT = FireRiskTableSingleton.getInstance();
        FireRiskData fr = frT.getRisk(dico);

        int priority = fr.fireRisk;
        priority = reverse(priority);

        long[] numbs = numbVotesComm(occKey);
        long votesN = numbs[0];
        long comments = numbs[1];
        boolean has_vote = false;
        if (votesN > 0 && userKey != null) {
            has_vote = hasVote(occKey, userKey, votes);
        }

        return new OccurrenceInfo(id, username, geoHash, title, description, type, status, imageID, lat, lng, lats, lngs, creationDate, comments, votesN, has_vote, completedImageID, county, district, dico, priority, address);
    }

    private static int reverse(int p) {
        //Super advanced algorithm to calculate the priority
        //Based on the risk of FIRE
        return 6 - p;
    }

    @SuppressWarnings("unchecked")
    public static List<OccurrenceInfo> getListWithUserVote(List<Entity> results, Key userKey) {
        List<OccurrenceInfo> occurrences = new LinkedList<>();

        List<Key> voteKeyList = new LinkedList<>();
        Map<Key, Entity> votes = null;

        if (userKey != null) {
            for (Entity occurrence : results) {
                String lKey = KeyFactory.keyToString(occurrence.getKey()).concat(KeyFactory.keyToString(userKey));
                Key voteKey = KeyFactory.createKey(Votes.kind, lKey);
                voteKeyList.add(voteKey);
            }

            votes = datastore.get(voteKeyList);
        }

        for (Entity occurrence : results) {
            occurrences.add(getOccurrenceInfo(occurrence, userKey, votes));
        }

        return occurrences;
    }

    private static boolean hasVote(Key occKey, Key userKey, Map<Key, Entity> votes) {
        String lKey = KeyFactory.keyToString(occKey).concat(KeyFactory.keyToString(userKey));
        Key voteKey = KeyFactory.createKey(Votes.kind, lKey);

        if (votes != null)
            return votes.containsKey(voteKey);
        else {
            try {
                datastore.get(voteKey);
                return true;
            } catch (EntityNotFoundException e) {
                return false;
            }
        }

    }

    private static long[] numbVotesComm(Key occKey) {
        long[] numbs = new long[2];
        try {
            Key countKey = KeyFactory.createKey(VotesCommentsCount.kind, occKey.getId());
            Entity count = datastore.get(countKey);
            numbs[0] = InputDataVerification.getLongValue((Long) count.getProperty(VotesCommentsCount.likeCount));
            numbs[1] = InputDataVerification.getLongValue((Long) count.getProperty(VotesCommentsCount.commentCount));

        } catch (EntityNotFoundException e) {
            numbs[0] = 0L;
            numbs[1] = 0L;
        }
        return numbs;
    }

    public static List<CommentInfo> listComments(List<Entity> results) {
        List<CommentInfo> comments = new LinkedList<>();
        long comment_id;
        String comment, username, imageID = null;
        Date creating_time, last_edit_time;
        for (Entity e : results) {
            comment_id = e.getKey().getId();
            comment = (String) e.getProperty(Comments.comment);
            creating_time = (Date) e.getProperty(Comments.creation_time);
            last_edit_time = (Date) e.getProperty(Comments.last_edit_time);


            Key userKey = (Key) e.getProperty(Comments.userKey);
            username = userKey.getName();
            try {
                Entity user = datastore.get(userKey);
                imageID = (String) user.getProperty(User.imageID);
            } catch (EntityNotFoundException e1) {
                e1.printStackTrace();
            }

            comments.add(new CommentInfo(comment_id, comment, creating_time, last_edit_time, username, imageID));
        }

        return comments;
    }

    public static boolean isInWater(double lat, double lng) {
        //This is the base 64 of the byte array of the image that is returned from the request,
        //the idea is that when I request a single pixel from lat/lng, if it is the water it returns
        //a pixel with the color blue, that pixel is always the same hence the base 64 will be the same.
        //It is simpler than creating an image object and comparing the color of the pixel, I don't know about speed
        String base64ofBluePixel = "iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAABlBMVEWr2//////2IjZJAAAAAWJLR0QB/wIt3gAAAApJREFUCNdjYAAAAAIAAeIhvDMAAAAASUVORK5CYII=";

        String uriI = String.format(
                "http://maps.googleapis.com/maps/api/staticmap?center=%f,%f" +
                        "&zoom=20" +
                        "&size=1x1" +
                        "&maptype=roadmap" +
                        "&sensor=false" +
                        "&visual_refresh=true",
                lat, lng);

        HttpClient clientI = HttpClientBuilder.create().build();
        HttpGet requestI = new HttpGet(uriI);

        try {
            HttpResponse response = clientI.execute(requestI);
            HttpEntity entity = response.getEntity();
            try (InputStream stream = entity.getContent()) {
                byte[] bytes = ByteStreams.toByteArray(stream);
                String data = DatatypeConverter.printBase64Binary(bytes);
                boolean inWater = data.equals(base64ofBluePixel);

                if (inWater)
                    LOG.warning("Occurrence is in water");
                else
                    LOG.info("Water check passed!");

                return inWater;
            }
        } catch (IOException e) {
            LOG.warning("Error checking water...");
        }

        return false;
    }

    public static DicoData getDico(double lat, double lng) {
        DicoTableSingleton dt = DicoTableSingleton.getInstance();
        return dt.getDicoFromCoords(lat, lng);
    }

    public static class GeoCode {
        public final String address;
        public final boolean inPT;

        public GeoCode(String address, boolean inPT) {
            this.address = address;
            this.inPT = inPT;
        }
    }

    public static GeoCode getAddress(double lat, double lng) {
        String uri = String.format(
                "https://maps.googleapis.com/maps/api/geocode/" +
                        "json?" +
                        "latlng=%f,%f" +
                        "&key=AIzaSyBNDXgmiS4mWGrt1SOidAWm_cbeUdLgYkw" +
                        "&language=pt-PT",
                lat, lng);

        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(uri);

        String reply;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            HttpResponse response = client.execute(request);
            HttpEntity entity = response.getEntity();

            boolean ok = response.getStatusLine().getStatusCode() == 200;
            if (ok && entity != null) {
                IOUtils.copy(entity.getContent(), out);

                reply = new String(out.toByteArray(), "UTF-8");
            } else
                return new GeoCode("", false);
        } catch (IOException ef) {
            return new GeoCode("", false);
        }

        JsonParser parser = new JsonParser();
        JsonObject o = parser.parse(reply).getAsJsonObject();

        String status = o.get("status").getAsString();

        if (!status.equals("OK")) {
            return new GeoCode("", true);
        }

        JsonArray res = o
                .getAsJsonArray("results")
                .get(0)
                .getAsJsonObject()
                .getAsJsonArray("address_components");

        boolean checked = false;

        for (JsonElement c : res) {
            JsonArray types = c.getAsJsonObject().getAsJsonArray("types");
            for (JsonElement t : types)
                if (t.getAsString().equals("country"))
                    if (c.getAsJsonObject().get("short_name").getAsString().equals("PT"))
                        checked = true;
        }

        if (!checked)
            return new GeoCode("", false);

        String address = o
                .getAsJsonArray("results")
                .get(0)
                .getAsJsonObject()
                .getAsJsonPrimitive("formatted_address")
                .getAsString();


        String[] values = address.split("Unnamed Road, ");

        if (values.length > 1)
            address = values[1];
        else
            return new GeoCode("", true);

        LOG.info("Address: " + address);

        return new GeoCode(address, true);
    }
}
