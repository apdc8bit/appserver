package apdc.input.verification.utils;

public class Device {

    public final static String MOBILE = "m";
    public final static String WEB = "w";

    public Device() {
    }

    public static boolean confirmCorrectDevice(String device) {
        return (device.equals(MOBILE) || device.equals(WEB));
    }

    public static boolean confirmMobile(String device) {
        return device.equals(MOBILE);
    }

    public static boolean confirmWeb(String device) {
        return device.equals(WEB);
    }
}
