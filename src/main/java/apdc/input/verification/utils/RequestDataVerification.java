package apdc.input.verification.utils;

import apdc.exceptions.ExpiredKeyException;
import apdc.exceptions.InvalidKeyException;
import apdc.input.data.InputDataInterface;
import apdc.output.messages.OutputStatus;
import apdc.permissions.Actions;
import apdc.permissions.UserPermissions;
import apdc.tables.user.User;
import apdc.util.keys.KeyGenerator;
import apdc.util.keys.KeyInterface;
import com.google.appengine.api.datastore.*;

import javax.ws.rs.core.Response;
import java.util.logging.Logger;

public class RequestDataVerification {
    private static final Logger LOG = Logger.getLogger(RequestDataVerification.class.getName());
    private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

    public static Reply validateDataTokenPermission(InputDataInterface data, String tokenID, String sessionID, String device, Actions action) {
        Reply reply_token = validateTokenPermission(tokenID, sessionID, device, action);

        if (reply_token.ok()) {
            Reply reply_data = validateData(data);

            if (reply_data.ok())
                return reply_token;

            return reply_data;
        }

        return reply_token;
    }

    public static Reply validateTokenPermission(String tokenID, String sessionID, String device, Actions action) {
        Reply reply = validateToken(tokenID, sessionID, device);

        if (reply.ok()) {
            reply = UserPermissions.isAllowed(reply.getUser(), action, device);
        }

        return reply;
    }

    public static Reply validateData(InputDataInterface data) {

        if (data == null || !data.validateData()) {
            LOG.warning("Missing or Wrong Parameters");
            return new Reply("", null, OutputStatus.WRONG_DATA, Response.Status.BAD_REQUEST);
        }

        return new Reply("", null, OutputStatus.REPLY_OK, Response.Status.OK);
    }

    public static Reply validateToken(String tokenID, String sessionID, String device) {
        try {
            String username;
            switch (device) {
                case Device.MOBILE:
                    username = KeyGenerator.getValues(tokenID, sessionID, KeyInterface.Type.MOBILE)[KeyInterface.VALUE];
                    break;
                case Device.WEB:
                    username = KeyGenerator.getValues(tokenID, sessionID, KeyInterface.Type.TOKEN)[KeyInterface.VALUE];
                    break;
                default:
                    username = KeyGenerator.getValues(tokenID, sessionID, KeyInterface.Type.PICTURE)[KeyInterface.VALUE];
                    break;
            }

            Entity user = getUserEntity(username);

            if (user == null)
                return new Reply(username, null, OutputStatus.NO_USER_FOUND, Response.Status.BAD_REQUEST);

            return new Reply(username, user, OutputStatus.REPLY_OK, Response.Status.OK);
        } catch (InvalidKeyException e) {
            LOG.info("Invalid Token");
            return new Reply("", null, OutputStatus.INVALID_TOKEN, Response.Status.UNAUTHORIZED);
        } catch (ExpiredKeyException e) {
            LOG.info("Expired Token");
            return new Reply("", null, OutputStatus.EXPIRED_TOKEN, Response.Status.UNAUTHORIZED);
        }
    }

    private static Entity getUserEntity(String username) {
        Key userKey = KeyFactory.createKey(User.kind, username);

        try {
            return datastore.get(userKey);
        } catch (EntityNotFoundException e) {
            LOG.warning("User does not exist: " + username);
            return null;
        }
    }
}