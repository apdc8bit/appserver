package apdc.input.verification.utils;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import java.util.List;
import java.util.logging.Logger;

public class InputDataVerification {
    private static final Logger LOG = Logger.getLogger(InputDataVerification.class.getName());

    public enum TYPE {
        PASSWORD, EMAIL, ZIPCODE, PHONE_NUMBER, NIF, USERNAME, ID, KEY, ADDRESS, NAME,
        DESCRIPTION, IMAGE, COMMENT, TITLE, LATITUDE, LONGITUDE,
    }

    private static final String password_regex = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{6,16}$";
    private static final String email_regex = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)])";
    private static final String zip_code_regex = "^\\d{4}(?:[-]\\d{3})?$";

    private static final String username_regex = "^[a-zA-Z0-9]+([[.]_-]?[a-zA-Z0-9])*$";
    private static final String username_regex_length = "^.{5,18}$";

    private static final int MAX_ARRAY_SIZE = 16;
    private static final double MAX_LAT_DIFF = 7.0, MAX_LNG_DIFF = 8.0;

    public static boolean validateText(String text, TYPE type) {
        boolean valid;

        if (text == null || text.isEmpty())
            return false;

        if (!text.equals(cleanText(text))) {
            LOG.severe("Forgot to clean text!!!");
            return false;
        }

        switch (type) {
            case PASSWORD:
                valid = text.matches(password_regex);
                LOG.info("Password: " + valid);
                break;
            case EMAIL:
                valid = text.matches(email_regex);
                LOG.info("Email: " + valid);
                break;
            case ZIPCODE:
                valid = text.matches(zip_code_regex);
                LOG.info("ZipCode: " + valid);
                break;
            case USERNAME:
                valid = text.matches(username_regex);
                valid = valid && text.matches(username_regex_length);
                LOG.info("Username: " + valid);
                break;
            case ADDRESS:
                valid = text.length() <= 77 && text.length() >= 4;
                LOG.info("Address: " + valid);
                break;
            case DESCRIPTION:
                valid = text.length() <= 240;
                LOG.info("Description: " + valid);
                break;
            case KEY:
                valid = text.length() > 6 && text.length() < 14;
                LOG.info("Key: " + valid);
                break;
            case IMAGE:
                valid = text.length() > 1 && text.length() < 14;
                LOG.info("Image: " + valid);
                break;
            case COMMENT:
                valid = text.length() > 1 && text.length() < 120;
                LOG.info("Comment: " + valid);
                break;
            case TITLE:
                valid = text.length() > 3 && text.length() < 32;
                LOG.info("Title: " + valid);
                break;
            case NAME:
                valid = text.length() > 2 && text.length() < 44;
                LOG.info("Name: " + valid);
                break;
            default:
                valid = false;
                break;
        }

        return valid;
    }

    public static boolean isEmpty(String... text) {
        boolean empty = true;

        for (String t : text)
            empty &= t == null || t.isEmpty();

        return empty;
    }

    public static boolean validateNumber(Number nb, TYPE type) {
        boolean valid;

        if (nb == null)
            return false;

        switch (type) {
            case LATITUDE:
                double lat_value = getDoubleValue(nb);
                valid = isInMap(lat_value, type);
                LOG.info("Lat: " + valid);
                break;
            case LONGITUDE:
                double lng_value = getDoubleValue(nb);
                valid = isInMap(lng_value, type);
                LOG.info("Lon: " + valid);
                break;
            case ID:
                try {
                    long id = nb.longValue();
                    valid = id != 0L;
                } catch (Exception e) {
                    return false;
                }
                LOG.info("ID: " + valid);
                break;
            case PHONE_NUMBER:
                int phone = getIntValue(nb);
                valid = String.valueOf(phone).length() == 9;
                LOG.info("Phone Number: " + valid);
                break;
            case NIF:
                int nif = getIntValue(nb);
                valid = String.valueOf(nif).length() == 9;
                LOG.info("NIF: " + valid);
                break;
            default:
                valid = false;
                break;
        }

        return valid;
    }

    public static boolean validateCoordinates(List<Double> lats, List<Double> lngs) {
        boolean validArray = lats.size() < MAX_ARRAY_SIZE && lngs.size() < MAX_ARRAY_SIZE && lats.size() == lngs.size();

        if (!validArray)
            return false;
        if (lats.size() == 0)
            return true;

        for (Double n : lats)
            validArray = validArray && validateNumber(n, InputDataVerification.TYPE.LATITUDE);

        for (Double n : lngs)
            validArray = validArray && validateNumber(n, InputDataVerification.TYPE.LONGITUDE);

        if (!validArray) {
            LOG.warning("Invalid Array");
            return false;
        }

        validArray = isAreaTooBig(lats, lngs);

        return validArray;
    }

    private static int getIntValue(Number number) {
        try {
            return number == null ? 0 : number.intValue();
        } catch (Exception e) {
            return 0;
        }
    }

    private static double getDoubleValue(Number number) {
        try {
            return number == null ? 0.0 : number.doubleValue();
        } catch (Exception e) {
            return 0.0;
        }
    }

    public static long getLongValue(Long number) {
        try {
            return number == null ? 0L : number;
        } catch (Exception e) {
            return 0L;
        }
    }

    private static String cleanTextContent(String text) {
        // strips off all non-ASCII characters
        //text = text.replaceAll("[^\\x00-\\x7F]", "");

        // erases all the ASCII control characters
        text = text.replaceAll("[\\p{Cntrl}&&[^\r\n\t]]", "");

        // removes non-printable characters from Unicode
        text = text.replaceAll("\\p{C}", "");

        return text.trim();
    }

    public static String cleanText(String text) {
        //Remove Non characters
        String result = cleanTextContent(text);

        //Remove ' "
        result = result.replace("\"", "?");
        result = result.replace("'", "?");

        return Jsoup.clean(result, Whitelist.none());
    }

    private static boolean isAreaTooBig(List<Double> lat, List<Double> lng) {
        double diffLat, diffLng;
        int i = 0;

        for (; i < lat.size() - 1; i++) {
            diffLat = getDiff(lat.get(i), lat.get(i + 1));
            diffLng = getDiff(lng.get(i), lng.get(i + 1));

            if (isTooBig(diffLat, diffLng))
                return false;
        }

        diffLat = getDiff(lat.get(i), lat.get(0));
        diffLng = getDiff(lng.get(i), lng.get(0));

        return !isTooBig(diffLat, diffLng);
    }

    private static double getDiff(double coordF, double coordL) {
        return Math.abs(coordF - coordL) * 1000;
    }

    private static boolean isTooBig(double diffLat, double diffLng) {
        boolean res = diffLat > MAX_LAT_DIFF || diffLng > MAX_LNG_DIFF;
        if (res) {
            LOG.warning("Area Too Big: Lat diff: " + diffLat + ", Lng diff: " + diffLng);
            return true;
        }
        return false;
    }

    public static double[] generateCenter(List<Double> lats, List<Double> lngs) {
        double lat, lng;

        double x = 0.;
        double y = 0.;
        double pointCount = lats.size();

        for (int i = 0; i < lats.size(); i++) {
            double x_point = lats.get(i);
            double y_point = -lngs.get(i);
            x += x_point;
            y += y_point;
        }

        lat = x / pointCount;
        lng = -y / pointCount;

        return new double[]{lat, lng};
    }

    private static boolean isInMap(double value, TYPE type) {
        //Portugal Continental
        double bound_left = -9.6;//lng
        double bound_right = -6.1;//lng
        double bound_bottom = 36.9;//lat
        double bound_top = 42.0;//lat

        //Azores
        double bound_left_m = -31.5;//lng
        double bound_right_m = -24.4;//lng
        double bound_bottom_m = 39.8;//lat
        double bound_top_m = 36.8;//lat

        //Madeira
        double bound_left_a = -17.4;//lng
        double bound_right_a = -16.2;//lng
        double bound_bottom_a = 32.3;//lat
        double bound_top_a = 33.2;//lat

        if (type.equals(TYPE.LATITUDE))
            return value >= bound_bottom && value <= bound_top
                    || value >= bound_bottom_m && value <= bound_top_m
                    || value >= bound_bottom_a && value <= bound_top_a;

        else if (type.equals(TYPE.LONGITUDE))
            return value >= bound_left && value <= bound_right
                    || value >= bound_left_m && value <= bound_right_m
                    || value >= bound_left_a && value <= bound_right_a;

        return false;
    }
}
