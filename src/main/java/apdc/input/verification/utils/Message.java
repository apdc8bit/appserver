package apdc.input.verification.utils;

import apdc.output.messages.OutputStatus;
import com.google.gson.Gson;

public class Message {
    public String message;
    public final int code;

    public Message(int code) {
        this.code = code;
    }

    public Message(String response, int code) {
        this(code);
        this.message = response;
    }

    public Message(OutputStatus.Messages response, int code) {
        this(code);
        this.message = response.name();
    }

    public String getJson() {
        return new Gson().toJson(this);
    }
}
