package apdc.input.verification.utils;

import apdc.exceptions.ExpiredEmailVerificationException;
import apdc.exceptions.InvalidEmailKeyException;
import apdc.tables.special.EmailValidation;
import apdc.tables.user.User;
import apdc.util.CalendarUtils;
import com.google.appengine.api.datastore.*;
import com.google.appengine.repackaged.org.apache.commons.codec.digest.DigestUtils;

import java.util.Date;

public class EmailValidationKey {
    private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

    public static String getKey(String username) {
        Key userKey = KeyFactory.createKey(User.kind, username);

        Long time = System.currentTimeMillis();

        String key = DigestUtils.sha512Hex(time.toString() + username);

        Entity email = new Entity(EmailValidation.kind, key);

        email.setProperty(EmailValidation.expiration_date, CalendarUtils.addDays(new Date(), 1));
        email.setProperty(EmailValidation.user_key, userKey);

        datastore.put(email);

        return key;
    }

    private static Entity getEmail(String key) throws InvalidEmailKeyException {
        Key emailKey = KeyFactory.createKey(EmailValidation.kind, key);

        try {
            Entity email = datastore.get(emailKey);
            datastore.delete(emailKey);
            return email;
        } catch (EntityNotFoundException e) {
            throw new InvalidEmailKeyException();
        }
    }

    public static Entity getUserValid(String key) throws ExpiredEmailVerificationException, InvalidEmailKeyException {
        Entity email = getEmail(key);

        Key userKey = (Key) email.getProperty(EmailValidation.user_key);

        Date expiration_date = (Date) email.getProperty(EmailValidation.expiration_date);
        if (expiration_date.before(new Date())) {
            throw new ExpiredEmailVerificationException();
        }

        try {
            return datastore.get(userKey);
        } catch (EntityNotFoundException e) {
            throw new InvalidEmailKeyException();
        }
    }

    public static Entity getUserExpired(String key) throws InvalidEmailKeyException {
        Entity email = getEmail(key);

        Key user = (Key) email.getProperty(EmailValidation.user_key);

        try {
            return datastore.get(user);
        } catch (EntityNotFoundException e) {
            throw new InvalidEmailKeyException();
        }
    }
}
