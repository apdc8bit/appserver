package apdc.tables.entity;

public class EntityStats {
    public static final String kind = "EntityStats"; //key = entity key
    public static final String numbStarted = "number_started";
    public static final String numbFinished = "number_finished";
    public static final String avgResponseTime = "average_response_time";
    public static final String avgCompletionTime = "average_completion_time";
}
