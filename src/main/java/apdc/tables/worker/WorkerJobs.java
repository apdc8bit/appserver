package apdc.tables.worker;

public class WorkerJobs {
    public static final String kind = "WorkerJobs";//key= parent key worker + occ id
    public static final String occurrence_key = "occurrence_key";
    public static final String added_time = "added_time";
    public static final String finish_time = "finish_time";
    public static final String confirmed_time = "confirmed_time";
    public static final String state = "state";
    public static final String entity_key = "entity_key";
    public static final String imageID = "image_id";
}
