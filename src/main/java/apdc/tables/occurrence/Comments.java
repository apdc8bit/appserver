package apdc.tables.occurrence;

public class Comments {
    public static final String kind = "Comments";//key = random id
    public static final String occurrenceKey = "Occurrence_key_comments";
    public static final String userKey = "User_key_comments";
    public static final String comment = "comment_text";
    public static final String creation_time = "comment_creation_time";
    public static final String last_edit_time = "comment_edition_time";

}
