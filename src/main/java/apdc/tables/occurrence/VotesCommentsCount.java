package apdc.tables.occurrence;

public class VotesCommentsCount {
    public static final String kind = "OccurrenceKeyCount";//key = occurrence key
    public static final String commentCount = "comment_count";
    public static final String likeCount = "like_count";
    public static final String occurrenceKey = "occurrence_key";
    public static final String occurrenceCreationTime = "occurrence_creation_time";
    public static final String lastInteractionTime = "occurrence_last_interaction_time";

}
