package apdc.tables.occurrence;

public class Votes {

    public static final String kind = "Votes";//key = String user key + String occurrence key
    public static final String occurrenceKey = "occurrence_key_likes";
    public static final String userKey = "user_key_likes";
    public static final String added_date = "added_date";

}
