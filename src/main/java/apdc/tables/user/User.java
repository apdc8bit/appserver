package apdc.tables.user;

public class User {
    public static final String kind = "User";//key = username
    public static final String email = "user_email";
    public static final String password = "user_pwd";
    public static final String role = "user_role";
    public static final String valid = "user_valid";
    public static final String creation_time = "user_creation_time";
    public static final String address = "user_address";
    public static final String zip_code = "user_zip_code";
    public static final String phone_number = "user_phone_number";
    public static final String name = "user_first_last_name";
    public static final String nif = "user_nif";
    public static final String last_edit_time = "user_last_edit_time";
    public static final String imageID = "user_image_id";
    public static final String for_removal = "user_for_removal";
    public static final String old_password = "user_old_password";
    public static final String timeout = "user_timeout";
    public static final String dico = "user_dico";
    public static final String county = "user_county";
    public static final String district = "user_district";
    public static final String lat = "user_lat";
    public static final String lng = "user_lng";
    public static final String volunteer = "user_volunteer";
    public static final String mobileDeviceID = "user_mobile_device_id";
    public static final String webDeviceID = "user_web_device_id";
}
