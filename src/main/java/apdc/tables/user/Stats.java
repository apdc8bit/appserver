package apdc.tables.user;

public class Stats {

    public static final String kind = "UserStats";
    public static final String login = "user_stats_login";
    public static final String failed = "user_stats_failed";
}
