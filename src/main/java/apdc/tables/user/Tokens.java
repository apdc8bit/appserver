package apdc.tables.user;

public class Tokens {

    public static final String kind = "Tokens";//key = token
    public static final String expirationDate = "token_expiration_date";
    public static final String value = "token_value";

}