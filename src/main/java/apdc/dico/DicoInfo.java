package apdc.dico;

public class DicoInfo {
    //Class used to load json

    public String DICO;
    public int idConcelho;
    public int idDistrito;
    public double latitude;
    public String local;
    public double longitude;
}
