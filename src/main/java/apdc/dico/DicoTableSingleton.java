package apdc.dico;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Map;
import java.util.logging.Logger;

public class DicoTableSingleton {
    private final Logger LOG = Logger.getLogger(DicoTableSingleton.class.getName());

    private Map<String, DicoInfo[]> dicoTable;

    private static DicoTableSingleton instance = null;

    private DicoTableSingleton() {
    }

    public static DicoTableSingleton getInstance() {
        if (instance == null) {
            instance = new DicoTableSingleton();
        }
        return instance;
    }

    public DicoData getDicoFromCoords(double lat, double lng) {
        checkTable();

        double best_match = Double.MAX_VALUE;
        String county = "", district = "", dico = "";

        //Iterate through all counties and find closest one
        for (Map.Entry<String, DicoInfo[]> c : dicoTable.entrySet()) {
            for (DicoInfo d : c.getValue()) {
                double diff_lat = Math.abs(lat - d.latitude);
                double diff_lng = Math.abs(lng - d.longitude);
                double total = diff_lat + diff_lng;

                if (total < best_match) {
                    dico = d.DICO;
                    best_match = total;
                    county = d.local;
                    district = c.getKey();
                }
            }
        }

        return new DicoData(county, district, dico);
    }


    public DicoInfo getInfoFromDico(String dico) {
        checkTable();

        //Iterate through all entries until it finds a matching dico
        for (Map.Entry<String, DicoInfo[]> c : dicoTable.entrySet()) {
            for (DicoInfo d : c.getValue()) {
                if (d.DICO.equals(dico))
                    return d;
            }
        }

        return null;
    }

    private void checkTable() {
        if (dicoTable == null)
            loadTable();
    }

    private void loadTable() {
        Gson gson = new Gson();
        JsonReader reader;
        String DICO_TABLE = "dico";
        String file = new java.io.File("").getAbsolutePath().concat("/secret/" + DICO_TABLE + ".json");

        try {
            reader = new JsonReader(new FileReader(file));
            dicoTable = gson.fromJson(reader, new TypeToken<Map<String, DicoInfo[]>>() {
            }.getType());
        } catch (FileNotFoundException e) {
            LOG.warning("No File Found!");
        }
    }
}
