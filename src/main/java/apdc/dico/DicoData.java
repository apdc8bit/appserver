package apdc.dico;

public class DicoData {
    public final String county;
    public final String district;
    public final String dico;

    public DicoData(String county, String district, String dico) {
        this.county = county;
        this.district = district;
        this.dico = dico;
    }
}
