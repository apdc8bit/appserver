package apdc.exceptions;

public class InvalidTextException extends RuntimeException {
    static final long serialVersionUID = 0L;

    public InvalidTextException() {
        super();
    }

    public InvalidTextException(String message) {
        super(message);
    }

}