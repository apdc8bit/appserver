package apdc.exceptions;

public class ExpiredKeyException extends RuntimeException {
    static final long serialVersionUID = 0L;

    public ExpiredKeyException() {
        super();
    }

    public ExpiredKeyException(String message) {
        super(message);
    }

}