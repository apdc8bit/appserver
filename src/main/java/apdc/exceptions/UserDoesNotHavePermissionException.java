package apdc.exceptions;

public class UserDoesNotHavePermissionException extends RuntimeException {
    static final long serialVersionUID = 0L;

    public UserDoesNotHavePermissionException() {
        super();
    }

    public UserDoesNotHavePermissionException(String message) {
        super(message);
    }

}