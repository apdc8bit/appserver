package apdc.exceptions;

public class NoDeviceException extends RuntimeException {
    static final long serialVersionUID = 0L;

    public NoDeviceException() {
        super();
    }

    public NoDeviceException(String message) {
        super(message);
    }
}