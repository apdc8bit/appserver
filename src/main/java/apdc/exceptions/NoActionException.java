package apdc.exceptions;

public class NoActionException extends RuntimeException {
    static final long serialVersionUID = 0L;

    public NoActionException() {
        super();
    }

    public NoActionException(String message) {
        super(message);
    }
}
