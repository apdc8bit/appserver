package apdc.permissions;

import apdc.exceptions.*;
import apdc.input.verification.utils.Device;
import apdc.input.verification.utils.Reply;
import apdc.output.messages.OutputStatus;
import apdc.tables.user.User;
import apdc.tables.user.UserLogs;
import com.google.appengine.api.datastore.*;

import javax.ws.rs.core.Response;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Logger;

import static apdc.resources.api.Base.datastore;

public class UserPermissions {
    private static final Logger LOG = Logger.getLogger(UserPermissions.class.getName());

    private static final String PORTUGAL = "PT";
    private static final int MIN_HOUR = 8;
    private static final int MAX_HOUR = 18;
    private static final int MAX_LOGIN_FAILED = 3;

    public enum ROLE {
        u_basic,
        u_worker,
        u_entity,
        u_admin
    }

    public static Reply isAllowed(Entity user, Actions action, String device) throws UserIsNotConfirmedException, NoDeviceException, NoActionException, NoUserRoleException, UserDoesNotHavePermissionException {

        //If action is one of this, no need to check permissions
        if (isActionForNonValidUsers(action))
            return new Reply(user.getKey().getName(), user, OutputStatus.REPLY_OK, Response.Status.OK);

        //Check if user is valid or wants to change email
        if (!(boolean) user.getProperty(User.valid)) {
            LOG.warning("User needs email confirmation");
            return new Reply(user.getKey().getName(), user, OutputStatus.NOT_VALID, Response.Status.FORBIDDEN);
        }

        //Check if the device is valid
        if (!isValidDevice(device)) {
            LOG.warning("Wrong device use /m/ or /w/");
            return new Reply(user.getKey().getName(), user, OutputStatus.NOT_A_DEVICE, Response.Status.FORBIDDEN);
        }

        String role = (String) user.getProperty(User.role);

        //Check if the is valid
        if (!isValidRole(role)) {
            LOG.warning("Someone messed up, because the role ins't valid");
            return new Reply(user.getKey().getName(), user, OutputStatus.NOT_A_ROLE, Response.Status.FORBIDDEN);
        }

        //Check if user has permission to execute action
        if (!hasPermission(role, action)) {
            LOG.warning("user does not have permission, role: " + role);
            return new Reply(user.getKey().getName(), user, OutputStatus.NO_PERMISSION, Response.Status.FORBIDDEN);
        }

        return new Reply(user.getKey().getName(), user, OutputStatus.REPLY_OK, Response.Status.OK);
    }

    private static boolean isActionForNonValidUsers(Actions action) {
        return (action.equals(Actions.add_image) || action.equals(Actions.change_self_email)
                || action.equals(Actions.list_self_info) || action.equals(Actions.delete_self_account)
                || action.equals(Actions.change_settings) || action.equals(Actions.edit_self_profile));
    }

    private static boolean isValidDevice(String device) {
        return Device.MOBILE.equals(device) || Device.WEB.equals(device);
    }

    private static boolean isValidRole(String role) {
        try {
            ROLE.valueOf(role);
            return true;
        } catch (IllegalArgumentException | NullPointerException e) {
            return false;
        }
    }

    private static boolean hasPermission(String role, Actions action) {
        PermissionTableSingleton table = PermissionTableSingleton.getInstance();

        return table.hasPermission(role.toLowerCase(), action.name().toLowerCase());
    }

    public static boolean verifyAdminLogin(long logins_failed) {
        boolean ok;

        //Check hours
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        ok = hour >= MIN_HOUR && hour <= MAX_HOUR;

        //Check failed logins
        ok = ok || logins_failed < MAX_LOGIN_FAILED;

        return ok;
    }

    public static boolean isUserInTimeout(Entity user) {
        Date timeout = (Date) user.getProperty(User.timeout);
        if (timeout != null) {
            if (timeout.after(new Date())) {
                LOG.warning("User is still in timeout");
                return true;
            } else {
                //Timeout is over
                user.removeProperty(User.timeout);
                return false;
            }
        }
        return false;
    }

    public static boolean isLoginFromAuthorizedCountry(String country) {
        if (country != null && !country.equalsIgnoreCase(PORTUGAL)) {
            LOG.warning("Login is from another country: " + country);
            return false;
        }
        return true;
    }

    public static boolean isLoginFromAnotherLocation(Key userKey, String request_ip) {
        FetchOptions fo = FetchOptions.Builder.withLimit(1);
        Query query = new Query(UserLogs.kind)
                .setAncestor(userKey)
                .addSort(UserLogs.userLoginTime, Query.SortDirection.DESCENDING);
        QueryResultList<Entity> logs_a = datastore.prepare(query).asQueryResultList(fo);

        if (!logs_a.isEmpty()) {
            Entity last_log = logs_a.get(0);
            String last_ip = (String) last_log.getProperty(UserLogs.loginIP);

            return last_ip != null && request_ip != null && !last_ip.equals(request_ip);
        }

        return false;
    }

    public static boolean isLoginInNewDay(Date last_login) {
        if (last_login != null) {
            Calendar cal = Calendar.getInstance();
            int today = cal.get(Calendar.DAY_OF_MONTH);
            cal.setTime(last_login);
            int last_day = cal.get(Calendar.DAY_OF_MONTH);
            return today != last_day;
        }
        return false;
    }
}
